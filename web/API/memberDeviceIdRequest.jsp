<%-- 
    Document   : memberDeviceIdRequest
    Created on : APR 28, 2020, 10:56:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    Logger logger = Logger.getLogger("memberDeviceIdRequest_jsp.class");

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String deviceId = "";
    String reqHost = "";
    String reqIP = "";
    String reqTerm = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("deviceId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        deviceId = request.getParameter("deviceId").trim();
        //  paymentRefNo = request.getParameter("paymentRefNo") == null ? "" : request.getParameter("paymentRefNo").trim();

        reqHost = request.getParameter("reqHost") == null ? "" : request.getParameter("reqHost").trim();
        reqIP = request.getParameter("reqIP") == null ? "" : request.getParameter("reqIP").trim();
        reqTerm = request.getParameter("reqTerm") == null ? "" : request.getParameter("reqTerm").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberDeviceIdRequest API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    Query memberDeviceIdAddSQL = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    String adduser = Integer.toString(memId);
    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {
            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //check member member_deviceid already exits or not
                        String memberDeviceIdCheckSQL = "SELECT  count(*) FROM member_deviceid  "
                                + "WHERE member_id = '" + memId + "'";

                        String memberDeviceIdCheckNumRows = dbsession.createSQLQuery(memberDeviceIdCheckSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(memberDeviceIdCheckSQL).uniqueResult().toString();
                        //   int numrows = Integer.parseInt(numrow1);

                        logger.info("API :: memberDeviceIdCheckNumRows ::" + memberDeviceIdCheckNumRows);

                        //if 0 insert member Id with device Id
                        //else Update member device Id
                        if (memberDeviceIdCheckNumRows.equals("0")) {

                            //   String idS = getId.getID(43);
                            //   int postContentRatingHistoryId = Integer.parseInt(idS);                            
                            //  or memId
                            String status = "1";

                            //insert member_deviceid
                            memberDeviceIdAddSQL = dbsession.createSQLQuery("INSERT INTO member_deviceid("
                                    + "id,"
                                    + "member_id,"
                                    + "device_id,"
                                    + "status,"
                                    + "add_date,"
                                    + "add_ip,"
                                    + "add_term,"
                                    + "add_user"
                                    + ") VALUES("
                                    + "'" + memId + "',"
                                    + "'" + memId + "',"
                                    + "'" + deviceId + "',"
                                    + "'" + status + "',"
                                    + "'" + adddate + "',"
                                    + "'" + ipAddress + "',"
                                    + "'" + hostname + "',"
                                    + "'" + adduser + "'"
                                    + "  ) ");

                            logger.info("API :: memberDeviceIdAddSQL ::" + memberDeviceIdAddSQL);
                            memberDeviceIdAddSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member deviceId added successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When deviceId  added ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When deviceId added");
                            }

                        } else {

                            //update member_deviceid deviceId
                            //update request status
                            Query updateMemberDeviceIdSQL = dbsession.createSQLQuery("UPDATE  member_deviceid SET device_id='" + deviceId + "',mod_user = '" + adduser + "',mod_date = '" + adddate + "',mod_ip = '" + adduser + "',mod_term='" + hostname + "' WHERE member_id = '" + memId + "'");

                            updateMemberDeviceIdSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member deviceId updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When deviceId  updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member address updated");
                            }

                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        //finally {
        //     dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
