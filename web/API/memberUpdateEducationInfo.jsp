<%-- 
    Document   : memberUpdateEducationInfo
    Created on : June 19, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    //  ALTER TABLE `joybandb`.`member_address` 
//CHANGE COLUMN `address_type` `address_type` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'P->Permanent Address:M->Mailing Address' ;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    Logger logger = Logger.getLogger("memberUpdateEducationInfo_jsp.class");

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String memberEduId = "";
    String degreeTypeId = "";
    String instituteName = "";
    String boardUniversityName = "";
    String passingYear = "";
    String resultTypeId = "";
    String resultText = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("memberEduId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        memberEduId = request.getParameter("memberEduId").trim();

        degreeTypeId = request.getParameter("degreeTypeId") == null ? "0" : request.getParameter("degreeTypeId").trim();
        instituteName = request.getParameter("instituteName") == null ? "" : request.getParameter("instituteName").trim();
        boardUniversityName = request.getParameter("boardUniversityId") == null ? "" : request.getParameter("boardUniversityId").trim();
        passingYear = request.getParameter("passingYear") == null ? "" : request.getParameter("passingYear").trim();
        resultTypeId = request.getParameter("resultTypeId") == null ? "0" : request.getParameter("resultTypeId").trim();
        resultText = request.getParameter("resultText") == null ? "" : request.getParameter("resultText").trim();

        //   memberName = request.getParameter("name").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("BYDO :: API :: memberUpdateEducationInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //   String districtSQL = "SELECT  DISTRICT_NAME FROM district WHERE ID = '" + districtId + "'";
                        //  String districtName = dbsession.createSQLQuery(districtSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(districtSQL).uniqueResult().toString();
                        Query universitySQL = null;
                        Object universityObj[] = null;

                        String universityId = "";

                        universitySQL = dbsession.createSQLQuery("SELECT * FROM university WHERE university_long_name ='" + boardUniversityName + "'");
                        //universitySQL = dbsession.createSQLQuery("SELECT * FROM university  ORDER BY university_long_name ASC");

                        if (!universitySQL.list().isEmpty()) {
                            for (Iterator it = universitySQL.list().iterator(); it.hasNext();) {

                                universityObj = (Object[]) it.next();
                                universityId = universityObj[0].toString();
                            }
                        } else {

                            universityId = getId.getID(56); // UNIVERSITY_ID
                            //   int regMemberUniversityId = Integer.parseInt(idUniversity);

                            Query mEducationUniversityAddSQL = dbsession.createSQLQuery("INSERT INTO university("
                                    + "university_id,"
                                    + "university_short_name,"
                                    + "university_long_name,"
                                    + "type"
                                    + ") VALUES("
                                    + "" + universityId + ","
                                    + "'" + boardUniversityName + "',"
                                    + "'" + boardUniversityName + "',"
                                    + "'0'"
                                    + "  ) ");

                            logger.info("mEducationUniversityAddSQL ::" + mEducationUniversityAddSQL);
                            System.out.println("mEducationUniversityAddSQL ::" + mEducationUniversityAddSQL);

                            mEducationUniversityAddSQL.executeUpdate();
                        }

                        if (!memberEduId.equals("0")) {
                            //Update member Address

                            //update member Rating for comment_content_point filed
                            Query updateMemberEducationInfoSQL = dbsession.createSQLQuery("UPDATE  member_education_info SET "
                                    + "degree_type_id = '" + degreeTypeId + "',"
                                    + "institute_name = '" + instituteName + "',"
                                    + "board_university_id = '" + universityId + "',"
                                    + "year_of_passing='" + passingYear + "', "
                                    + "result_type_id='" + resultTypeId + "', "
                                    + "result='" + resultText + "'"
                                    + "WHERE id = '" + memberEduId + "'");
                            
                            System.out.println("updateMemberEducationInfoSQL:  " + updateMemberEducationInfoSQL);

                            updateMemberEducationInfoSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Education info updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member Education updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member Education updated");
                            }

                        } else {
                            //Insert member Education

                            String idMA = getId.getID(42);
                            int regMemberEducationId = Integer.parseInt(idMA);

                            Query mEducationAddSQL = dbsession.createSQLQuery("INSERT INTO member_education_info("
                                    + "id,"
                                    + "member_id,"
                                    + "degree_type_id,"
                                    + "institute_name,"
                                    + "board_university_id,"
                                    + "year_of_passing,"
                                    + "result_type_id,"
                                    + "result"
                                    + ") VALUES("
                                    + "" + regMemberEducationId + ","
                                    + "'" + memId + "',"
                                    + "'" + degreeTypeId + "',"
                                    + "'" + instituteName + "',"
                                    + "'" + universityId + "',"
                                    + "'" + passingYear + "',"
                                    + "'" + resultTypeId + "',"
                                    + "'" + resultText + "'"
                                    + "  ) ");

                            logger.info("mEducationAddSQL ::" + mEducationAddSQL);
                            System.out.println("mEducationAddSQL ::" + mEducationAddSQL);

                            mEducationAddSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Education Added Successfully");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("User ID :" + memberId + " Member Education Added Successfully");
                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member address Add");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member address Add");
                            }
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "ERROR!!! Member Password info wrong ");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("ERROR!!! Member Password info wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
