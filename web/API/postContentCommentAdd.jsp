<%-- 
    Document   : postContentCommentAdd
    Created on : MAY 07, 2020, 10:53:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>



<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.util.FileUploadHelper" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    Logger logger = Logger.getLogger("postContentCommentAdd_jsp.class");

    getRegistryID getId = new getRegistryID();

    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageUploadPath + "postcontent/";

    logger.info("postContentCommentAdd API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    String key = "";
    String memberId = "";
    String givenPassword = "";

    String adddate = "";
    String commentParent = "";
    String commentDesc = "";
    String commentDate = "";
    String postContentId = "";

    String haveAttachment = ""; //1== yes 0==No attachment   
    String attachment = "";
    String attachmentThumb = "";
    String attachmentLink = "";

    String decoded = "";

    String commentContentPicture = "";
    String commentContentPictureLink = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("commentParent") && request.getParameterMap().containsKey("postContentId")) {
        key = request.getParameter("key").trim();

        System.out.println("Request:"+request); 
        System.out.println("Request Part Size:"+request.getParts().size()); 
        
        
        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        postContentId = request.getParameter("postContentId").trim();
        commentParent = request.getParameter("commentParent").trim();
        commentDesc = request.getParameter("commentDesc") == null ? "" : request.getParameter("commentDesc").trim();

        haveAttachment = request.getParameter("haveAttachment").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("postContentCommentAdd API  imageBase64String :" + imageBase64String);

        System.out.println("postContentCommentAdd API imageBase64String :" + imageBase64String);

        System.out.println("postContentCommentAdd API commentDesc :" + commentDesc);

        decoded = URLDecoder.decode(commentDesc, "UTF-8");

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: postContentCommentAdd API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    Query postContentCommentAddSQL = null;

    Query commentRatingAddSQL = null;

    //  String ratingPointHostorySQL = "";
    //   String ratingPointHostoryNumRows = "";
    if (rec == 1) {

        try {

            
            if (haveAttachment.equals("0")) {
                
                
                        //isMultipart = ServletFileUpload.isMultipartContent(request);	
			//logger.debug("process mobile resu  130 ");
			//String path = getServletContext().getRealPath("/img2/");
                        String path = "D:\\";
			String FileNamePrefix = "";
			//FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
			Collection<Part> parts = new ArrayList<Part>();
			try {
                            
				parts = request.getParts();
                                
				System.out.println("process mobile resu  131 partsize==== "+parts.size());
			} catch (Exception e) {
				System.out.println("process mobile resu  13000 partsize==== "+e.getMessage());
				// TODO: handle exception
			}
		    
			System.out.println("2");
                        
			if(parts != null && parts.size()>0) {
                            System.out.println("3");
				Part filePart_recordFile =  request.getPart("file");
				if(filePart_recordFile != null)
				{
                                    System.out.println("4");
					String Value = (String) FileUploadHelper.getFileName(filePart_recordFile);
					System.out.println("process mobile resu  131 ");
					if(Value != null)
					{
						//Value = Jsoup.clean(Value,Whitelist.simpleText());
					}
					System.out.println("recordFile = " + Value);
					if(Value != null && !Value.equalsIgnoreCase(""))
					{
						if(Value != null && !Value.equalsIgnoreCase(""))
						{					
							String FileName = FileNamePrefix + "" + "recordFile" + "" + Value;
							//appUser.profileImageFile = (FileName);
							
							FileUploadHelper.uploadFile(filePart_recordFile, FileName, path);
						}
					}
					else
					{
						System.out.println("FieldName has a null Value, not updating" + " = " + Value);
					}
					        //System.out.println("process profile image "+appUser.profileImageFile);
				}
				else {
					//appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
					//System.out.println("process profile image from elseeeeee===  "+appUser.profileImageFile);
					
				}
			}
			else {
				//appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
			}
                
            }
            if (haveAttachment.equals("1")) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("postContentCommentAdd API filePath :" + filePath);
                logger.info("postContentCommentAdd API fileName1 :" + fileName1);

                System.out.println("postContentCommentAdd API filePath :" + filePath);
                System.out.println("postContentCommentAdd API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                attachmentLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                attachment = fileName1;
                attachmentThumb = fileName1;

            }
            
            

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    logger.info("memId ::" + memId);

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String oldCommentPointContentSQL = "SELECT  comment_content_point FROM post_content_rating_point WHERE content_id = '" + postContentId + "'";
                        String oldCommentPointContent = dbsession.createSQLQuery(oldCommentPointContentSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(oldCommentPointContentSQL).uniqueResult().toString();
                        int newCommentPointContent = (1 + Integer.parseInt(oldCommentPointContent));

                        logger.info("newCommentPointContent ::" + newCommentPointContent);
                        logger.info("oldCommentPointContent ::" + oldCommentPointContent);

                        System.out.println("newCommentPointContent ::" + newCommentPointContent);
                        System.out.println("oldCommentPointContent ::" + oldCommentPointContent);

                        String idS = getId.getID(72);
                        int postContentCommentId = Integer.parseInt(idS);

                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        String adduser = Integer.toString(memId);
                        adddate = dateFormatX.format(dateToday);
                        String customOrderby = "0";
                        String published = "0";

                        commentDate = dateFormatX.format(dateToday);
                        /*
                        //insert post_content_comment
                        postContentCommentAddSQL = dbsession.createSQLQuery("INSERT INTO post_content_comment("
                                + "comment_id,"
                                + "parent,"
                                + "commment_desc,"
                                + "content_id,"
                                + "member_id,"
                                + "commment_date,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date"
                                + ") VALUES("
                                + "'" + postContentCommentId + "',"
                                + "'" + commentParent + "',"
                                + "'" + commentDesc + "',"
                                + "'" + postContentId + "',"
                                + "'" + memId + "',"
                                + "'" + commentDate + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "',"
                                + "'" + adddate + "'"
                                + "  ) ");

                        logger.info("postContentCommentAddSQL ::" + postContentCommentAddSQL);

                        postContentCommentAddSQL.executeUpdate();
                         */
                        //prepare Statement
                        PreparedStatement ps = con.prepareStatement("INSERT INTO post_content_comment("
                                + "comment_id,"
                                + "parent,"
                                + "commment_desc,"
                                + "content_id,"
                                + "member_id,"
                                + "commment_date,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date,"
                                + "attachment_thumb,"
                                + "attachment,"
                                + "custom_order) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                        //  ps.setInt(1, Integer.parseInt(postContentRequestId));
                        ps.setInt(1, postContentCommentId);
                        ps.setInt(2, Integer.parseInt(commentParent));

                        //   ps.setString(3, commentDesc);
                        //   ps.setNString(3, commentDesc);
                        ps.setString(3, decoded);

                        ps.setInt(4, Integer.parseInt(postContentId));
                        ps.setInt(5, memId);

                        ps.setString(6, commentDate);
                        ps.setString(7, adduser);
                        ps.setString(8, adddate);
                        ps.setString(9, hostname);
                        ps.setString(10, ipAddress);
                        ps.setString(11, adddate);
                        ps.setString(12, attachmentThumb);
                        ps.setString(13, attachment);
                        ps.setString(14, String.valueOf(postContentCommentId));

                        logger.info("SQL Query: " + ps.toString());

                        ps.executeUpdate();
                        ps.close();
                        if (commentParent.equals("0")) {
                            //update post_content_rating_point Rating for comment_content_point filed
                            Query updatePostContentRatingPointSQL = dbsession.createSQLQuery("UPDATE  post_content_rating_point SET comment_content_point = '" + newCommentPointContent + "',update_time='" + adddate + "' WHERE content_id = '" + postContentId + "'");
                            updatePostContentRatingPointSQL.executeUpdate();
                        }

                        //insert post_content_comment_like
                        commentRatingAddSQL = dbsession.createSQLQuery("INSERT INTO post_content_comment_like("
                                + "id,"
                                + "comment_id,"
                                + "like_count,"
                                + "update_time"
                                + ") VALUES("
                                + "'" + postContentCommentId + "',"
                                + "'" + postContentCommentId + "',"
                                + "'0',"
                                + "'" + adddate + "'"
                                + "  ) ");

                        logger.info("commentRatingAddSQL ::" + commentRatingAddSQL);

                        commentRatingAddSQL.executeUpdate();

                        con.commit();
                        con.close();

                        //  dbtrx.commit();
                        postContentObj = new JSONObject();

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Post content comment added successfully");
                        logingObj.put("ResponseData", postContentObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
