<%-- 
    Document   : patientQueryReplyInfo
    Created on : MAY 11, 2020, 10:35:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String qryId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("qryId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        qryId = request.getParameter("qryId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);
    
    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: patientQueryInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    
    
    // String contentTotalLikeCount = "";
    //  String contentTotalCommentCount = "";
    //  String contentTotalShareCount = "";
    int totalLikeCount = 0;
    String totalLikeCountText = "";
    int totalCommentCount = 0;
    int totalShareCount = 0;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    
    
    JSONObject queryReplyInfoObj = new JSONObject();
    JSONObject queryReplyInfoResponseObj = new JSONObject();
    JSONArray queryReplyInfoObjArray = new JSONArray();

    String pQueryReplySQL = null;
    String pQueryReplyCountSQL = null;
    Query pQueryReplySQLQry = null;
    Object[] pQueryReplyObj = null;

    String pQueryReplyId = "";
    String pQueryReplyDesc = "";
    String pQueryReplyDate = "";
    String pQueryReplyCommentType = "";
    String pQueryReplyCommentTypeText = "";
    String pQueryReplyCommenterId = "";

    String pQueryReplyOwnerId = "";
    String pQueryReplyOwnerName = "";
    String pQueryReplyOwnerPic = "";
    String pQueryReplyOwnerPicLink = "";

    String attachment = "";
    String attachmentLink = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
//                        if (!filter.equals("")) {
//                            filterSQLStr = " AND pc.post_content_title LIKE '%" + filter + "%' ";
//                        } else {
//                            filterSQLStr = "";
//                        }

                        if (!filter.equals("")) {
                            filterSQLStr = " AND pq.query_desc LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM post_content pc "
//                                + "LEFT JOIN member AS m ON m.id = pc.content_owner "
//                                + "WHERE pc.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pc.id_post_content DESC";
//                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
//                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
//                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
//                                + "WHERE pq.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pq.mod_date DESC";
                        pQueryReplyCountSQL = "SELECT  count(*) FROM patient_query pq "
                                + "WHERE pq.published = 1 AND pq.parent = '" + qryId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC";

                        String numrow1 = dbsession.createSQLQuery(pQueryReplyCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pQueryReplyCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        pQueryReplySQL = "SELECT  pq.query_id, pq.query_desc,pq.query_date, "
                                + "pq.comment_type,pq.commenter_id,pq.attachment,  "
                                + "m.id pQueryReplyOwnerId,m.member_name  pQueryReplyOwnerName, m.picture_name pQueryReplyOwnerPic  "
                                + "FROM patient_query pq "
                                + "LEFT JOIN member AS m ON m.id = pq.commenter_id "
                                + "WHERE pq.published = 1 AND pq.parent = '" + qryId + "'  "
                                + "ORDER BY pq.mod_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        System.out.println("pQueryReplySQL :" + pQueryReplySQL);

                        pQueryReplySQLQry = dbsession.createSQLQuery(pQueryReplySQL);

                        for (Iterator pQueryReplyItr = pQueryReplySQLQry.list().iterator(); pQueryReplyItr.hasNext();) {

                                    pQueryReplyObj = (Object[]) pQueryReplyItr.next();

                                    pQueryReplyId = pQueryReplyObj[0].toString();
                                    pQueryReplyDesc = pQueryReplyObj[1] == null ? "" : pQueryReplyObj[1].toString();
                                    pQueryReplyDate = pQueryReplyObj[2] == null ? "" : pQueryReplyObj[2].toString();
                                    pQueryReplyCommentType = pQueryReplyObj[3] == null ? "" : pQueryReplyObj[3].toString();

                                    if (pQueryReplyCommentType.equals("1")) {
                                        pQueryReplyCommentTypeText = "Doctor";
                                    } else {
                                        pQueryReplyCommentTypeText = "Patient";
                                    }

                                    pQueryReplyCommenterId = pQueryReplyObj[4] == null ? "" : pQueryReplyObj[4].toString();

                                    attachment = pQueryReplyObj[5] == null ? "" : pQueryReplyObj[5].toString();
                                    attachmentLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + attachment;

                                    pQueryReplyOwnerId = pQueryReplyObj[6] == null ? "" : pQueryReplyObj[6].toString();
                                    pQueryReplyOwnerName = pQueryReplyObj[7] == null ? "" : pQueryReplyObj[7].toString();
                                    pQueryReplyOwnerPic = pQueryReplyObj[8] == null ? "" : pQueryReplyObj[8].toString();

                                    pQueryReplyOwnerPicLink = GlobalVariable.imageMemberDirLink + pQueryReplyOwnerPic;

                                    
                                     queryReplyInfoResponseObj = new JSONObject();

                                    queryReplyInfoResponseObj.put("pQueryReplyId", pQueryReplyId);

                                    queryReplyInfoResponseObj.put("pQueryReplyDesc", pQueryReplyDesc);
                                    queryReplyInfoResponseObj.put("pQueryReplyDate", pQueryReplyDate);
                                    queryReplyInfoResponseObj.put("pQueryReplyCommentType", pQueryReplyCommentType);
                                    queryReplyInfoResponseObj.put("pQueryReplyCommentTypeText", pQueryReplyCommentTypeText);

                                    queryReplyInfoResponseObj.put("pQueryReplyCommenterId", pQueryReplyCommenterId);

                                    queryReplyInfoResponseObj.put("attachment", attachment);
                                    queryReplyInfoResponseObj.put("attachmentLink", attachmentLink);

                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerId", pQueryReplyOwnerId);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerName", pQueryReplyOwnerName);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerPic", pQueryReplyOwnerPic);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerPicLink", pQueryReplyOwnerPicLink);

                                    queryReplyInfoObjArray.add(queryReplyInfoResponseObj);
                                    
                        

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", queryReplyInfoObjArray);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
