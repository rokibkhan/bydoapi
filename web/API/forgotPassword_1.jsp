<%-- 
    Document   : forgotPassword
    Created on : APR 29, 2020, 11:40:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("forgotPassword_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String memberMobileNumber = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("memberMobileNumber")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        memberMobileNumber = request.getParameter("memberMobileNumber").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;
    
    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberMobile = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: forgotPassword API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query qMemberAddress = null;
    Object[] mAddressObj = null;
    Object[] pAddressObj = null;

    SendSMS sendsms = new SendSMS();
    SendEmail sendemail = new SendEmail();

    RandomString randNumber = new RandomString();

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    //String pinCode = randNumber.randomString(4,2);
    String member_pass_code_phone = randNumber.randomString(4, 2);
    String member_pass_code_email = randNumber.randomString(6, 2);

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    memberName = member.getMemberName();

                    memberMobile = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();

                }

                //check New Mobile Number with old Mobile number
                if (memberMobile.equalsIgnoreCase(memberMobileNumber)) {

                    //  String smsMsg = "Your verification code is" + member_temp_pass_phone + " Email code is " + member_temp_pass_email;
                    String smsMsg = "Your IEB APP temporary password is " + member_pass_code_phone + ".Please change password after successfully login.";
                    //  String receipentMobileNo = "+8801755656354";
                    String receipentMobileNo = memberMobile;
                    logger.info("API :: SMS : " + smsMsg);
                    logger.info("API :: receipentMobileNo : " + receipentMobileNo);
                    //    System.out.println(SendSMS(sms, receipentMobileNo));

                    String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);

                    logger.info("API :: sendsms1::" + sendsms1);

                    //update password in system
                    String newPassEnc = new Encryption().getEncrypt(member_pass_code_phone);

                    Query passUpdateSQL = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + newPassEnc + "',update_time=now(),status= '2' WHERE member_id='" + memId + "'");
                    logger.info("API :: forgotPassword API passUpdateSQL :: " + passUpdateSQL);
                    passUpdateSQL.executeUpdate();
                    //  dbtrx.commit();

                    responseObj = new JSONObject();
                    responseObj.put("memId", memId);
                    responseObj.put("memberId", memberId);
                    responseObj.put("memberName", memberName);
                    responseObj.put("memberEmail", memberEmail);
                    responseObj.put("memberMobile", memberMobile);
                    responseObj.put("sendSMSId", sendsms1);

                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "1");
                    logingObj.put("ResponseText", "SMS sent successfully");
                    logingObj.put("ResponseData", responseObj);

                } else {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "Member mobile number not match ");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("User ID :" + memberId + " Not Found");

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Member not found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
