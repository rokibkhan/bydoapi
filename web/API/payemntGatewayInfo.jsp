<%-- 
    Document   : payemntGatewayInfo
    Created on : OCT 31, 2019, 1:32:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("payemntGatewayInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    
    
    
    // Consumer consumer = null;
    Member member = null;

    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: payemntGatewayInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    Query paymentGatewaySQL = null;
    Object[] paymentGatewayObj = null;

    String SSLCommerzStoreId = "";
    String SSLCommerzStorePass = "";
    String SSLCommerzStoreMode = "";

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //   SSLCommerzStoreId = GlobalVariable.SSLCommerzStoreId;
                        //   SSLCommerzStorePass = GlobalVariable.SSLCommerzStorePass;
                        //   SSLCommerzStoreMode = GlobalVariable.SSLCommerzStoreMode;
                        paymentGatewaySQL = dbsession.createSQLQuery("SELECT * FROM payment_gatway where id='1'");

                        for (Iterator paymentGatewayItr = paymentGatewaySQL.list().iterator(); paymentGatewayItr.hasNext();) {

                            paymentGatewayObj = (Object[]) paymentGatewayItr.next();
                            
                            SSLCommerzStoreId = paymentGatewayObj[1] == null ? "" : paymentGatewayObj[1].toString();
                            SSLCommerzStorePass = paymentGatewayObj[2] == null ? "" : paymentGatewayObj[2].toString();
                            SSLCommerzStoreMode = paymentGatewayObj[3] == null ? "" : paymentGatewayObj[3].toString();

                        }

                        postContentObj = new JSONObject();
                        postContentObj.put("storeId", SSLCommerzStoreId);
                        postContentObj.put("storePassword", SSLCommerzStorePass);
                        postContentObj.put("storeMode", SSLCommerzStoreMode);

                        postContentObjArr.add(postContentObj);

                        responseObj = new JSONObject();
                        responseObj.put("ResponseCode", "1");
                        responseObj.put("ResponseText", "Payment Gateway Info Found");
                        responseObj.put("ResponseData", postContentObjArr);

                    } else {

                        responseObj = new JSONObject();
                        responseObj.put("ResponseCode", "2");
                        responseObj.put("ResponseText", "Member password wrong");
                        responseObj.put("ResponseData", postContentObj);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {

                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "0");
                responseObj.put("ResponseText", "Member not found");
                responseObj.put("ResponseData", postContentObj);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            responseObj = new JSONObject();
            responseObj.put("ResponseCode", "0");
            responseObj.put("ResponseText", "Something went wrong!");
            responseObj.put("ResponseData", postContentObj);

        }

        PrintWriter writer = response.getWriter();
        writer.write(responseObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        responseObj = new JSONObject();
        responseObj.put("ResponseCode", "999");
        responseObj.put("ResponseText", "Key Validation Failed");
        responseObj.put("ResponseData", logingObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
