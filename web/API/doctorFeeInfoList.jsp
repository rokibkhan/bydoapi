<%-- 
    Document   : doctorFeeInfoList
    Created on : Nov 15, 2020, 2:43:21 PM
    Author     : ROKIB
--%>



<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorFeeInfoList.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String doctorId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();
        doctorId = request.getParameter("doctorId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query doctorFeeSQL = null;
    Object doctorFeeObj[] = null;

    String doctorFee = "";
    String feeStartDate = "";
    String feeEndDate = "";
    
    JSONArray doctorFeeResponseObjArr = new JSONArray();
    JSONObject doctorFeeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String[] starttimesplit;
    String[] endtimesplit;
    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    if (rec == 1) {

        try {

            doctorFeeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_fee_info where doctor_id = '"+doctorId +"' ORDER BY id_fee ASC");

            if (!doctorFeeSQL.list().isEmpty()) {
                for (Iterator it = doctorFeeSQL.list().iterator(); it.hasNext();) {

                    doctorFeeObj = (Object[]) it.next();
                    doctorFee = doctorFeeObj[2].toString();
                    feeStartDate = doctorFeeObj[3].toString();
                    feeEndDate = doctorFeeObj[4].toString();
                    
                    starttimesplit = feeStartDate.split(" ");
                    endtimesplit = feeEndDate.split(" ");

                    doctorFeeResponseObj = new JSONObject();

                    doctorFeeResponseObj.put("doctorId", doctorId);
                    doctorFeeResponseObj.put("doctorFee", Math.round(Double.parseDouble(doctorFee)));
                    doctorFeeResponseObj.put("feeStartDate", starttimesplit[0]);
                    doctorFeeResponseObj.put("feeEndDate", endtimesplit[0]);

                    doctorFeeResponseObjArr.add(doctorFeeResponseObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", doctorFeeResponseObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", doctorFeeResponseObjArr);

            }
        } catch (Exception e) {

        } 
        
//        finally {
//            dbtrx.commit();
//
//        }

        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", doctorFeeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>