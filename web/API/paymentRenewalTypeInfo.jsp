<%-- 
    Document   : paymentRenewalTypeInfo
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: paymentRenewalTypeInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject feeYearObj = new JSONObject();
    JSONArray feeYearObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String renewalTypeId_1 = "";
    String renewalTypeName_1 = "";

    String renewalTypeId_2 = "";
    String renewalTypeName_2 = "";

    String renewalTypeId_3 = "";
    String renewalTypeName_3 = "";

    String renewalTypeId_5 = "";
    String renewalTypeName_5 = "";

    String renewalTypeId_10 = "";
    String renewalTypeName_10 = "";

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        SimpleDateFormat dateFormatToday = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat dateFormatM = new SimpleDateFormat("MM");
                        SimpleDateFormat dateFormatY = new SimpleDateFormat("yyyy");
                        Date dateToday = new Date();
                        String todayDate = dateFormatToday.format(dateToday);

                        String todayMonth = dateFormatM.format(dateToday);
                       //      String todayMonth = "1";

                        String todayYear = dateFormatY.format(dateToday);

                        String todayMemberFeesYearNameId = "";
                        String todayMemberFeesYearName = "";

                        String todayNextMemberFeesYearNameId = "";
                        String todayNextMemberFeesYearName = "";
                        int todayNextMemberFeesYearName1 = 0;
                        int todayNextMemberFeesYearName2 = 0;
                        String todayNextFeeYearSQL = "";

                        String todayNextTwoMemberFeesYearNameId = "";
                        String todayNextTwoMemberFeesYearName = "";
                        int todayNextTwoMemberFeesYearName1 = 0;
                        int todayNextTwoMemberFeesYearName2 = 0;
                        String todayNextTwoFeeYearSQL = "";

                        String todayNextThreeMemberFeesYearNameId = "";
                        String todayNextThreeMemberFeesYearName = "";
                        int todayNextThreeMemberFeesYearName1 = 0;
                        int todayNextThreeMemberFeesYearName2 = 0;
                        String todayNextThreeFeeYearSQL = "";

                        String todayNextFiveMemberFeesYearNameId = "";
                        String todayNextFiveMemberFeesYearName = "";
                        int todayNextFiveMemberFeesYearName1 = 0;
                        int todayNextFiveMemberFeesYearName2 = 0;
                        String todayNextFiveFeeYearSQL = "";

                        String todayMonthCheck = "";
                        int currentYearValue = 0;

                        if (Integer.parseInt(todayMonth) <= 6) {
                            todayMonthCheck = "1-6-Month-January-June";
                            currentYearValue = Integer.parseInt(todayYear) - 1;
                            todayMemberFeesYearName = currentYearValue + "-" + todayYear;

                            todayNextMemberFeesYearName1 = currentYearValue + 1;
                            todayNextMemberFeesYearName2 = currentYearValue + 2;
                            todayNextMemberFeesYearName = todayNextMemberFeesYearName1 + "-" + todayNextMemberFeesYearName2;
                            todayNextFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextMemberFeesYearName + "'";
                            todayNextMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult().toString();

                            todayNextTwoMemberFeesYearName1 = currentYearValue + 2;
                            todayNextTwoMemberFeesYearName2 = currentYearValue + 3;
                            todayNextTwoMemberFeesYearName = todayNextTwoMemberFeesYearName1 + "-" + todayNextTwoMemberFeesYearName2;
                            todayNextTwoFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextTwoMemberFeesYearName + "'";
                            todayNextTwoMemberFeesYearNameId = dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult().toString();

                            todayNextThreeMemberFeesYearName1 = currentYearValue + 3;
                            todayNextThreeMemberFeesYearName2 = currentYearValue + 4;
                            todayNextThreeMemberFeesYearName = todayNextThreeMemberFeesYearName1 + "-" + todayNextThreeMemberFeesYearName2;
                            todayNextThreeFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextThreeMemberFeesYearName + "'";
                            todayNextThreeMemberFeesYearNameId = dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult().toString();

                            todayNextFiveMemberFeesYearName1 = currentYearValue + 5;
                            todayNextFiveMemberFeesYearName2 = currentYearValue + 6;
                            todayNextFiveMemberFeesYearName = todayNextFiveMemberFeesYearName1 + "-" + todayNextFiveMemberFeesYearName2;
                            todayNextFiveFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextFiveMemberFeesYearName + "'";
                            todayNextFiveMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult().toString();

                        }

                        if (Integer.parseInt(todayMonth) > 6 && Integer.parseInt(todayMonth) <= 12) {
                            todayMonthCheck = "7-12-Month-July-December";
                            currentYearValue = Integer.parseInt(todayYear) + 1;
                            todayMemberFeesYearName = todayYear + "-" + currentYearValue;

                            todayNextMemberFeesYearName1 = currentYearValue;
                            todayNextMemberFeesYearName2 = currentYearValue + 1;
                            todayNextMemberFeesYearName = todayNextMemberFeesYearName1 + "-" + todayNextMemberFeesYearName2;
                            todayNextFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextMemberFeesYearName + "'";                            
                            todayNextMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFeeYearSQL).uniqueResult().toString();

                            todayNextTwoMemberFeesYearName1 = currentYearValue + 1;
                            todayNextTwoMemberFeesYearName2 = currentYearValue + 2;
                            todayNextTwoMemberFeesYearName = todayNextTwoMemberFeesYearName1 + "-" + todayNextTwoMemberFeesYearName2;
                            todayNextTwoFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextTwoMemberFeesYearName + "'";                            
                            todayNextTwoMemberFeesYearNameId = dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextTwoFeeYearSQL).uniqueResult().toString();
                            
                            todayNextThreeMemberFeesYearName1 = currentYearValue + 2;
                            todayNextThreeMemberFeesYearName2 = currentYearValue + 3;
                            todayNextThreeMemberFeesYearName = todayNextThreeMemberFeesYearName1 + "-" + todayNextThreeMemberFeesYearName2;
                            todayNextThreeFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextThreeMemberFeesYearName + "'";
                            todayNextThreeMemberFeesYearNameId = dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextThreeFeeYearSQL).uniqueResult().toString();

                            todayNextFiveMemberFeesYearName1 = currentYearValue + 4;
                            todayNextFiveMemberFeesYearName2 = currentYearValue + 5;
                            todayNextFiveMemberFeesYearName = todayNextFiveMemberFeesYearName1 + "-" + todayNextFiveMemberFeesYearName2;
                            todayNextFiveFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayNextFiveMemberFeesYearName + "'";
                            todayNextFiveMemberFeesYearNameId = dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayNextFiveFeeYearSQL).uniqueResult().toString();

                        }

                        String todayFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayMemberFeesYearName + "'";
                        logger.info("todayFeeYearSQL :: " + todayFeeYearSQL);
                        todayMemberFeesYearNameId = dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult().toString();

                        renewalTypeId_1 = "1";
                        renewalTypeName_1 = "1 Year";
                        feeYearObj = new JSONObject();
                        feeYearObj.put("RenewalTypeId", renewalTypeId_1);
                        feeYearObj.put("RenewalTypeName", renewalTypeName_1);

//                        feeYearObj.put("TodayMonthCheck", todayMonthCheck);
//                        feeYearObj.put("TodayYear", todayYear);
//                        feeYearObj.put("CurrentYearValue", currentYearValue);
//
//                        feeYearObj.put("TodayMemberFeesYearNameId", todayMemberFeesYearNameId);
//                        feeYearObj.put("TodayMemberFeesYearName", todayMemberFeesYearName);
                        //  feeYearObj.put("TodayNextMemberFeesYearId", todayNextMemberFeesYearNameId);
                        //   feeYearObj.put("TodayNextMemberFeesYearName", todayNextMemberFeesYearName);
                        feeYearObj.put("RenewalFeeYearId", todayNextMemberFeesYearNameId);
                        feeYearObj.put("RenewalFeeYearName", todayNextMemberFeesYearName);
                        feeYearObjArr.add(feeYearObj);

                        renewalTypeId_2 = "2";
                        renewalTypeName_2 = "2 Year";
                        feeYearObj = new JSONObject();
                        feeYearObj.put("RenewalTypeId", renewalTypeId_2);
                        feeYearObj.put("RenewalTypeName", renewalTypeName_2);

                        feeYearObj.put("RenewalFeeYearId", todayNextTwoMemberFeesYearNameId);
                        feeYearObj.put("RenewalFeeYearName", todayNextTwoMemberFeesYearName);

                        feeYearObjArr.add(feeYearObj);

                        renewalTypeId_3 = "3";
                        renewalTypeName_3 = "3 Year";
                        feeYearObj = new JSONObject();
                        feeYearObj.put("RenewalTypeId", renewalTypeId_3);
                        feeYearObj.put("RenewalTypeName", renewalTypeName_3);

                        feeYearObj.put("RenewalFeeYearId", todayNextThreeMemberFeesYearNameId);
                        feeYearObj.put("RenewalFeeYearName", todayNextThreeMemberFeesYearName);

                        feeYearObjArr.add(feeYearObj);

                        renewalTypeId_5 = "5";
                        renewalTypeName_5 = "5 Year";
                        feeYearObj = new JSONObject();
                        feeYearObj.put("RenewalTypeId", renewalTypeId_5);
                        feeYearObj.put("RenewalTypeName", renewalTypeName_5);

                        feeYearObj.put("RenewalFeeYearId", todayNextFiveMemberFeesYearNameId);
                        feeYearObj.put("RenewalFeeYearName", todayNextFiveMemberFeesYearName);

                        feeYearObjArr.add(feeYearObj);

//                        renewalTypeId_10 = "10";
//                        renewalTypeName_10 = "Life";
//                        feeYearObj = new JSONObject();
//                        feeYearObj.put("RenewalTypeId", renewalTypeId_10);
//                        feeYearObj.put("RenewalTypeName", renewalTypeName_10);
//                        feeYearObjArr.add(feeYearObj);
                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", feeYearObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
