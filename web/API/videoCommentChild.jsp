<%-- 
    Document   : videoCommentChild
    Created on : Nov 27, 2020, 4:05:02 PM
    Author     : Nahid
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("postContentCommentChildInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String postContentId = "";
    String postCommentId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("postContentId") && request.getParameterMap().containsKey("postCommentId") && request.getParameterMap().containsKey("currentpage")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        postContentId = request.getParameter("postContentId").trim();
        postCommentId = request.getParameter("postCommentId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: postContentCommentChildInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    JSONObject commentChildInfoResponseObj = new JSONObject();
    JSONArray commentChildInfoObjArray = new JSONArray();
    String commentChildCountSQL = null;
    String commentChildSQL = null;
    Query commentChildSQLQry = null;
    Object[] commentChildObj = null;

    String commentChildId = "";
    String commentChildParent = "";
    String commmentChildDesc = "";
    String commmentChildDate = "";
    String commmentChildPinedStatus = "";
    String commmentChildCustomOrder = "";

    String commmentChildAttachment = "";
    String commmentChildAttachmentLink = "";
    String commmentChildAttachmentThumb = "";
    String commmentChildAttachmentThumbLink = "";

    String commmenterChildId = "";
    String commmenterChildName = "";
    String commmenterChildPicture = "";
    String commmenterChildPictureLink = "";
    String commmenterChildType = "";
    String commmenterChildTypeName = "";

    String commentChildLikeCount = "";
    String commentChildLiked = "";
    String commentChildLikedText = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        commentChildCountSQL = "SELECT count(*) FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "WHERE cm.parent = '" + postCommentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC";

                        String numrow1 = dbsession.createSQLQuery(commentChildCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(commentChildCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 5;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;
                        rowsperpage += offset;
                        String commentId = postCommentId;
                        String commentSQL="";
                        Query commentSQLQry;
                        
                        String commentCountSQL = dbsession.createSQLQuery("SELECT count(*) FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ").uniqueResult() == null ? "0" : dbsession.createSQLQuery("SELECT count(*) FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ").uniqueResult().toString();
  
                          if(commentCountSQL.equals("0"))
                          {
                              commentChildInfoObjArray = new JSONArray();
                          }      
                         else{
                              System.out.println("___________________________________sssssssss_________________________ "+ offset +" "+rowsperpage);
                                
                                commentSQL = "SELECT cm.id "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                     
                        List Childcmid = commentSQLQry.list();
                        
                   commentSQL = "SELECT cm.parent "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                               + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                               + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmparent = commentSQLQry.list();  
                       
                       commentSQL = "SELECT cm.commment_desc "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                               + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmcommment_desc = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT cm.commment_date "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                               + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmcommment_date = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT m.id "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                               + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                               + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childmid = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT m.member_name "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                            + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       // System.out.println("_____________________________________KAJ KORsdcasE d NAAAA __________________________________________________");    
                        List Childmmember_name = commentSQLQry.list();
                   commentSQL = "SELECT m.picture_name "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                              + " WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                               + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childmpicture_name = commentSQLQry.list();  
                       
                       commentSQL = "SELECT mt.member_type_id "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childmtmember_type_id = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT mti.member_type_name "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                               + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childmtimember_type_name = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT m.mobile "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                               + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childmmobile = commentSQLQry.list(); 
                        
                       
                       commentSQL = "SELECT cm.like_count "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                              + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmlike_count = commentSQLQry.list(); 
                       
                       commentSQL = "SELECT cm.isaccepted "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmisaccepted = commentSQLQry.list(); 
                       
                       
                       commentSQL = "SELECT cm.picture_link "
                                + "FROM video_comment cm "
                                + "LEFT JOIN member AS m ON m.id = cm.commment_from  "
                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                + "WHERE cm.parent = '" + commentId + "' AND cm.video_id = '" + postContentId + "'  "
                                + "ORDER BY cm.commment_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        commentSQLQry = dbsession.createSQLQuery(commentSQL);
                       List Childcmpiclink = commentSQLQry.list(); 
                               
                                    for (int j=0;j<Childcmisaccepted.size();j++) {
                                            System.out.println("_____________________________________KAJ KORse Baccha __________________________________________________");    
                        
                                       // commentChildObj = (Object[]) commentChildItr.next();

                                        commentChildId = Childcmid.get(j).toString();
                                        commentChildParent = Childcmparent.get(j).toString();
                                        commmentChildDesc = Childcmcommment_desc.get(j).toString();
                                        commmentChildDate = Childcmcommment_date.get(j).toString();

                                        commmenterChildId = Childmid.get(j).toString();
                                        commmenterChildName =  Childmmember_name.get(j).toString();

                                        commmenterChildPicture = Childmpicture_name.get(j).toString();
                                        commmenterChildPictureLink = GlobalVariable.imageMemberDirLink + commmenterChildPicture;

                                        commmenterChildType = Childmtmember_type_id.get(j).toString();
                                        commmenterChildTypeName = Childmtimember_type_name.get(j).toString();

//                                        commmentChildAttachment = commentChildObj[14] == null ? "" : commentChildObj[14].toString();
//                                        commmentChildAttachmentLink = GlobalVariable.baseUrlImg + "/upload/postcontent/" + commmentChildAttachment;
//
//                                        commmentChildAttachmentThumb = commentChildObj[15] == null ? "" : commentChildObj[15].toString();
//                                        commmentChildAttachmentThumbLink = GlobalVariable.baseUrlImg + "/upload/postcontent/" + commmentChildAttachmentThumb;

                                        commentChildLikeCount = Childcmlike_count.get(j).toString();
                                        
                                       String commentChildIsAccepted = Childcmisaccepted.get(j).toString();
                                       String commentChildPictureLink = Childcmpiclink.get(j).toString();

                                        //check user rating history point already exits or not
                                        String commentChildLikedHostorySQL = "SELECT  count(*) FROM video_comment_like_history  "
                                                + "WHERE member_id = '" + memId + "' AND comment_id = '" + commentChildId + "'";

                                        String commentChildLikedHostoryRows = dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult().toString();
                                        //   int numrows = Integer.parseInt(numrow1);

                                        logger.info("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);
                                        System.out.println("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);

                                        if (commentChildLikedHostoryRows.equals("0")) {
                                            commentChildLiked = "0";
                                            commentChildLikedText = "No";
                                        } else {
                                            commentChildLikedHostorySQL = "SELECT  status FROM video_comment_like_history  "
                                                + "WHERE member_id = '" + memId + "' AND comment_id = '" + commentChildId + "'";

                                            Query stu = dbsession.createSQLQuery(commentChildLikedHostorySQL);
                                            List rs = stu.list();
                                             int ck=0;
                                            String s = rs.get(0).toString();
                                            ck = Integer.parseInt(s);
                                            if(ck==1)
                                            {
                                                commentChildLiked = "1";
                                                commentChildLikedText = "Yes";
                                            }
                                            else 
                                            {
                                                commentChildLiked = "0";
                                                commentChildLikedText = "No";
                                            }
                                            
                                        }
                                        
                                        commentChildLikedHostorySQL = "SELECT  count(*) FROM member_point  "
                                                + "WHERE member_id = '" + memId + "'";

                                        commentChildLikedHostoryRows = dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult().toString();
                                        //   int numrows = Integer.parseInt(numrow1);

                                        logger.info("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);
                                        System.out.println("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);
                                        String commmenterChildRating;
                                        if (commentChildLikedHostoryRows.equals("0")) {
                                            commmenterChildRating = "0";
                                        } else {
                                            commentChildLikedHostorySQL = "SELECT  rating_point  FROM member_point  "
                                                + "WHERE member_id = '" + memId + "'";

                                            Query stu = dbsession.createSQLQuery(commentChildLikedHostorySQL);
                                            List rs = stu.list();
                                             int ck=0;
                                            String s = rs.get(0).toString();
                                            commmenterChildRating = s;
                                            
                                        }

                                        commentChildInfoResponseObj = new JSONObject();
                                        commentChildInfoResponseObj.put("commentChildId", commentChildId);

                                        commentChildInfoResponseObj.put("commentChildLikeCount", commentChildLikeCount);
                                        commentChildInfoResponseObj.put("commentChildIsAccepted", commentChildIsAccepted);
                                        
                                        commentChildInfoResponseObj.put("commentChildPictureLink", GlobalVariable.baseUrlImg2 + "/upload/videocomment/" +commentChildPictureLink);
                                        
                                        commentChildInfoResponseObj.put("commentChildLiked", commentChildLiked);
                                        commentChildInfoResponseObj.put("commentChildLikedText", commentChildLikedText);

                                        commentChildInfoResponseObj.put("commentChildParent", commentChildParent);
                                        commentChildInfoResponseObj.put("commmentChildDesc", commmentChildDesc);
                                        commentChildInfoResponseObj.put("commmentChildDate", commmentChildDate);
//                                        commentChildInfoResponseObj.put("commmentChildPinedStatus", commmentChildPinedStatus);

//                                        commentChildInfoResponseObj.put("commmentChildAttachment", commmentChildAttachment);
//                                        commentChildInfoResponseObj.put("commmentChildAttachmentLink", commmentChildAttachmentLink);
//                                        commentChildInfoResponseObj.put("commmentChildAttachmentThumb", commmentChildAttachmentThumb);
//                                        commentChildInfoResponseObj.put("commmentChildAttachmentThumbLink", commmentChildAttachmentThumbLink);

                                        commentChildInfoResponseObj.put("commmenterChildId", commmenterChildId);
                                        commentChildInfoResponseObj.put("commmenterChildName", commmenterChildName);
                                        commentChildInfoResponseObj.put("commmenterChildPicture", commmenterChildPicture);
                                        commentChildInfoResponseObj.put("commmenterChildPictureLink", commmenterChildPictureLink);
                                        commentChildInfoResponseObj.put("commmenterChildType", commmenterChildType);
                                        commentChildInfoResponseObj.put("commmenterChildTypeName", commmenterChildTypeName);
                                        commentChildInfoResponseObj.put("commmenterChildRating", commmenterChildRating);
                                        //commentInfoResponseObj.put("CommentChildInfo", commentChildInfoObjArray);

                                commentChildInfoObjArray.add(commentChildInfoResponseObj);

                            }
                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", commentChildInfoObjArray);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    dbsession.flush();
    dbsession.close();

%>
