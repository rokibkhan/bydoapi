<%-- 
    Document   : doctorListCategoryWise
    Created on : June 26, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorListCategoryWise_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorCategoryId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        doctorCategoryId = request.getParameter("doctorCategoryId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    String memberXId = "";
    String memberXIEBId = "";
    String memberXName = "";
    String memberXPhone1 = "";
    String memberXPhone2 = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";
    String memberCustomOrder = "";
    String doctorRegNo = "";
    String memberAddress = "";
    String memberThana = "";
    String memberDistrict = "";
    
    String memberFullAddress = "";

    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorMedicalCollege = "";

    String doctorCategoryName = "";

    Query mDoctorFeeSQL = null;
    Object[] mDoctorFeeObj = null;

    Query mDoctorDiscountSQL = null;
    Object[] mDoctorDiscountObj = null;

    String doctorDiscountId = "";
    String doctorDiscountType = "";
    String doctorDiscountTypeText = "";
    String doctorDiscountFixedAmount = "";
    String doctorDiscountPercentageAmount = "";

    String doctorDiscountTD = "";
    String doctorDiscountED = "";

    String baseFee = "";
    String discountType = ""; //fixed //percentage
    String discountAmount = "";
    String totalFee = "";
    String totalWithDiscount = "";
    String totalWithoutDiscount = "";
    String doctorBaseFee = "";

    String doctorFeeId = "";
    String doctorFeeAmount = "";
    String doctorFeeTD = "";
    String doctorFeeED = "";

    Double doctorActualFee = 0.0d;
    Double doctorDiscontPercentageFee = 0.0d;

    String doctorRating = "5.0";
    String doctorContributionRating = "100K";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);
    Date dateToday = new Date();
    String today = new SimpleDateFormat("yyyy-MM-dd").format(dateToday);  
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorListCategoryWise API rec :: " + rec);

    if (rec == 1) {

        try {

            memberSQL = dbsession.createSQLQuery("SELECT m.id, m.member_id,m.member_name, "
                    + "m.mobile,m.email_id, m.phone1,m.phone2,co.custom_order,"
                    + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                    + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege,"
                    + "m.picture_name, ab.ADDRESS_1 , t.THANA_NAME,d.DISTRICT_NAME "
                    + "FROM  member m Left join "
                    + " doctor_category dc on m.id = dc.member_id "
                    + " Left join member_custom_order co on m.id = co.member_id "
                    + " Left join member_additional_info ai on  m.id = ai.member_id "
                    + " Left join member_address ma on m.id = ma.member_id"
                    + " Left join address_book ab on ma.address_id = ab.ID "
                    + " Left join thana t on ab.THANA_ID = t.ID "
                    + " Left join district d on ab.DISTRICT_ID = d.ID"
                    + "WHERE m.status = 1 "
                    + "AND dc.doctor_category_id = " + doctorCategoryId + " "
                    // + "AND m.id = dc.member_id "
                    //+ "AND m.id = co.member_id "
                    //+ "AND m.id = ai.member_id "
                    //+ "AND m.id = ma.member_id "
                    //+ "AND ma.address_id = ab.ID "
                    //+ "AND ab.THANA_ID = t.ID "
                    //+ "AND ab.DISTRICT_ID = d.ID "
                    + " AND ma.address_type = 'P' "
                    + "ORDER BY co.custom_order ASC");

            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                for (Iterator it = memberSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    memberXId = memberObj[0].toString();
                    memberXIEBId = memberObj[1] == null ? "" : memberObj[1].toString();
                    memberXName = memberObj[2] == null ? "" : memberObj[2].toString();

                    memberXMobile = memberObj[3] == null ? "" : memberObj[3].toString();
                    memberXEmail = memberObj[4] == null ? "" : memberObj[4].toString();
                    memberXPhone1 = memberObj[5] == null ? "" : memberObj[5].toString();
                    memberXPhone2 = memberObj[6] == null ? "" : memberObj[6].toString();
                    memberCustomOrder = memberObj[7] == null ? "" : memberObj[7].toString();

                    doctorDegree0 = memberObj[8] == null ? "" : memberObj[8].toString();
                    doctorDegree1 = memberObj[9] == null ? "" : memberObj[9].toString();
                    doctorDegree2 = memberObj[10] == null ? "" : memberObj[10].toString();
                    doctorDegree3 = memberObj[11] == null ? "" : memberObj[11].toString();
                    doctorDegree4 = memberObj[12] == null ? "" : memberObj[12].toString();

                    doctorRegNo = memberObj[13] == null ? "" : memberObj[13].toString();
                    doctorMedicalCollege = memberObj[14] == null ? "" : memberObj[14].toString();

                    memberXPictureName = memberObj[15] == null ? "" : memberObj[15].toString();
                    
                    memberAddress = memberObj[16] == null ? "" : memberObj[16].toString() + ", ";
                    
                    memberThana = memberObj[17] == null ? "" : memberObj[17].toString() + ", ";
                    
                    memberDistrict = memberObj[18] == null ? "" : memberObj[18].toString()+ ".";
                    
                    memberFullAddress = memberAddress  + memberDistrict;

                    memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                    logger.info("API :: memberInfo API pictureLink ::" + memberXPictureLink);

                    memberContentObj = new JSONObject();
                    memberContentObj.put("Id", memberXId);

                    memberContentObj.put("DoctorCategoryId", doctorCategoryId);
                    //   memberContentObj.put("DoctorCategoryName", doctorCategoryName);
                    memberContentObj.put("DoctorDegree0", doctorDegree0);
                    memberContentObj.put("DoctorDegree1", doctorDegree1);
                    memberContentObj.put("DoctorDegree2", doctorDegree2);
                    memberContentObj.put("DoctorDegree3", doctorDegree3);
                    memberContentObj.put("DoctorDegree4", doctorDegree4);

                    memberContentObj.put("DoctorRegNo", doctorRegNo);
                    memberContentObj.put("DoctorMedicalCollege", doctorMedicalCollege);

                    memberContentObj.put("doctorRating", doctorRating);
                    memberContentObj.put("doctorContributionRating", doctorContributionRating);

                    System.out.println("today:"+today);
                    //doctor fee 
                    mDoctorFeeSQL = dbsession.createSQLQuery("SELECT id_fee,fee_amount,ed,td FROM doctor_fee_info WHERE doctor_id='" + memberXId + "'"
                    + " and ed <='" 
                            +today +"' and td >= '" + today + "'");
                    if (!mDoctorFeeSQL.list().isEmpty()) {
                        for (Iterator mDoctorFeeItr = mDoctorFeeSQL.list().iterator(); mDoctorFeeItr.hasNext();) {

                            mDoctorFeeObj = (Object[]) mDoctorFeeItr.next();
                            doctorFeeId = mDoctorFeeObj[0].toString();
                            doctorFeeAmount = mDoctorFeeObj[1].toString();
                            doctorFeeTD = mDoctorFeeObj[2].toString();
                            doctorFeeED = mDoctorFeeObj[3].toString();

                        }
                    } else {

                        doctorFeeAmount = "0.00";
                        doctorFeeTD = "";
                        doctorFeeED = "";
                    }
                    // memberContentObj.put("baseFeeId", doctorFeeId);
                    memberContentObj.put("baseFeeAmount", Math.round(Double.parseDouble(doctorFeeAmount)));
                    //        memberContentObj.put("baseFeeAmount", doctorFeeAmount);
           
                    memberContentObj.put("doctorFeeTD", doctorFeeTD);
                    memberContentObj.put("doctorFeeED", doctorFeeED);

                    //doctor discount
                    mDoctorDiscountSQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,TD,ED FROM doctor_discount WHERE doctor_id='" + memberXId + "'"
                    + " and ED <='" 
                    +today +"' and TD >= '" + today + "'");
                    if (!mDoctorDiscountSQL.list().isEmpty()) {
                        for (Iterator mDoctorDiscountItr = mDoctorDiscountSQL.list().iterator(); mDoctorDiscountItr.hasNext();) {

                            mDoctorDiscountObj = (Object[]) mDoctorDiscountItr.next();
                            doctorDiscountId = mDoctorDiscountObj[0].toString();
                            doctorDiscountType = mDoctorDiscountObj[1] == null ? "" : mDoctorDiscountObj[1].toString();

                            doctorDiscountFixedAmount = mDoctorDiscountObj[2] == null ? "" : mDoctorDiscountObj[2].toString();
                            doctorDiscountPercentageAmount = mDoctorDiscountObj[3] == null ? "" : mDoctorDiscountObj[3].toString();

                            if (doctorDiscountType.equals(1)) {
                                doctorDiscountTypeText = "PercentageAmount";

                                doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountPercentageAmount);
                                doctorDiscontPercentageFee = ((Double.parseDouble(doctorDiscountPercentageAmount)/ Double.parseDouble(doctorFeeAmount)) * 100);

                            } else {
                                doctorDiscountTypeText = "FixedAmount";
                                doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountFixedAmount);
                            }

                            doctorDiscountTD = mDoctorDiscountObj[4] == null ? "" : mDoctorDiscountObj[4].toString();
                            doctorDiscountED = mDoctorDiscountObj[5] == null ? "" : mDoctorDiscountObj[5].toString();

                        }
                    } else {

                        doctorDiscountType = "2";
                        doctorDiscountTypeText = "NoDiscount";
                        doctorDiscountFixedAmount = "0.00";
                        doctorDiscountPercentageAmount = "0.00";
                        doctorDiscountTD = "";
                        doctorDiscountED = "";
                        doctorActualFee = Double.parseDouble(doctorFeeAmount);

                    }

                    String doctorActualFee1 = Double.toString(doctorActualFee);

                    memberContentObj.put("discountType", doctorDiscountType);
                    memberContentObj.put("discountTypeText", doctorDiscountTypeText);
                    memberContentObj.put("discountFixedAmount", Math.round(Double.parseDouble(doctorDiscountFixedAmount)));
                    memberContentObj.put("discountPercentageAmount", Math.round(Double.parseDouble(doctorDiscountPercentageAmount)));
                    memberContentObj.put("discontPercentageFee", Math.round(doctorDiscontPercentageFee));
                    memberContentObj.put("doctorDiscountTD", doctorDiscountTD);
                    memberContentObj.put("doctorDiscountED", doctorDiscountED);

                    memberContentObj.put("doctorActualFee", Math.round(doctorActualFee));
                    memberContentObj.put("doctorActualFee1", doctorActualFee1);
                    //  memberContentObj.put("totalWithDiscount", totalWithDiscount);
                    //   memberContentObj.put("totalWithOutDiscount", totalWithoutDiscount);

                    memberContentObj.put("MemberFullAddress", memberFullAddress);
                    memberContentObj.put("MemberId", memberXIEBId);
                    memberContentObj.put("Name", memberXName);
                    memberContentObj.put("Phone1", memberXPhone1);

                    memberContentObj.put("Phone2", memberXPhone2);

                    memberContentObj.put("Mobile", memberXMobile);

                    memberContentObj.put("Email", memberXEmail);

                    memberContentObj.put("MemberCustomOrder", memberCustomOrder);
                    memberContentObj.put("Picture", memberXPictureLink);

                    memberContentObjArr.add(memberContentObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", memberContentObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>