<%-- 
    Document   : getProfile
    Created on : APR 28, 2020, 11:02:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getProfile_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: getProfile rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();
    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query qMemberAddress = null;
    Object[] mAddressObj = null;
    Object[] pAddressObj = null;

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";
    String unitName = "";
    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";

    if (rec == 1) {

        try {

            //   qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "' and status='1'  ");
            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();;
                    memId = member.getId();

                    logger.info("API :: GetProfile API memId ::" + memId);

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    logger.info("API :: GetProfile API memberKeySQL ::" + memberKeySQL);

                    dbPass = memberKeySQL.uniqueResult().toString();

                    logger.info("API :: GetProfile API dbPass ::" + dbPass);

                    memberName = member.getMemberName();
                    fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                    motherName = member.getMotherName() == null ? "" : member.getMotherName();

                    dob = member.getDob();
                    gender = member.getGender() == null ? "" : member.getGender().trim();
                    if (gender.equals("1")) {
                        gender = "Male";
                    } else {
                        gender = "Female";
                    }
                    mobileNo = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                    bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                    unitName = member.getUnitName() == null ? "" : member.getUnitName();
                    nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();

                    Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + memId + "'");

                    logger.info("API :: Login API memberOrganizationSQL ::" + mOrganizationSQL);

                    if (!mOrganizationSQL.list().isEmpty()) {
                        for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {
                            //   addressBook = (AddressBook) mAddressItr.next();

                            mOrganization = (MemberOrganization) mOrganizationItr.next();

                            organizationName = mOrganization.getOrganizationInfo().getOrganizationName();
                            organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo();
                            organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                        }
                    }

                    pictureName = member.getPictureName();

                    pictureLink = GlobalVariable.imageMemberDirLink + pictureName;

                    logger.info("API :: GetProfile API pictureLink ::" + pictureLink);

                    logger.info("API :: GetProfile API givenPassword ::" + encryption.getEncrypt(givenPassword));

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + memId + "' AND SYSDATE() between mt.ed and mt.td");

                        logger.info("API :: Login API mMemberTypeSQL ::" + mMemberTypeSQL);
                        for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                            mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                            mMemberTypeId = mMemberTypeObj[0].toString();
                            mMemberTypeName = mMemberTypeObj[1].toString();

                        }

                        Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + memId + "'");

                        logger.info("API :: GetProfile API memberAddressSQL ::" + memberAddressSQL);

                        if (!memberAddressSQL.list().isEmpty()) {
                            for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                                //   addressBook = (AddressBook) mAddressItr.next();

                                memberAddress = (MemberAddress) memberAddressItr.next();

                                memberAddressId = memberAddress.getId();
                                memberAddressType = memberAddress.getAddressType();
                                logger.info("API :: GetProfile API memberAddressType ::" + memberAddressType);

                                memberAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                memberAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                memberZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                                //  memberThanaId = memberAddress.getAddressBook().getThana().getId();
                                //   memberThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                //    memberDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                //    memberDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                                addressResponseObj = new JSONObject();
                                addressResponseObj.put("addressId", memberAddressId);
                                addressResponseObj.put("addressType", memberAddressType);

                                addressResponseObj.put("addressLine1", memberAddressLine1);
                                addressResponseObj.put("addressLine2", memberAddressLine2);
                                addressResponseObj.put("thanaId", memberThanaId);
                                addressResponseObj.put("thanaName", memberThanaName);
                                addressResponseObj.put("districtId", memberDistrictId);
                                addressResponseObj.put("districtName", memberDistrictName);
                                addressResponseObj.put("country", memberCountry);

                                addressObjArray.add(addressResponseObj);

                            }
                        }

                        addressObj = new JSONObject();
                        addressObj.put("ResponseCode", "1");
                        addressObj.put("ResponseText", "Found");
                        addressObj.put("ResponseData", addressObjArray);
                        logger.info("Consumer Address of :" + memberId + " Found");
                        /*
                        //   MemberEducationInfo memberEducation
                        Query memberEducationSQL = dbsession.createQuery(" from MemberEducationInfo WHERE member_id='" + memId + "'");

                        logger.info("API :: GetProfile API memberEducationSQL ::" + memberEducationSQL);

                        int degreeId = 0;
                        String degreeName = "";
                        String instituteName = "";
                        String boardUniversityName = "";
                        String yearOfPassing = "";
                        String resultTypeName = "";
                        String result = "";

                        if (!memberEducationSQL.list().isEmpty()) {
                            for (Iterator memberEducationItr = memberEducationSQL.list().iterator(); memberEducationItr.hasNext();) {

                                memberEducation = (MemberEducationInfo) memberEducationItr.next();
                                memberEducationId = memberEducation.getId();

                                degreeId = memberEducation.getDegree().getDegreeId();
                                degreeName = memberEducation.getDegree().getDegreeName();

                                instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();

                                boardUniversityName = memberEducation.getUniversity().getUniversityLongName();
                                yearOfPassing = memberEducation.getYearOfPassing();
                                resultTypeName = memberEducation.getResultType().getResultTypeName();
                                result = memberEducation.getResult();

                                educationResponseObj = new JSONObject();
                                educationResponseObj.put("educationId", memberEducationId);
                                educationResponseObj.put("degreeId", degreeId);
                                educationResponseObj.put("degreeName", degreeName);
                                educationResponseObj.put("instituteName", instituteName);
                                educationResponseObj.put("boardUniversityName", boardUniversityName);
                                educationResponseObj.put("yearOfPassing", yearOfPassing);
                                educationResponseObj.put("resultTypeName", resultTypeName);
                                educationResponseObj.put("result", result);

                                educationObjArray.add(educationResponseObj);

                            }
                        }
                         */
                        educationObj = new JSONObject();
                        educationObj.put("ResponseCode", "1");
                        educationObj.put("ResponseText", "Found");
                        educationObj.put("ResponseData", educationObjArray);
                        logger.info("Member Education Info of :" + memberId + " Found");

                        responseObj = new JSONObject();
                        responseObj.put("memId", memId);

                        responseObj.put("organizationName", organizationName);
                        responseObj.put("organizationLogo", organizationLogoLink);
                        responseObj.put("unitName", unitName);
                        responseObj.put("nirbachoniAson", nirbachoniAson);
                        responseObj.put("ratingPoint", ratingPoint);
                        responseObj.put("likeCount", likeCount);
                        responseObj.put("shareCount", shareCount);
                        responseObj.put("uploadCount", uploadCount);
                        responseObj.put("bloodDonationCount", bloodDonationCount);

                        responseObj.put("memberLifeStatus", memberLifeStatus);
                        responseObj.put("memberLifeStatusText", memberLifeStatusText);
                        responseObj.put("memberTypeId", mMemberTypeId);
                        responseObj.put("memberTypeName", mMemberTypeName);
                        responseObj.put("memberId", memberId);
                        responseObj.put("memberName", memberName);
                        responseObj.put("memberEmail", memberEmail);
                        responseObj.put("memberMobile", mobileNo);
                        responseObj.put("memberPhone1", phone1);
                        responseObj.put("memberPhone2", phone2);
                        responseObj.put("memberGender", gender);
                        responseObj.put("memberBloodGroup", bloodGroup);
                        responseObj.put("memberPicture", pictureLink);
                        responseObj.put("address", addressObj);
                        responseObj.put("education", educationObj);

                        responseObj.put("memberCenterId", memberCenterId);
                        responseObj.put("memberCenterName", memberCenterName);

                        responseObj.put("memberDivisionId", memberDivisionId);
                        responseObj.put("memberDivisionShortName", memberDivisionShortName);
                        responseObj.put("memberDivisionFullName", memberDivisionFullName);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", responseObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
