<%-- 
    Document    : memberDetailsInfoShort
    Created on  : APR 28, 2020, 9:42:05 PM
    Author      : TAHAJJAT
    
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String appLang = "";
    String memberId = "";
    String givenPassword = "";
    String mId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    ///if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("mId")) {
    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("mId")) {

        key = request.getParameter("key").trim();

        logger.info("BYDO :: API :: memberDetailsInfoShort appLang :: " + appLang);

        //  memberId = request.getParameter("memberId").trim();
        //  givenPassword = request.getParameter("password").trim();
        mId = request.getParameter("mId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*     
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    String memId = "";

    String memberName = "";
    String memberName_BN = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String fatherName_BN = "";
    String motherName = "";
    String motherName_BN = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String gender_BN = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";
    String countryCode = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";

    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("BYDO :: API :: memberDetailsInfoShort API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject professionObj = new JSONObject();
    JSONObject professionResponseObj = new JSONObject();
    JSONArray professionObjArray = new JSONArray();

    JSONObject trainingObj = new JSONObject();
    JSONObject trainingResponseObj = new JSONObject();
    JSONArray trainingObjArray = new JSONArray();

    JSONObject publicationObj = new JSONObject();
    JSONObject publicationResponseObj = new JSONObject();
    JSONArray publicationObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMemberCheckSQL = null;
    Object[] qMemberCheckObj = null;

    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    Query mMemberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    int memberXId = 0;
    String memberXIEBId = "";
    String memberXName = "";
    String memberXName_BN = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    String address = "";
    String district = "";
    String country = "";

    String addressSQL = "";
    String addressId = "";

    String thanaSQL = "";
    String thanaId = "";

    String districtSQL = "";
    String districtId  = "";

    String districtNameSQL  = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String bacgroundPictureName = "";
    String bacgroundPictureLink = "";

    Query mMemberUnitSQL = null;
    Object[] mMemberUnitObj = null;

    String unitId = "";
    String unitName = "";
    String unitName_BN = "";

    Query mMemberReligionSQL = null;
    Object[] mMemberReligionObj = null;

    String memberReligionId = "";
    String memberReligionName = "";
    String memberReligionName_BN = "";

    Query mMemberDesignationSQL = null;
    Object[] mMemberDesignationObj = null;

    String memberDesignationId = "";
    String memberDesignationName = "";
    String memberDesignationName_BN = "";

    Query mMemberDesignationJoiningSQL = null;
    Object[] mMemberDesignatioJoiningObj = null;

    String memberDesignationJoiningId = "";
    String memberDesignationJoiningName = "";
    String memberDesignationJoiningName_BN = "";

    Query mMemberJoiningRetireSQL = null;
    Object[] mMemberJoiningRetireObj = null;

    String memberJoiningRetireId = "";
    String memberJoiningDate = "";
    String memberRetireDate = "";

    Query mMemberBcsBatchSQL = null;
    Object[] mMemberBcsBatchObj = null;

    String bcsBatchId = "";
    String bcsBatchName = "";
    String bcsBatchName_BN = "";

    String homeDistrictId = "";
    String homeDistrictName = "";
    String homeDistrictName_BN = "";

    Query mMemberHomeDistrictSQL = null;
    Object[] mMemberHomeDistrictObj = null;

    Object[] mCustomTitleObj = null;
    String mCustomTitleText = "";
    String mCustomTitleUpdateDate = "";

    if (rec == 1) {

        try {

            mMemberSQL = dbsession.createQuery(" from Member WHERE id = '" + mId + "'");
            if (!mMemberSQL.list().isEmpty()) {
                for (Iterator memberItr = mMemberSQL.list().iterator(); memberItr.hasNext();) {
                    member = (Member) memberItr.next();
                    memberXId = member.getId();

                    memberXIEBId = member.getMemberId();
                    memberXName = member.getMemberName();

                    //instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();
                    fatherName = member.getFatherName() == null ? "" : member.getFatherName();

                    motherName = member.getMotherName() == null ? "" : member.getMotherName();

                    dob = member.getDob();
                    gender = member.getGender() == null ? "" : member.getGender().trim();

                    mobileNo = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                    bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();
                    countryCode = member.getCountryCode() == null ? "" : member.getCountryCode();

                    //      memberXName = member.getMemberName();
                    //     memberXName_BN = member.getMemberNameBn() == null ? "" : member.getMemberNameBn();
                    //   unitName = member.getUnitName() == null ? "" : member.getUnitName();
                    //  nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();
                    memberXPictureName = member.getPictureName() == null ? "no_image.jpg" : member.getPictureName();

                    memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                    // bacgroundPictureName = "no_image.jpg";
                    Query memberBgPictureSQL = dbsession.createSQLQuery("SELECT bg_file_name FROM member_profile_bg where member_id='" + mId + "'");
                    bacgroundPictureName = memberBgPictureSQL.uniqueResult() == null ? "" : memberBgPictureSQL.uniqueResult().toString();

                    bacgroundPictureLink = GlobalVariable.imageDirLink + "backgroundpicture/" + bacgroundPictureName;

                    mMemberOptionViewSQL = dbsession.createSQLQuery("SELECT phone1_view,phone2_view,mobile_view,email_view FROM member_view_setting WHERE member_id='" + mId + "'");

                    for (Iterator mMemberOptionViewItr = mMemberOptionViewSQL.list().iterator(); mMemberOptionViewItr.hasNext();) {

                        mMemberOptionViewObj = (Object[]) mMemberOptionViewItr.next();
                        memberXPhone1View = mMemberOptionViewObj[0].toString();
                        memberXPhone2View = mMemberOptionViewObj[1].toString();
                        memberXMobileView = mMemberOptionViewObj[2].toString();
                        memberXMobileView = mMemberOptionViewObj[3].toString();

                    }

                    mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + mId + "' AND SYSDATE() between mt.ed and mt.td");

                    for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                        mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                        mMemberTypeId = mMemberTypeObj[0].toString();
                        mMemberTypeName = mMemberTypeObj[1].toString();

                    }

                    //  String titleSQL = "SELECT custom_title FROM member_custom_title WHERE member_id=" + mId;
                    //   String title = dbsession.createSQLQuery(titleSQL).uniqueResult() == null ? "Not Available" : dbsession.createSQLQuery(titleSQL).uniqueResult().toString();
                    Query mCustomTitleSQL = dbsession.createSQLQuery("SELECT custom_title,update_time FROM member_custom_title WHERE member_id='" + mId + "'");

                    logger.info("API :: memberDetailsInfoShort API mCustomTitleSQL ::" + mCustomTitleSQL);

                    if (!mCustomTitleSQL.list().isEmpty()) {
                        for (Iterator mCustomTitleItr = mCustomTitleSQL.list().iterator(); mCustomTitleItr.hasNext();) {

                            mCustomTitleObj = (Object[]) mCustomTitleItr.next();
                            mCustomTitleText = mCustomTitleObj[0] == null ? "" : mCustomTitleObj[0].toString();
                            mCustomTitleUpdateDate = mCustomTitleObj[1] == null ? "" : mCustomTitleObj[1].toString();

                        }

                    }

                    String instituteSQL = "SELECT institute_name FROM member_education_info WHERE member_id=" + mId;
                    String institute = dbsession.createSQLQuery(instituteSQL).uniqueResult() == null ? "Not Available" : dbsession.createSQLQuery(instituteSQL).uniqueResult().toString();

                    /*
                    addressSQL = "SELECT address_id FROM member_address WHERE address_type ='M' AND member_id = '" + mId + "'";
                    addressId = dbsession.createSQLQuery(addressSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(addressSQL).uniqueResult().toString();

                    if (!addressId.equals("0")) {

                        thanaSQL = "SELECT THANA_ID FROM address_book WHERE ID = '" + addressId + "'";
                        thanaId = dbsession.createSQLQuery(thanaSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(thanaSQL).uniqueResult().toString();

                        districtSQL = "SELECT DISTRICT_ID FROM thana WHERE ID = '" + thanaId + "'";
                        districtId = dbsession.createSQLQuery(districtSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(districtSQL).uniqueResult().toString();

                        districtNameSQL = "SELECT DISTRICT_NAME FROM district WHERE ID = '" + districtId + "'";
                        district = dbsession.createSQLQuery(districtNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(districtNameSQL).uniqueResult().toString();
                    }
                    */
                    
                    /*
                    String mAddressLine1 = "";
                    String mAddressLine2 = "";
                    int mThanaId = 0;
                    String mThanaName = "";
                    int mDistrictId = 0;
                    String mDistrictName = "";
                    String mCountry = "Bangladesh";
                    String mZipCode = "";

                    String pAddressLine1 = "";
                    String pAddressLine2 = "";
                    int pThanaId = 0;
                    String pThanaName = "";
                    int pDistrictId = 0;
                    String pDistrictName = "";
                    String pCountry = "Bangladesh";
                    String pZipCode = "";
                   */
                    
                    
                    
                      //   Query mAddressSQL = dbsession.createSQLQuery(" from AddressBook WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='M' ) ");
                Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + mId + "'");

                logger.info("API ::  memberDetailsInfo API memberAddressSQL ::" + memberAddressSQL);

                if (!memberAddressSQL.list().isEmpty()) {
                    for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                        //   addressBook = (AddressBook) mAddressItr.next();

                        memberAddress = (MemberAddress) memberAddressItr.next();

                        memberAddressId = memberAddress.getId();
                        memberAddressType = memberAddress.getAddressType();
                        logger.info("API ::  memberDetailsInfo API memberAddressType ::" + memberAddressType);
                        //mailing address
                        if (memberAddressType.equals("M")) {
                            mAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                            mAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                            mZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            mThanaId = memberAddress.getAddressBook().getThana().getId();
                            mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                            mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                            mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                        }
                        //permanent address
                        if (memberAddressType.equals("P")) {
                            pAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                            pAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                            pZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            pThanaId = memberAddress.getAddressBook().getThana().getId();
                            pThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                            pDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                            pDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                        }

                    }
                }
                    
                    
                    
                    
                    
                    
                    String countryNameSQL = "SELECT COUNTRY_NAME FROM country WHERE COUNTRY_CODE = '" + countryCode + "'";
                    country = dbsession.createSQLQuery(countryNameSQL).uniqueResult() == null ? "Not Available" : dbsession.createSQLQuery(countryNameSQL).uniqueResult().toString();

                    responseObj = new JSONObject();
                    responseObj.put("Id", memberXId);

                    responseObj.put("organizationName", organizationName);
                    responseObj.put("organizationLogo", organizationLogoLink);

                    responseObj.put("unitId", unitId);
                    responseObj.put("unitName", unitName);

                    responseObj.put("BcsBatch", bcsBatchName);

                    responseObj.put("ReligionName", memberReligionName);

                    responseObj.put("HomeDistrictId", homeDistrictId);
                    responseObj.put("HomeDistrict", homeDistrictName);

                    responseObj.put("CustomTitle", mCustomTitleText);
                    responseObj.put("Institute", institute);
                    responseObj.put("District", district);
                    responseObj.put("Country", country);
                    
                    
                    
                    responseObj.put("mAddressLine1", mAddressLine1);
                responseObj.put("mAddressLine2", mAddressLine2);
                responseObj.put("mThanaId", mThanaId);
                responseObj.put("mThanaName", mThanaName);
                responseObj.put("mDistrictId", mDistrictId);
                responseObj.put("mDistrictName", mDistrictName);
                responseObj.put("mCountry", mCountry);
                responseObj.put("mZipCode", mZipCode);

                responseObj.put("pAddressLine1", pAddressLine1);
                responseObj.put("pAddressLine2", pAddressLine2);
                responseObj.put("pThanaId", pThanaId);
                responseObj.put("pThanaName", pThanaName);
                responseObj.put("pDistrictId", pDistrictId);
                responseObj.put("pDistrictName", pDistrictName);
                responseObj.put("pCountry", pCountry);
                responseObj.put("pZipCode", pZipCode);
                    
                    
                    

                    responseObj.put("DesignationId", memberDesignationId);
                    responseObj.put("Designation", memberDesignationName);
                    responseObj.put("Designation_BN", memberDesignationName_BN);
                    responseObj.put("JoiningDesignation", memberDesignationJoiningName);
                    responseObj.put("JoiningDesignation_BN", memberDesignationJoiningName_BN);
                    responseObj.put("JoiningDate", memberJoiningDate);
                    responseObj.put("RetireDate", memberRetireDate);

                    responseObj.put("nirbachoniAson", nirbachoniAson);
                    responseObj.put("ratingPoint", ratingPoint);
                    responseObj.put("likeCount", likeCount);
                    responseObj.put("shareCount", shareCount);
                    responseObj.put("uploadCount", uploadCount);
                    responseObj.put("bloodDonationCount", bloodDonationCount);

                    responseObj.put("MemberLifeStatus", memberLifeStatus);
                    responseObj.put("MemberLifeStatusText", memberLifeStatusText);
                    responseObj.put("MemberTypeId", mMemberTypeId);
                    responseObj.put("MemberTypeName", mMemberTypeName);
                    responseObj.put("MemberId", memberXIEBId);
                    responseObj.put("BPNO", memberXIEBId);
                    responseObj.put("Name", memberXName);

                    responseObj.put("FatherName", fatherName);

                    responseObj.put("MotherName", motherName);

                    responseObj.put("PlaceOfBirth", placeOfBirth);
                    responseObj.put("DateOfBirth", dob);
                    responseObj.put("Gender", gender);
                    responseObj.put("BloodGroup", bloodGroup);

                    responseObj.put("Mobile", mobileNo);
                    responseObj.put("Phone1", phone1);
                    responseObj.put("Phone2", phone2);
                    responseObj.put("Email", memberEmail);

                    responseObj.put("Phone1View", memberXPhone1View);
                    responseObj.put("Phone2View", memberXPhone2View);
                    responseObj.put("MobileView", memberXMobileView);
                    responseObj.put("EmailView", memberXEmailView);

                    responseObj.put("Picture", memberXPictureLink);

                    responseObj.put("BacgroundPicture", bacgroundPictureLink);

                    responseObj.put("CenterId", memberCenterId);
                    responseObj.put("CenterName", memberXCenter);

                    responseObj.put("DivisionId", memberDivisionId);
                    responseObj.put("DivisionShortName", memberXDivisionShort);
                    responseObj.put("DivisionFullName", memberXDivisionFull);
                    responseObj.put("Address", addressObjArray);
                    responseObj.put("Education", educationObjArray);
                    responseObj.put("Profession", professionObjArray);
                    responseObj.put("Training", trainingObjArray);
                    responseObj.put("Publication", publicationObjArray);

                    memberContentObjArr.add(responseObj);

                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "1");
                    logingObj.put("ResponseText", "Found");
                    logingObj.put("ResponseData", memberContentObjArr);

                }
            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Member Not Found");
                logingObj.put("ResponseData", memberContentObjArr);
            }

            /*
                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }
                

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
             */
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        //  finally {
        //      dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
