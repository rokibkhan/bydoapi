<%-- 
    Document    : memberDetailsInfoLong
    Created on  : APR 28, 2020, 9:42:05 PM
    Author      : TAHAJJAT
    
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("memberDetailsInfoLong_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("mId")) {
    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("mId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

        givenPassword = request.getParameter("password") == null ? "" : request.getParameter("password").trim();

        mId = request.getParameter("mId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    String memId = "";

    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberDetailsInfoLong API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject customTitleObj = new JSONObject();
    JSONObject customTitleResponseObj = new JSONObject();
    JSONArray customTitleObjArray = new JSONArray();

    JSONObject objectiveObj = new JSONObject();
    JSONObject objectiveResponseObj = new JSONObject();
    JSONArray objectiveObjArray = new JSONArray();

    JSONObject personalObj = new JSONObject();
    JSONObject personalResponseObj = new JSONObject();
    JSONArray personalObjArray = new JSONArray();

    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject trainingObj = new JSONObject();
    JSONObject trainingResponseObj = new JSONObject();
    JSONArray trainingObjArray = new JSONArray();

    JSONObject professionObj = new JSONObject();
    JSONObject professionResponseObj = new JSONObject();
    JSONArray professionObjArray = new JSONArray();

    JSONObject projectObj = new JSONObject();
    JSONObject projectResponseObj = new JSONObject();
    JSONArray projectObjArray = new JSONArray();

    JSONObject publicationObj = new JSONObject();
    JSONObject publicationResponseObj = new JSONObject();
    JSONArray publicationObjArray = new JSONArray();

    JSONObject conferenceObj = new JSONObject();
    JSONObject conferenceResponseObj = new JSONObject();
    JSONArray conferenceObjArray = new JSONArray();

    JSONObject certificateObj = new JSONObject();
    JSONObject certificateResponseObj = new JSONObject();
    JSONArray certificateObjArray = new JSONArray();

    JSONObject signatureObj = new JSONObject();
    JSONObject signatureResponseObj = new JSONObject();
    JSONArray signatureObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMemberCheckSQL = null;
    Object[] qMemberCheckObj = null;

    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    Query mMemberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    int memberXId = 0;
    String memberXIEBId = "";
    String memberXName = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    Object[] mCustomTitleObj = null;
    String mCustomTitleText = "";
    String mCustomTitleUpdateDate = "";

    Object[] mObjectiveObj = null;
    String mObjectiveText = "";
    String mObjectiveUpdateDate = "";

    Object[] mSignatureObj = null;
    String mSignatureFileName = "";
    String mSignatureUpdateDate = "";
    String mSignatureFileLink = "";

    if (rec == 1) {

        try {

            /*
            // qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE member_id = '" + memberId + "'");
            qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE mobile = '" + memberId1 + "'");

            if (!qMemberCheckSQL.list().isEmpty()) {
               
            
                for (Iterator qMemberCheckItr = qMemberCheckSQL.list().iterator(); qMemberCheckItr.hasNext();) {

                    qMemberCheckObj = (Object[]) qMemberCheckItr.next();
                    memId = qMemberCheckObj[0].toString();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");
                    dbPass = memberKeySQL.uniqueResult().toString();
                    
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {
             */
            mMemberSQL = dbsession.createQuery(" from Member WHERE id = '" + mId + "'");

            for (Iterator memberItr = mMemberSQL.list().iterator(); memberItr.hasNext();) {
                member = (Member) memberItr.next();
                memberXId = member.getId();

                memberXIEBId = member.getMemberId();
                memberXName = member.getMemberName();

                //instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();
                fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                motherName = member.getMotherName() == null ? "" : member.getMotherName();

                dob = member.getDob();
                gender = member.getGender() == null ? "" : member.getGender().trim();

                mobileNo = member.getMobile() == null ? "" : member.getMobile();
                phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                personalResponseObj = new JSONObject();

                personalResponseObj.put("MemberID", memberXIEBId);
                personalResponseObj.put("Name", memberXName);
                personalResponseObj.put("FatherName", fatherName);
                personalResponseObj.put("MotherName", motherName);
                personalResponseObj.put("Gender", gender);
                personalResponseObj.put("DOB", dob);
                personalResponseObj.put("MobileNo", mobileNo);
                personalResponseObj.put("Phone1", phone1);
                personalResponseObj.put("Phone2", phone2);
                personalResponseObj.put("MemberEmail", memberEmail);
                personalResponseObj.put("BloodGroup", bloodGroup);

                personalObjArray.add(personalResponseObj);

                Query mCustomTitleSQL = dbsession.createSQLQuery("SELECT custom_title,update_time FROM member_custom_title WHERE member_id='" + mId + "'");

                logger.info("API :: memberDetailsInfoLong API mCustomTitleSQL ::" + mCustomTitleSQL);

                if (!mCustomTitleSQL.list().isEmpty()) {
                    for (Iterator mCustomTitleItr = mCustomTitleSQL.list().iterator(); mCustomTitleItr.hasNext();) {

                        mCustomTitleObj = (Object[]) mCustomTitleItr.next();
                        mCustomTitleText = mCustomTitleObj[0] == null ? "" : mCustomTitleObj[0].toString();
                        mCustomTitleUpdateDate = mCustomTitleObj[1] == null ? "" : mCustomTitleObj[1].toString();

                        customTitleResponseObj = new JSONObject();
                        customTitleResponseObj.put("CustomTitleText", mCustomTitleText);
                        customTitleResponseObj.put("CustomTitleUpdateDate", mCustomTitleUpdateDate);

                        customTitleObjArray.add(customTitleResponseObj);

                    }

                }

                Query mObjectiveSQL = dbsession.createSQLQuery("SELECT objectives,update_time FROM member_career_objective WHERE member_id='" + mId + "'");

                logger.info("API :: memberDetailsInfoLong API mObjectiveSQL ::" + mObjectiveSQL);

                if (!mObjectiveSQL.list().isEmpty()) {
                    for (Iterator mObjectiveItr = mObjectiveSQL.list().iterator(); mObjectiveItr.hasNext();) {

                        mObjectiveObj = (Object[]) mObjectiveItr.next();
                        mObjectiveText = mObjectiveObj[0] == null ? "" : mObjectiveObj[0].toString();
                        mObjectiveUpdateDate = mObjectiveObj[1] == null ? "" : mObjectiveObj[1].toString();

                        objectiveResponseObj = new JSONObject();
                        objectiveResponseObj.put("ObjectiveText", mObjectiveText);
                        objectiveResponseObj.put("ObjectiveUpdateDate", mObjectiveUpdateDate);

                        objectiveObjArray.add(objectiveResponseObj);

                    }

                }

                Query mSignatureSQL = dbsession.createSQLQuery("SELECT signature_file_name,update_time FROM member_signature WHERE member_id='" + mId + "'");

                logger.info("API :: memberDetailsInfoLong API mSignatureSQL ::" + mSignatureSQL);

                if (!mSignatureSQL.list().isEmpty()) {
                    for (Iterator mSignatureItr = mSignatureSQL.list().iterator(); mSignatureItr.hasNext();) {

                        mSignatureObj = (Object[]) mSignatureItr.next();
                        mSignatureFileName = mSignatureObj[0] == null ? "" : mSignatureObj[0].toString();
                        mSignatureUpdateDate = mSignatureObj[1] == null ? "" : mSignatureObj[1].toString();

                        mSignatureFileLink = GlobalVariable.imageDirLink + "signature/" + mSignatureFileName;

                        signatureResponseObj = new JSONObject();
                        signatureResponseObj.put("SignatureFileName", mSignatureFileName);
                        signatureResponseObj.put("SignatureFileLink", mSignatureFileLink);
                        signatureResponseObj.put("SignatureUpdateDate", mSignatureUpdateDate);

                        signatureObjArray.add(signatureResponseObj);

                    }

                }

                //   Query mAddressSQL = dbsession.createSQLQuery(" from AddressBook WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='M' ) ");
                Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + mId + "'");

                logger.info("API ::  memberDetailsInfo API memberAddressSQL ::" + memberAddressSQL);

                if (!memberAddressSQL.list().isEmpty()) {
                    for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                        //   addressBook = (AddressBook) mAddressItr.next();

                        memberAddress = (MemberAddress) memberAddressItr.next();

                        memberAddressId = memberAddress.getId();
                        memberAddressType = memberAddress.getAddressType();
                        logger.info("API ::  memberDetailsInfo API memberAddressType ::" + memberAddressType);
                        //mailing address
                        if (memberAddressType.equals("M")) {
                            mAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                            mAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                            mZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            mThanaId = memberAddress.getAddressBook().getThana().getId();
                            mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                            mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                            mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                        }
                        //permanent address
                        if (memberAddressType.equals("P")) {
                            pAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                            pAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                            pZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                            pThanaId = memberAddress.getAddressBook().getThana().getId();
                            pThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                            pDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                            pDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                        }

                    }
                }

//                        Query pAddressSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='P' ) ");
//                        for (Iterator itr3 = pAddressSQL.list().iterator(); itr3.hasNext();) {
//
//                            pAddressObj = (Object[]) itr3.next();
//                            pAddressLine1 = pAddressObj[1].toString();
//                            pAddressLine2 = pAddressObj[2].toString();
//                            pThanaId = Integer.parseInt(pAddressObj[3].toString());
//
//                        }
                addressResponseObj = new JSONObject();

                addressResponseObj.put("mAddressLine1", mAddressLine1);
                addressResponseObj.put("mAddressLine2", mAddressLine2);
                addressResponseObj.put("mThanaId", mThanaId);
                addressResponseObj.put("mThanaName", mThanaName);
                addressResponseObj.put("mDistrictId", mDistrictId);
                addressResponseObj.put("mDistrictName", mDistrictName);
                addressResponseObj.put("mCountry", mCountry);
                addressResponseObj.put("mZipCode", mZipCode);

                addressResponseObj.put("pAddressLine1", pAddressLine1);
                addressResponseObj.put("pAddressLine2", pAddressLine2);
                addressResponseObj.put("pThanaId", pThanaId);
                addressResponseObj.put("pThanaName", pThanaName);
                addressResponseObj.put("pDistrictId", pDistrictId);
                addressResponseObj.put("pDistrictName", pDistrictName);
                addressResponseObj.put("pCountry", pCountry);
                addressResponseObj.put("pZipCode", pZipCode);

                addressObjArray.add(addressResponseObj);

                //   MemberEducationInfo memberEducation
                MemberEducationInfo memberEducation = null;
                int memberEducationId = 0;
                int degreeId = 0;
                String degreeName = "";
                String instituteName = "";
                int boardUniversityId = 0;
                String boardUniversityName = "";
                String yearOfPassing = "";
                int resultTypeId = 0;
                String resultTypeName = "";
                String result = "";

                Object[] subjectObject = null;
                String subjectUniversityName = "";
                String subjectName = "";

                Query memberEducationSQL = dbsession.createQuery(" from MemberEducationInfo WHERE member_id='" + mId + "'");

                logger.info("API ::  memberDetailsInfo API memberEducationSQL ::" + memberEducationSQL);

                if (!memberEducationSQL.list().isEmpty()) {
                    for (Iterator memberEducationItr = memberEducationSQL.list().iterator(); memberEducationItr.hasNext();) {

                        memberEducation = (MemberEducationInfo) memberEducationItr.next();
                        memberEducationId = memberEducation.getId();

                        degreeId = memberEducation.getDegree().getDegreeId();
                        degreeName = memberEducation.getDegree().getDegreeName();

                        instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();

                        logger.info("API ::  memberDetailsInfo API instituteName ::" + instituteName);

                        boardUniversityId = memberEducation.getUniversity().getUniversityId();
                        boardUniversityName = memberEducation.getUniversity().getUniversityLongName();
                        yearOfPassing = memberEducation.getYearOfPassing();
                        resultTypeId = memberEducation.getResultType().getResultTypeId();
                        resultTypeName = memberEducation.getResultType().getResultTypeName();
                        result = memberEducation.getResult();

                        educationResponseObj = new JSONObject();
                        educationResponseObj.put("EducationId", memberEducationId);
                        educationResponseObj.put("DegreeId", degreeId);
                        educationResponseObj.put("DegreeName", degreeName);
                        educationResponseObj.put("InstituteName", instituteName);

                        educationResponseObj.put("BoardUniversityId", boardUniversityId);
                        educationResponseObj.put("BoardUniversityName", boardUniversityName);

                        educationResponseObj.put("YearOfPassing", yearOfPassing);

                        educationResponseObj.put("ResultTypeId", resultTypeId);
                        educationResponseObj.put("ResultTypeName", resultTypeName);

                        educationResponseObj.put("Result", result);
                        educationResponseObj.put("SubjectName", subjectName);

                        educationObjArray.add(educationResponseObj);

                    }
                }

                //Training
                MemberTrainingInfo memberTraining = null;
                int trainingId = 0;
                String trainingInstituteName = "";
                String courseTitle = "";
                String trainingTitle = "";
                String trainingYearOfPassing = "";
                String weeks = "";

                Query memberTrainingSQL = dbsession.createQuery("from MemberTrainingInfo where  member_id=" + mId + " ");
                logger.info("API ::  memberDetailsInfo API memberTrainingSQL ::" + memberTrainingSQL);
                for (Iterator memberTrainingItr = memberTrainingSQL.list().iterator(); memberTrainingItr.hasNext();) {
                    memberTraining = (MemberTrainingInfo) memberTrainingItr.next();

                    trainingId = memberTraining.getId();
                    trainingInstituteName = memberTraining.getMemberTrainingInstitute() == null ? "" : memberTraining.getMemberTrainingInstitute();
                    trainingTitle = memberTraining.getMemberTrainingTitle() == null ? "" : memberTraining.getMemberTrainingTitle();
                    courseTitle = memberTraining.getMemberTrainingCourse() == null ? "" : memberTraining.getMemberTrainingCourse();
                    trainingYearOfPassing = memberTraining.getMemberTrainingYear() == null ? "" : memberTraining.getMemberTrainingYear();
                    weeks = memberTraining.getMemberTrainingDuration() == null ? "" : memberTraining.getMemberTrainingDuration();

                    trainingResponseObj = new JSONObject();
                    trainingResponseObj.put("TrainingId", trainingId);
                    trainingResponseObj.put("TrainingInstituteName", trainingInstituteName);
                    trainingResponseObj.put("TrainingTitle", trainingTitle);
                    trainingResponseObj.put("CourseTitle", courseTitle);
                    trainingResponseObj.put("TrainingYearOfPassing", trainingYearOfPassing);
                    trainingResponseObj.put("weeks", weeks);

                    trainingObjArray.add(trainingResponseObj);
                }

                //Profession Object
                MemberProfessionalInfo memberProfession = null;
                int memberProfessionId = 0;
                String organizationName = "";
                String organizationType = "";
                String organizationAddress = "";
                String organizationDesignationName = "";
                String organizationStartDate = "";
                String organizationTillDate = "";
                Query memberProfessionSQL = dbsession.createQuery("from MemberProfessionalInfo WHERE  member_id='" + mId + "' ");

                logger.info("API ::  memberDetailsInfo API memberProfessionSQL ::" + memberProfessionSQL);

                for (Iterator memberProfessionItr = memberProfessionSQL.list().iterator(); memberProfessionItr.hasNext();) {
                    memberProfession = (MemberProfessionalInfo) memberProfessionItr.next();
                    memberProfessionId = memberProfession.getId();
                    organizationName = memberProfession.getCompanyName() == null ? "" : memberProfession.getCompanyName();

                    organizationType = memberProfession.getCompanyType() == null ? "" : memberProfession.getCompanyType();
                    organizationDesignationName = memberProfession.getDesignation() == null ? "" : memberProfession.getDesignation();
                    organizationStartDate = memberProfession.getFromDate().toString() == null ? "" : memberProfession.getFromDate().toString();
                    organizationTillDate = memberProfession.getTillDate().toString() == null ? "" : memberProfession.getTillDate().toString();
                    organizationAddress = memberProfession.getCompanyAddress() == null ? "" : memberProfession.getCompanyAddress();

                    professionResponseObj = new JSONObject();
                    professionResponseObj.put("ProfessionId", memberProfessionId);
                    professionResponseObj.put("OrganizationName", organizationName);
                    professionResponseObj.put("OrganizationType", organizationType);
                    professionResponseObj.put("DesignationName", organizationDesignationName);
                    professionResponseObj.put("StartDate", organizationStartDate);
                    professionResponseObj.put("EndDate", organizationTillDate);
                    professionResponseObj.put("OrganizationAddress", organizationAddress);

                    professionObjArray.add(professionResponseObj);

                }

                MemberPublicationInfo memberPublication = null;
                int publicationId = 0;
                String publicationTitle = "";
                String publicationAuthor = "";
                String publicationYear = "";
                String publicationJournal = "";

                Query memberPublicationSQL = dbsession.createQuery("from MemberPublicationInfo where  member_id='" + mId + "' ");
                logger.info("API ::  memberDetailsInfo API memberPublicationSQL ::" + memberPublicationSQL);
                for (Iterator memberPublicationItr = memberPublicationSQL.list().iterator(); memberPublicationItr.hasNext();) {
                    memberPublication = (MemberPublicationInfo) memberPublicationItr.next();

                    publicationId = memberPublication.getId();
                    publicationTitle = memberPublication.getPublicationTitle() == null ? "" : memberPublication.getPublicationTitle();
                    publicationYear = memberPublication.getPublicationYear() == null ? "" : memberPublication.getPublicationYear();
                    publicationJournal = memberPublication.getPublicationJournalConference() == null ? "" : memberPublication.getPublicationJournalConference();
                    publicationAuthor = memberPublication.getPublicationAuthor() == null ? "" : memberPublication.getPublicationAuthor().toString();

                    publicationResponseObj = new JSONObject();
                    publicationResponseObj.put("PublicationId", publicationId);
                    publicationResponseObj.put("PublicationTitle", publicationTitle);
                    publicationResponseObj.put("PublicationYear", publicationYear);
                    publicationResponseObj.put("PublicationJournal", publicationJournal);
                    publicationResponseObj.put("PublicationAuthor", publicationAuthor);

                    publicationObjArray.add(publicationResponseObj);
                }

                MemberProjectInfo memberProject = null;
                int projectId = 0;
                String projectTitle = "";
                String projectCategoty = "";
                String projectRole = "";
                String projectDetails = "";
                String projectStartDate = "";
                String projectEndDate = "";

                Query memberProjectSQL = dbsession.createQuery("from MemberProjectInfo where  member_id ='" + mId + "' ");
                logger.info("API ::  memberDetailsInfo API memberProjectSQL ::" + memberProjectSQL);
                for (Iterator memberProjectItr = memberProjectSQL.list().iterator(); memberProjectItr.hasNext();) {
                    memberProject = (MemberProjectInfo) memberProjectItr.next();

                    projectId = memberProject.getId();
                    projectTitle = memberProject.getMemberProjectTitle() == null ? "" : memberProject.getMemberProjectTitle();
                    projectCategoty = memberProject.getMemberProjectCategory() == null ? "" : memberProject.getMemberProjectCategory();
                    projectRole = memberProject.getMemberProjectMemberRole() == null ? "" : memberProject.getMemberProjectMemberRole();
                    projectDetails = memberProject.getMemberProjectDetails() == null ? "" : memberProject.getMemberProjectDetails().toString();
                    projectStartDate = memberProject.getMemberProjectStartDate() == null ? "" : memberProject.getMemberProjectStartDate().toString();
                    projectEndDate = memberProject.getMemberProjectEndDate() == null ? "" : memberProject.getMemberProjectEndDate().toString();

                    projectResponseObj = new JSONObject();
                    projectResponseObj.put("ProjectId", projectId);
                    projectResponseObj.put("ProjectTitle", projectTitle);
                    projectResponseObj.put("ProjectCategoty", projectCategoty);
                    projectResponseObj.put("ProjectRole", projectRole);
                    projectResponseObj.put("ProjectDetails", projectDetails);
                    projectResponseObj.put("ProjectStartDate", projectStartDate);
                    projectResponseObj.put("ProjectEndDate", projectEndDate);

                    projectObjArray.add(projectResponseObj);
                }

                MemberCertificate memberCertificate = null;
                int certificateId = 0;
                String certificateShortName = "";
                String certificateLongName = "";
                String certificateFileName = "";
                String certificateFileLink = "";

                Query memberCertificateSQL = dbsession.createQuery("from MemberCertificate where  member_id ='" + mId + "' ");
                logger.info("API ::  memberDetailsInfo API memberCertificateSQL ::" + memberCertificateSQL);
                for (Iterator memberCertificateItr = memberCertificateSQL.list().iterator(); memberCertificateItr.hasNext();) {
                    memberCertificate = (MemberCertificate) memberCertificateItr.next();

                    certificateId = memberCertificate.getId();
                    certificateShortName = memberCertificate.getCertificateShortName() == null ? "" : memberCertificate.getCertificateShortName().toString();
                    certificateLongName = memberCertificate.getCertificateLongName() == null ? "" : memberCertificate.getCertificateLongName().toString();
                    certificateFileName = memberCertificate.getCertificateFileName() == null ? "" : memberCertificate.getCertificateFileName().toString();

                    certificateFileLink = GlobalVariable.imageDirLink + "certificate/" + certificateFileName;

                    certificateResponseObj = new JSONObject();
                    certificateResponseObj.put("CertificateId", certificateId);
                    certificateResponseObj.put("CertificateShortName", certificateShortName);
                    certificateResponseObj.put("CertificateLongName", certificateLongName);
                    certificateResponseObj.put("CertificateFileName", certificateFileName);
                    certificateResponseObj.put("CertificateFileLink", certificateFileLink);

                    certificateObjArray.add(certificateResponseObj);
                }

                MemberConferenceInfo memberConference = null;
                int conferenceId = 0;
                String conferenceTitle = "";
                String conferenceRole = "";
                String conferenceDetails = "";
                String conferenceStartDate = "";
                String conferenceEndDate = "";
                String conferenceUrl = "";

                Query memberConferenceSQL = dbsession.createQuery("from MemberConferenceInfo where  member_id ='" + mId + "' ");
                logger.info("API ::  memberDetailsInfo API memberConferenceSQL ::" + memberConferenceSQL);
                for (Iterator memberConferenceItr = memberConferenceSQL.list().iterator(); memberConferenceItr.hasNext();) {
                    memberConference = (MemberConferenceInfo) memberConferenceItr.next();

                    conferenceId = memberConference.getId();
                    conferenceTitle = memberConference.getConferenceTitle() == null ? "" : memberConference.getConferenceTitle().toString();
                    conferenceRole = memberConference.getConferenceMemberRole() == null ? "" : memberConference.getConferenceMemberRole().toString();
                    conferenceDetails = memberConference.getConferenceDetails() == null ? "" : memberConference.getConferenceDetails().toString();
                    conferenceStartDate = memberConference.getConferenceStartDate() == null ? "" : memberConference.getConferenceStartDate().toString();
                    conferenceEndDate = memberConference.getConferenceEndDate() == null ? "" : memberConference.getConferenceEndDate().toString();
                    conferenceUrl = memberConference.getConferenceUrl() == null ? "" : memberConference.getConferenceUrl();

                    conferenceResponseObj = new JSONObject();
                    conferenceResponseObj.put("ConferenceId", conferenceId);
                    conferenceResponseObj.put("ConferenceTitle", conferenceTitle);
                    conferenceResponseObj.put("ConferenceRole", conferenceRole);
                    conferenceResponseObj.put("ConferenceDetails", conferenceDetails);
                    conferenceResponseObj.put("ConferenceStartDate", conferenceStartDate);
                    conferenceResponseObj.put("ConferenceEndDate", conferenceEndDate);
                    conferenceResponseObj.put("ConferenceUrl", conferenceUrl);

                    conferenceObjArray.add(conferenceResponseObj);
                }

                responseObj = new JSONObject();
                responseObj.put("Id", memberXId);

                responseObj.put("customTitle", customTitleObjArray);
                responseObj.put("Objective", objectiveObjArray);
                responseObj.put("Personal", personalObjArray);
                responseObj.put("Address", addressObjArray);
                responseObj.put("Education", educationObjArray);
                responseObj.put("Training", trainingObjArray);
                responseObj.put("Profession", professionObjArray);
                responseObj.put("Project", projectObjArray);
                responseObj.put("Publication", publicationObjArray);
                responseObj.put("Conference", conferenceObjArray);
                responseObj.put("Certificate", certificateObjArray);
                responseObj.put("Signature", signatureObjArray);

                memberContentObjArr.add(responseObj);

            }

            logingObj = new JSONObject();
            logingObj.put("ResponseCode", "1");
            logingObj.put("ResponseText", "Found");
            logingObj.put("ResponseData", memberContentObjArr);
            /*

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }
                

                }


            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
             */
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
