<%-- 
    Document   : getSessionInfo
    Created on : Nov 7, 2020, 3:27:06 AM
    Author     : Zeeon
--%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getSessionInfo_jsp.class");

    PrintWriter writer = response.getWriter();

    String key = "";
    String sessionId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("sessionId")) {
        key = request.getParameter("key").trim();
        sessionId = request.getParameter("sessionId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    JSONObject responseDataObj = new JSONObject();

    JSONArray chapterDataObjArr = new JSONArray();
    JSONObject chapterDataObj = new JSONObject();

    JSONArray videoDataObjArr = new JSONArray();
    JSONObject videoDataObj = new JSONObject();

    Object[] obj3 = null;
    Object[] obj4 = null;

    String ChapterName = "";
    String ChapterShortName = "";
    String IdChapter = "";
    String ChapterFeatureImage = "";
    String ChapterDesc = "";

    String VideoTitle = "";
    String VideoEmbedLink = "";
    String IdVideo = "";
    String VideoCaption = "";
    String VideoFeatureImage = "";
    String VideoTeacherID = "";
    String VideoCategory = "";
    String VideoPaymentTypeID = "";
    String VideoLinkTypeID = "";

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("BYDO :: API :: getSubjectInfo rec :: " + rec);

    if (rec == 1) {
        try {

            //Chapter Info Start
            chapterDataObjArr = new JSONArray();

            Query chapSQL = dbsession.createSQLQuery("SELECT id_chapter, chapter_name, chapter_short_name, chapter_desc, feature_image"
                    + " FROM chapter_info where session_id = '" + sessionId + "'");
            
            if (!chapSQL.list().isEmpty()) {
                for (Iterator it3 = chapSQL.list().iterator(); it3.hasNext();) {
                    chapterDataObj = new JSONObject();
                    obj3 = (Object[]) it3.next();

                    IdChapter = obj3[0].toString().trim();
                    ChapterName = obj3[1].toString().trim();
                    ChapterShortName = obj3[2].toString().trim();
                    ChapterDesc = obj3[3].toString().trim();
                    ChapterFeatureImage = obj3[4] == null ? "" : obj3[4].toString().trim();

                    //Video Start
                    videoDataObjArr = new JSONArray();

                    Query videoSQL = dbsession.createSQLQuery("SELECT id_video, video_title,video_emded_link, video_caption"
                            + ", feature_image,teacher_id, video_category, payment_type_id,link_type_id"
                            + " FROM video_gallery_info where chapter_id = '" + IdChapter + "'");

                    for (Iterator it4 = videoSQL.list().iterator(); it4.hasNext();) {
                        obj4 = (Object[]) it4.next();

                        IdVideo = obj4[0].toString().trim();
                        VideoTitle = obj4[1].toString().trim();
                        VideoEmbedLink = obj4[2].toString().trim();
                        VideoCaption = obj4[3].toString().trim();
                        VideoFeatureImage = obj4[4] == null ? "" : obj4[4].toString().trim();
                        VideoCategory = obj4[5] == null ? "" : obj4[5].toString().trim();
                        VideoPaymentTypeID = obj4[6] == null ? "" : obj4[6].toString().trim();
                        VideoLinkTypeID = obj4[7] == null ? "" : obj4[7].toString().trim();

                        videoDataObj.put("VideoId", IdVideo);
                        videoDataObj.put("VideoTitle", VideoTitle);
                        videoDataObj.put("VideoEmbedLink", VideoEmbedLink);
                        videoDataObj.put("VideoFeatureImage", VideoFeatureImage);
                        videoDataObj.put("VideoCategory", VideoCategory);
                        videoDataObj.put("VideoPaymentTypeID", VideoPaymentTypeID);
                        videoDataObj.put("VideoLinkTypeID", VideoLinkTypeID);
                        videoDataObjArr.add(videoDataObj);
                        
                        //Video End
                    }

                    chapterDataObj.put("VideoList", videoDataObjArr);

                    chapterDataObj.put("ChapterId", IdChapter);
                    chapterDataObj.put("ChapterName", ChapterName);
                    chapterDataObj.put("ChapterDesc", ChapterDesc);
                    chapterDataObj.put("ChapterImage", GlobalVariable.imageDirLink + ChapterFeatureImage);
                    chapterDataObjArr.add(chapterDataObj);
                    
                    //Chapter End
                }
            } else {
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", chapterDataObjArr);
            }

            responseDataObj.put("ResponseCode", "1");
            responseDataObj.put("ResponseText", "Found");
            responseDataObj.put("ResponseData", chapterDataObjArr);

        } catch (Exception e) {
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }
    } else {
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", chapterDataObjArr);
    }

    writer.write(responseDataObj.toJSONString());
    response.getWriter().flush();
    response.getWriter().close();

    dbsession.flush();
    dbsession.close();
%>
