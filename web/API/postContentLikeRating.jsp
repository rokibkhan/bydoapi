<%-- 
    Document   : postContentLikeRating
    Created on : APR 28, 2020, 10:26:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    Logger logger = Logger.getLogger("postContentLikeRating_jsp.class");

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String postContentId = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("postContentId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        postContentId = request.getParameter("postContentId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: postContentLikeRating API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String contentId = "";
    String contentCategory = "";
    String contentType = "";
    String contentTitle = "";
    String contentShortDesc = "";
    String contentDesc = "";
    String contentPostLink = "";
    String contentPostPicture = "";
    String contentPostPictureLink = "";

    String ratingDate = "";
    Query postContentShareRatingAddSQL = null;

    Query postContentRatingHistoryAddSQL = null;

    //  String ratingPointHostorySQL = "";
    //   String ratingPointHostoryNumRows = "";
    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    logger.info("memId ::" + memId);

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //check user rating history point already exits or not
                        String ratingPointHostorySQL = "SELECT  count(*) FROM member_like_history  "
                                + "WHERE member_id = '" + memId + "' AND content_id = '" + postContentId + "'";

                        String ratingPointHostoryNumRows = dbsession.createSQLQuery(ratingPointHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(ratingPointHostorySQL).uniqueResult().toString();
                        //   int numrows = Integer.parseInt(numrow1);

                        logger.info("ratingPointHostoryNumRows ::" + ratingPointHostoryNumRows);

                        //   String oldRatingPointUserSQL = "SELECT  rating_point FROM user_rating_point WHERE member_id = '" + memId + "'";
                        String oldRatingPointUserSQL = "SELECT  rating_point FROM member_rating_point WHERE member_id = '" + memId + "'";
                        String oldRatingPointUser = dbsession.createSQLQuery(oldRatingPointUserSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(oldRatingPointUserSQL).uniqueResult().toString();
                        int newRatingPointUser = (1 + Integer.parseInt(oldRatingPointUser));

                        logger.info("newRatingPointUser ::" + newRatingPointUser);
                        logger.info("oldRatingPointUser ::" + oldRatingPointUser);

                        String oldLikeRatingPointUserSQL = "SELECT  like_content_point FROM member_rating_point WHERE member_id = '" + memId + "'";
                        String oldLikeRatingPointUser = dbsession.createSQLQuery(oldLikeRatingPointUserSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(oldLikeRatingPointUserSQL).uniqueResult().toString();
                        int newLikeRatingPointUser = (1 + Integer.parseInt(oldLikeRatingPointUser));

                        logger.info("oldLikeRatingPointUser ::" + oldLikeRatingPointUser);
                        logger.info("newLikeRatingPointUser ::" + newLikeRatingPointUser);

                        //  String oldRatingPointContentSQL = "SELECT  share FROM post_content WHERE id_post_content = '" + postContentId + "'";
                        String oldRatingPointContentSQL = "SELECT  like_content_point FROM post_content_rating_point WHERE content_id = '" + postContentId + "'";
                        String oldRatingPointContent = dbsession.createSQLQuery(oldRatingPointContentSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(oldRatingPointContentSQL).uniqueResult().toString();
                        int newRatingPointContent = (1 + Integer.parseInt(oldRatingPointContent));

                        logger.info("newRatingPointContent ::" + newRatingPointContent);
                        logger.info("oldRatingPointContent ::" + oldRatingPointContent);

                        if (ratingPointHostoryNumRows.equals("0")) {

                            String idS = getId.getID(69);
                            int postContentRatingHistoryId = Integer.parseInt(idS);

                            InetAddress ip;
                            String hostname = "";
                            String ipAddress = "";
                            try {
                                ip = InetAddress.getLocalHost();
                                hostname = ip.getHostName();
                                ipAddress = ip.getHostAddress();

                            } catch (UnknownHostException e) {

                                e.printStackTrace();
                            }

                            String adduser = Integer.toString(memId);
                            adddate = dateFormatX.format(dateToday);
                            String customOrderby = "0";
                            String published = "0";

                            ratingDate = dateFormatX.format(dateToday);

                            //insert user_rating_point_history
//                            postContentRatingHistoryAddSQL = dbsession.createSQLQuery("INSERT INTO user_rating_point_history("
//                                    + "id_rating_history,"
//                                    + "client_id,"
//                                    + "content_id,"
//                                    + "rating_date,"
//                                    + "add_date,"
//                                    + "add_ip,"
//                                    + "add_term,"
//                                    + "add_user"
//                                    + ") VALUES("
//                                    + "'" + postContentRatingHistoryId + "',"
//                                    + "'" + memId + "',"
//                                    + "'" + postContentId + "',"
//                                    + "'" + ratingDate + "',"
//                                    + "'" + adddate + "',"
//                                    + "'" + ipAddress + "',"
//                                    + "'" + hostname + "',"
//                                    + "'" + adduser + "'"
//                                    + "  ) ");
                            postContentRatingHistoryAddSQL = dbsession.createSQLQuery("INSERT INTO member_like_history("
                                    + "id,"
                                    + "member_id,"
                                    + "content_id,"
                                    + "rating_date,"
                                    + "add_date,"
                                    + "add_ip,"
                                    + "add_term,"
                                    + "add_user"
                                    + ") VALUES("
                                    + "'" + postContentRatingHistoryId + "',"
                                    + "'" + memId + "',"
                                    + "'" + postContentId + "',"
                                    + "'" + ratingDate + "',"
                                    + "'" + adddate + "',"
                                    + "'" + ipAddress + "',"
                                    + "'" + hostname + "',"
                                    + "'" + adduser + "'"
                                    + "  ) ");

                            logger.info("postContentRatingHistoryAddSQL ::" + postContentRatingHistoryAddSQL);

                            postContentRatingHistoryAddSQL.executeUpdate();

                            //update user_info Rating for ratingPoint filed
                            // $udata = array('user_rating_point' => $newRating);
                            //  $updateInfo = $dataList->updateDataInfo('user_info', 'user_id', $id_client, $udata);
                            //update request status
                            Query updateUserRatingPointSQL = dbsession.createSQLQuery("UPDATE  member_rating_point SET rating_point='" + newRatingPointUser + "',like_content_point = '" + newLikeRatingPointUser + "',update_time='" + adddate + "' WHERE member_id = '" + memId + "'");

                            //   Query updateUserRatingPointSQL = dbsession.createSQLQuery("UPDATE  user_rating_point SET rating_point='" + newRatingPointUser + "',share_content_point = '" + newRatingPointUser + "',update_time='" + adddate + "' WHERE member_id = '" + memId + "'");
                            updateUserRatingPointSQL.executeUpdate();

                            //update user share count for shareCount filed
                            // $udataXuscp = array('user_share_content_point' => $newRating);
                            // $updateInfoXuscp = $dataList->updateDataInfo('user_info', 'user_id', $id_client, $udataXuscp);
                            //update content share count
                            //  $udataConX = array('share' => $newRatingConX);
                            //   $updateInfoConX = $dataList->updateDataInfo('post_content', 'id_post_content', $postContentId, $udataConX);
                            //   Query updateContentRatingPointSQL = dbsession.createSQLQuery("UPDATE  post_content SET share='" + newRatingPointContent + "',mod_date='" + adddate + "' WHERE id_post_content = '" + postContentId + "'");
                            Query updateContentRatingPointSQL = dbsession.createSQLQuery("UPDATE  post_content_rating_point SET like_content_point='" + newRatingPointContent + "',update_time='" + adddate + "' WHERE content_id = '" + postContentId + "'");
                            updateContentRatingPointSQL.executeUpdate();

                            dbtrx.commit();

                            postContentObj = new JSONObject();

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Post content like rating added successfully");
                            logingObj.put("ResponseData", postContentObj);

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "Error!!! Your like rating point already added your account");
                            logingObj.put("ResponseData", postContentObj);
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
