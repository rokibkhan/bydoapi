<%-- 
    Document   : memberDashboard
    Created on : Nov 13, 2020, 2:22:20 AM
    Author     : Fabiha Tasneem
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("personalProfile_jsp.class");
    String key = "";
    String memberId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray videoObjArray = new JSONArray();

    JSONArray profileObjArray = new JSONArray();
    JSONObject profileObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberID")) {
        key = request.getParameter("key").trim();
        memberId = request.getParameter("memberID").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encryptedString = new String((cipherText), "UTF-8");
    logger.info("EncryptedString  " + encryptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("BYDO :: API :: personalProfile rec :: " + rec);
    if (rec == 1) {
        try {
            String memberName = "";
            String picture = "";
            String degree = "";
            String university = "";
            String balance = "00.00";
            String tuitionWeek = "0";
            String tuitionMonth = "0";
            String memberTypeId = "0";
            String videoProgress = "0";
            String videoCount = "3";
            String totalVideoCount = "10";
            String videoId = "0";
            String courseId = "0";
            String sessionId = "0";
            String chapterId = "0";
            String chapterTitle = "";
            String videoSerial = "1";
            String showingOrder = "0";
            String videoTitle = "";
            String videoLink = "";
            String nextVideoId = "";
            String nextVideoTitle = "";
            String nextVideoLink = "";
            String nextChapterId = "0";
            String nextChapterTitle = "";

            Query studentSQL = dbsession.createSQLQuery("SELECT member_name, picture_name"
                    + " FROM  member where id = '" + memberId + "'");
            if (!studentSQL.list().isEmpty()) {

                for (Iterator it = studentSQL.list().iterator(); it.hasNext();) {
                    Object[] obj = (Object[]) it.next();
                    memberName = obj[0] == null ? "No name" : obj[0].toString().trim();
                    picture = obj[1] == null ? GlobalVariable.baseUrl + "/upload/no_image.jpg" : obj[1].toString().trim();
                }

                String uniSQL = "SELECT institute_name"
                        + " FROM member_education_info where member_id = '" + memberId + "'";

                university = dbsession.createSQLQuery(uniSQL).uniqueResult() == null ? ""
                        : dbsession.createSQLQuery(uniSQL).uniqueResult().toString();
                String eduSQL = "SELECT custom_title"
                        + " FROM member_custom_title where member_id = '" + memberId + "'";

                degree = dbsession.createSQLQuery(eduSQL).uniqueResult() == null ? ""
                        : dbsession.createSQLQuery(eduSQL).uniqueResult().toString();

                String balanceSQL = "SELECT balance"
                        + " FROM  member_wallet where member_id = '" + memberId + "'";

                balance = dbsession.createSQLQuery(balanceSQL).uniqueResult() == null ? "00.00"
                        : dbsession.createSQLQuery(balanceSQL).uniqueResult().toString();

                String memtypeSQL = "SELECT member_type_id"
                        + " FROM  member_type where member_id = '" + memberId + "'";
                memberTypeId = dbsession.createSQLQuery(memtypeSQL).uniqueResult() == null ? "0"
                        : dbsession.createSQLQuery(memtypeSQL).uniqueResult().toString();

                profileObj.put("MemberTypeId", memberTypeId);
                profileObj.put("MemberName", memberName);
                profileObj.put("Degree", degree);
                profileObj.put("University", university);
                profileObj.put("Picture", picture);
                profileObj.put("Balance", balance);
                profileObj.put("TuitionWeek", tuitionWeek);
                profileObj.put("TuitionMonth", tuitionMonth);
                if (memberTypeId.equals("2")) {
                    //Teacher
                    JSONObject videoObj = new JSONObject();
                    videoObj.put("CourseId", courseId);
                    videoObj.put("ChapterTitle", chapterTitle);
                    videoObj.put("VideoId", videoId);
                    videoObj.put("VideoTitle", videoTitle);
                    videoObj.put("VideoLink", videoLink);
                    videoObj.put("VideoProgress", videoProgress);
                    videoObj.put("VideoCount", videoCount);
                    videoObj.put("TotalVideoCount", totalVideoCount);
                    videoObj.put("NextVideoId", nextVideoId);
                    videoObj.put("NextVideoTitle", nextVideoTitle);
                    videoObj.put("NextChapterTitle", nextChapterTitle);
                    videoObj.put("NextVideoLink", nextVideoLink);
                    videoObjArray.add(videoObj);

                    profileObj.put("VideoList", videoObjArray);
                    profileObjArray.add(profileObj);

                    responseDataObj.put("ResponseCode", "1");
                    responseDataObj.put("ResponseText", "Found!");
                    responseDataObj.put("ResponseData", profileObjArray);
                } else if (memberTypeId.equals("1")) {
                    //Student
                    Query courseSQL = dbsession.createSQLQuery("SELECT course_id "
                            + "FROM member_course WHERE member_id = '" + memberId + "'");
                    if (!courseSQL.list().isEmpty()) {

                        for (Iterator it = courseSQL.list().iterator(); it.hasNext();) {
                            String str = it.next().toString();
                            courseId = str == null ? "0" : str.trim();

                            String lastVideoSQL = "SELECT MAX(id) FROM member_video_view_history WHERE member_id = '" + memberId
                                    + "' AND course_id = '" + courseId + "'";
                            videoSerial = dbsession.createSQLQuery(lastVideoSQL).uniqueResult() == null ? "0"
                                    : dbsession.createSQLQuery(lastVideoSQL).uniqueResult().toString();

                            Query idSQL = dbsession.createSQLQuery("SELECT video_id, session_id, chapter_id "
                                    + "FROM member_video_view_history WHERE id = '" + videoSerial + "'");

                            if (!idSQL.list().isEmpty()) {
                                for (Iterator it2 = idSQL.list().iterator(); it2.hasNext();) {
                                    Object[] obj2 = (Object[]) it2.next();
                                    videoId = obj2[0] == null ? "0" : obj2[0].toString().trim();
                                    sessionId = obj2[1] == null ? "0" : obj2[1].toString().trim();
                                    chapterId = obj2[2] == null ? "0" : obj2[2].toString().trim();
                                }
                            } else {
                                videoId = "0";
                                sessionId = "0";
                                chapterId = "0";
                            }

                            String chapSQL = "SELECT chapter_name FROM chapter_info WHERE id_chapter = '" + chapterId + "'";
                            chapterTitle = dbsession.createSQLQuery(chapSQL).uniqueResult() == null ? "0"
                                    : dbsession.createSQLQuery(chapSQL).uniqueResult().toString();

                            Query videoSQL = dbsession.createSQLQuery("SELECT video_title, showing_order, video_emded_link "
                                    + "FROM video_gallery_info WHERE id_video = '" + videoId + "'");

                            if (!videoSQL.list().isEmpty()) {
                                for (Iterator it3 = videoSQL.list().iterator(); it3.hasNext();) {
                                    Object[] obj3 = (Object[]) it3.next();
                                    videoTitle = obj3[0] == null ? "No video to show" : obj3[0].toString().trim();
                                    showingOrder = obj3[1] == null ? "0" : obj3[1].toString().trim();
                                    videoLink = obj3[2] == null ? "" : obj3[2].toString().trim();
                                }
                            } else {
                                videoTitle = "No video to show";
                                showingOrder = "";
                                videoLink = "";
                            }

                            Query nextVideoSQL = dbsession.createSQLQuery("SELECT id_video, chapter_id, video_title, video_emded_link "
                                    + "FROM video_gallery_info WHERE showing_order = '" + (Integer.parseInt(showingOrder) + 1) + "'");

                            if (!nextVideoSQL.list().isEmpty()) {
                                for (Iterator it4 = nextVideoSQL.list().iterator(); it4.hasNext();) {
                                    Object[] obj4 = (Object[]) it4.next();
                                    nextVideoId = obj4[0] == null ? "0" : obj4[0].toString().trim();
                                    nextChapterId = obj4[1] == null ? "" : obj4[1].toString().trim();
                                    nextVideoTitle = obj4[2] == null ? "" : obj4[2].toString().trim();
                                    nextVideoLink = obj4[3] == null ? "" : obj4[3].toString().trim();
                                }
                                String checkSessionSQL = "SELECT session_id "
                                        + "FROM chapter_info WHERE id_chapter = '" + nextChapterId + "'";
                                String sId = dbsession.createSQLQuery(checkSessionSQL).uniqueResult() == null ? "0"
                                        : dbsession.createSQLQuery(checkSessionSQL).uniqueResult().toString();
                                if (sId.equals("0")) {
                                    nextVideoTitle = "This session is complete!";
                                    nextVideoLink = "";
                                    nextVideoId = "0";
                                } else if (sId.equals(sessionId)) {
                                    String nextChapSQL = "SELECT chapter_name"
                                            + " FROM chapter_info WHERE id_chapter = '" + nextChapterId + "'";
                                    nextChapterTitle = dbsession.createSQLQuery(nextChapSQL).uniqueResult() == null ? "0"
                                            : dbsession.createSQLQuery(nextChapSQL).uniqueResult().toString();
                                }
                            } else {
                                nextVideoTitle = "This session is complete!";
                                nextVideoLink = "";
                                nextVideoId = "0";
                                nextChapterTitle = "";
                            }

                            String progressSQL = "SELECT COUNT(DISTINCT video_id)"
                                    + " FROM  member_video_view_history WHERE member_id = '" + memberId
                                    + "' AND course_id = '" + courseId + "'";
                            videoCount = dbsession.createSQLQuery(progressSQL).uniqueResult() == null ? "0"
                                    : dbsession.createSQLQuery(progressSQL).uniqueResult().toString();

                            String totalVideoSQL = "SELECT TOTAL_VIDEO_COUNT FROM course_video_info WHERE COURSE_ID = '" + courseId + "'";
                            totalVideoCount = dbsession.createSQLQuery(totalVideoSQL).uniqueResult() == null ? "0"
                                    : dbsession.createSQLQuery(totalVideoSQL).uniqueResult().toString();

                            videoProgress = String.valueOf((Integer.parseInt(videoCount) * 100) / Integer.parseInt(totalVideoCount));

                            JSONObject videoObj = new JSONObject();
                            videoObj.put("CourseId", courseId);
                            videoObj.put("ChapterTitle", chapterTitle);
                            videoObj.put("VideoId", videoId);
                            videoObj.put("VideoTitle", videoTitle);
                            videoObj.put("VideoLink", videoLink);
                            videoObj.put("VideoProgress", videoProgress);
                            videoObj.put("VideoCount", videoCount);
                            videoObj.put("TotalVideoCount", totalVideoCount);
                            videoObj.put("NextVideoId", nextVideoId);
                            videoObj.put("NextVideoTitle", nextVideoTitle);
                            videoObj.put("NextChapterTitle", nextChapterTitle);
                            videoObj.put("NextVideoLink", nextVideoLink);
                            videoObjArray.add(videoObj);
                        }
                        profileObj.put("VideoList", videoObjArray);
                        profileObjArray.add(profileObj);
                    }
                    responseDataObj.put("ResponseCode", "1");
                    responseDataObj.put("ResponseText", "Found!");
                    responseDataObj.put("ResponseData", profileObjArray);
                } else {
                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "0");
                    responseDataObj.put("ResponseText", "NotFound");
                    responseDataObj.put("ResponseData", profileObjArray);
                }
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", profileObjArray);
            }

            //responseDataObjArray.add(responseDataObj);
        } catch (Exception e) {
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", bannerObjArray);
        //responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }

    dbsession.flush();

    dbsession.close();
%>