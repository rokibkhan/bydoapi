<%-- 
    Document   : getSubjectInfo
    Created on : Oct 6, 2020, 2:09:45 PM
    Author     : rokib
--%>



<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getSubjectInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String coursID = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("courseID")) {
        key = request.getParameter("key").trim();
        coursID = request.getParameter("courseID").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query subjectSQL = null;
    Object subjectObj[] = null;

    String subjectId = "";
    String subjectName = "";
    String subjectDesc = "";
    String subjectStatus = "";
    String subjectImage = "";

    JSONArray subjectResponseObjArr = new JSONArray();
    JSONObject subjectResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    JSONArray subjectDataObjArr = new JSONArray();
    JSONObject subjectDataObj = new JSONObject();
    
    JSONArray sessionDataObjArr = new JSONArray();
    JSONObject sessionDataObj = new JSONObject();

    JSONArray chapterDataObjArr = new JSONArray();
    JSONObject chapterDataObj = new JSONObject();
    
    
    JSONArray videoDataObjArr = new JSONArray();
    JSONObject videoDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("BYDO :: API :: getSubjectInfo rec :: " + rec);
    if (rec == 1) {

        try {

            subjectSQL = dbsession.createSQLQuery("SELECT si.id_subject,si.subject_name,si.subject_desc,si.published,si.feature_image,"
                    + " si.course_id  FROM subject_info si"
                    
                    +" ORDER BY si.id_subject ASC");
            //subjectSQL = dbsession.createSQLQuery("SELECT * FROM subject  ORDER BY subject_long_name ASC");

            if (!subjectSQL.list().isEmpty()) {
                for (Iterator it = subjectSQL.list().iterator(); it.hasNext();) {
                    
                    //subjectDataObj = new JSONObject();

                    subjectObj = (Object[]) it.next();
                    subjectId = subjectObj[0].toString();
                    subjectName = subjectObj[1].toString();
                    subjectDesc = subjectObj[2].toString() == null ? "" : subjectObj[2].toString();
                    subjectStatus = subjectObj[3].toString();
                    subjectImage = subjectObj[4].toString() == null ? "" : subjectObj[4].toString();
                      
                 //   subjectStatus = "0";

                    subjectResponseObj = new JSONObject();
                    subjectResponseObj.put("SubjectId", subjectId);
                    subjectResponseObj.put("SubjectName", subjectName);
                    subjectResponseObj.put("SubjectDesc", subjectDesc);
                    subjectResponseObj.put("SubjectStatus", subjectStatus);
                    subjectResponseObj.put("SubjectImage", GlobalVariable.imageDirLink +subjectImage);
                    
                    Object obj2[] = null;
                    sessionDataObjArr = new JSONArray();                     
                    String SessionName = "";
                    String IdSession = "";
                    String SFeatureImage = "";
                    String SessionDesc = "";
                    Query sesSQL = dbsession.createSQLQuery("SELECT id_session ,session_name, session_desc,feature_image"
                            + " FROM  session_info where subject_id = '" + subjectId + "'");
                    
                    //System.out.println("session start");
                    
                    //System.out.println("SubjectID:"+subjectId);

                    if (!sesSQL.list().isEmpty()) {
                        for (Iterator it2 = sesSQL.list().iterator(); it2.hasNext();) {

                            
                            sessionDataObj = new JSONObject();
                            //obj = (SubjectInfo) it1.next();
                            obj2 = (Object[]) it2.next();

                            //System.out.println("in sessionid:");
                            
                            IdSession = obj2[0].toString();
                            
                            //System.out.println("in sessionid0:"+IdSession);
                            SessionName = obj2[1].toString();
                            SessionDesc = obj2[2].toString();
                            SFeatureImage = obj2[3].toString() == null ? "" : obj2[3].toString() ;
                            
                            //System.out.println("in sessionid:"+IdSession);
                            
                            // chapter info start
                            
                            Object obj3[] = null;
                            chapterDataObjArr = new JSONArray();       
                            String ChapterName = "";
                            String ChapterShortName = "";
                            String IdChapter = "";
                            String ChapterFeatureImage = "";
                            String ChapterDesc = "";
                            
                            Query chapSQL = dbsession.createSQLQuery("SELECT id_chapter ,chapter_name,chapter_short_name,chapter_desc,feature_image"
                                    + " FROM  chapter_info where session_id = '" + IdSession + "'");

                            if (!chapSQL.list().isEmpty()) {
                                for (Iterator it3 = chapSQL.list().iterator(); it3.hasNext();) {


                                    chapterDataObj =  new JSONObject();
                                    obj3 = (Object[]) it3.next();

                                    IdChapter = obj3[0].toString().trim();
                                    ChapterName = obj3[1].toString().trim();
                                    ChapterShortName = obj3[2].toString().trim();
                                    ChapterDesc = obj3[3].toString().trim();
                                    ChapterFeatureImage = obj3[4].toString().trim() == null ? "" : obj3[4].toString().trim() ;
                                    
                                    // viideo start
                                    
                                    Object obj4[] = null;
                                    videoDataObjArr = new JSONArray();    
                                    String VideoTitle = "";
                                    String VideoEmbedLink = "";
                                    String IdVideo = "";                                    
                                    String VideoCaption = "";
                                    String VideoFeatureImage = "";
                                    String VideoTeacherID = "";
                                    String VideoCategory = "";
                                    String VideoPaymentTypeID = "";
                                    String VideoLinkTypeID = "";
                                    
                                    Query videoSQL = dbsession.createSQLQuery("SELECT id_video ,video_title,video_emded_link,video_caption"
                                            + ",feature_image,teacher_id,video_category,payment_type_id,link_type_id"
                                            + " FROM  video_gallery_info where chapter_id = '" + IdChapter + "'");

                                    if (!videoSQL.list().isEmpty()) {
                                        for (Iterator it4 = videoSQL.list().iterator(); it4.hasNext();) {
                                            
                                            videoDataObj = new JSONObject();
                                            //obj = (SubjectInfo) it1.next();
                                            obj4 = (Object[]) it4.next();
                                            
                                            IdVideo = obj4[0].toString().trim();
                                            VideoTitle = obj4[1].toString().trim();
                                            VideoEmbedLink = obj4[2].toString().trim();
                                            VideoCaption = obj4[3].toString().trim();
                                            VideoFeatureImage = obj4[4].toString().trim() == null ? "" : obj4[4].toString().trim();
                                            VideoCategory = obj4[5].toString().trim();
                                            VideoPaymentTypeID = obj4[6].toString().trim();
                                            VideoLinkTypeID = obj4[7].toString().trim();
                                            

                                            videoDataObj.put("VideoId", IdVideo);
                                            videoDataObj.put("VideoTitle", VideoTitle);
                                            videoDataObj.put("VideoEmbedLink", VideoEmbedLink);
                                            //sessionDataObj.put("SessionStatus", subjectStatus);
                                            videoDataObj.put("VideoFeatureImage", VideoFeatureImage);
                                            videoDataObj.put("VideoCategory", VideoCategory);
                                            videoDataObj.put("VideoPaymentTypeID", VideoPaymentTypeID);
                                            videoDataObj.put("VideoLinkTypeID", VideoLinkTypeID);
                                            videoDataObjArr.add(videoDataObj);
                                        }
                                    }
                                    
                                    chapterDataObj.put("VideoList", videoDataObjArr);
                                    
                                    // video end
                                    

                                    chapterDataObj.put("ChapterId", IdChapter);
                                    chapterDataObj.put("ChapterName", ChapterName);
                                    chapterDataObj.put("ChapterDesc", ChapterDesc);
                                    //sessionDataObj.put("SessionStatus", subjectStatus);
                                    chapterDataObj.put("ChapterImage", GlobalVariable.imageDirLink +ChapterFeatureImage);
                                    chapterDataObjArr.add(chapterDataObj);
                                }
                            }

                            // chapter info end
                            sessionDataObj.put("ChapterList", chapterDataObjArr);
                            
                            
                            //System.out.println("session");
                            sessionDataObj.put("SessionId", IdSession);
                            sessionDataObj.put("SessionName", SessionName);
                            sessionDataObj.put("SessionDesc", SessionDesc);
                            //sessionDataObj.put("SessionStatus", subjectStatus);
                            sessionDataObj.put("SessionImage", GlobalVariable.imageDirLink +SFeatureImage);
                            sessionDataObjArr.add(sessionDataObj);
                        }
                    }

                    
                    subjectResponseObj.put("SessionList", sessionDataObjArr);
                    subjectResponseObjArr.add(subjectResponseObj);
                    //subjectDataObj.put(subjectName, subjectResponseObjArr);

                 //   subjectStatus = "0";

                }

                //subjectDataObj.put("Courses", subjectResponseObjArr);
                //   jsonThanaObjArr = new JSONArray();
                
                //subjectDataObjArr.add(subjectDataObj);

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", subjectResponseObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", subjectResponseObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", subjectResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
