<%-- 
    Document   : getDistrictWiseThanaInfo
    Created on : OCT 28, 2019, 1:32:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getDistrictWiseThanaInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String districtId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("districtId")) {
        key = request.getParameter("key").trim();

        districtId = request.getParameter("districtId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query thanaSQL = null;
    Object thanaObj[] = null;
    String thanaId = "";
    String thanaName = "";

    JSONArray jsonThanaObjArr = new JSONArray();
    JSONObject jsonThanaObj = new JSONObject();

    JSONObject productObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("JoyBangla :: API :: getDistrictWiseThanaInfo rec :: " + rec);
    if (rec == 1) {

        try {

            thanaSQL = dbsession.createSQLQuery("SELECT  * FROM thana  WHERE DISTRICT_ID ='" + districtId + "' ORDER BY THANA_NAME  ASC");

            if (!thanaSQL.list().isEmpty()) {
                for (Iterator it = thanaSQL.list().iterator(); it.hasNext();) {

                    thanaObj = (Object[]) it.next();
                    thanaId = thanaObj[0].toString().trim();
                    thanaName = thanaObj[1].toString();

                    jsonThanaObj = new JSONObject();

                    jsonThanaObj.put("thanaId", thanaId);
                    jsonThanaObj.put("thanaName", thanaName);

                    jsonThanaObjArr.add(jsonThanaObj);

                }

                productObj = new JSONObject();
                productObj.put("ResponseCode", "1");
                productObj.put("ResponseText", "Found");
                productObj.put("ResponseData", jsonThanaObjArr);
            } else {
                productObj = new JSONObject();
                productObj.put("ResponseCode", "0");
                productObj.put("ResponseText", "Thana not found");
                productObj.put("ResponseData", jsonThanaObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(productObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        productObj = new JSONObject();
        productObj.put("ResponseCode", "999");
        productObj.put("ResponseText", "ValidationFailed");
        productObj.put("ResponseData", jsonThanaObjArr);
//                        productObjArray.add(productObj);
        PrintWriter writer = response.getWriter();
        writer.write(productObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>