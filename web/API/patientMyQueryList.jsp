<%-- 
    Document   : patientMyQueryList
    Created on : MAY 06, 2020, 10:35:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientMyQueryList_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: patientMyQueryList rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String contentId = "";
    String contentCategory = "";
    String contentType = "";
    String contentTitle = "";
    String contentShortDesc = "";
    String contentDesc = "";
    String contentDate = "";
    String contentPostLink = "";
    String contentPostPicture = "";
    String contentPostShare = "";

    String pictureOpt = "";
    String videoOpt = "";

    String contentOwnerId = "";
    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";

    String ownContentFlag = "";

    // String contentTotalLikeCount = "";
    //  String contentTotalCommentCount = "";
    //  String contentTotalShareCount = "";
    int totalLikeCount = 0;
    String totalLikeCountText = "";
    int totalCommentCount = 0;
    int totalShareCount = 0;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    String pQuerySQL = null;
    String pQueryCountSQL = null;
    Query pQuerySQLQry = null;
    Object[] pQueryObj = null;

    String pQueryId = "";
    String pQueryParent = "";
    String pQueryPatientId = "";
    String pQueryProblemCategory = "";
    String pQueryDesc = "";
    String pQuerySufferingDate = "";
    String pQueryDate = "";
    String pQueryStatus = "";
    String pQueryReplyStatus = "";
    String pQueryCommentType = "";
    String pQueryCommenterId = "";

    String pQueryProblemCategoryId = "";
    String pQueryProblemCategoryName = "";

    String fever = "";
    String cough = "";
    String tiredness = "";
    String nasalcongestion = "";
    String headache = "";

    String feverText = "";
    String coughText = "";
    String tirednessText = "";
    String nasalcongestionText = "";
    String headacheText = "";

    String patientName = "";
    String patientEmail = "";
    String patientPhone = "";
    String patientDOB = "";
    String patientGender = "";

    String queryOwnerId = "";
    String queryOwnerName = "";
    String queryOwnerPic = "";
    String queryOwnerPicLink = "";

    JSONObject doctorInfoResponseObj = new JSONObject();
    JSONArray doctorInfoObjArray = new JSONArray();

    String assignDoctor = "";

    String doctorSQL = null;
    Query doctorSQLQry = null;
    Object[] doctorObj = null;

    String doctorId = "";
    String doctorName = "";
    String doctorPic = "";
    String doctorPicLink = "";
    String doctorMobile = "";
    String doctorPhone1 = "";
    String doctorPhone2 = "";
    String doctorEmail = "";

    String doctorRegNo = "";
    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorMedicalCollege = "";

    int doctorDCount = 0;

    String appointmentSQL = "";
    Query appointmentSQLQry = null;
    Object[] appointmentObj = null;

    String appointmentId = "";
    String appointmentDate = "";
    String appointmentDateTime = "";
    String appointmentStatus = "";
    String appointmentTimeSlotShortName = "";
    String appointmentTimeSlotName = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
//                        if (!filter.equals("")) {
//                            filterSQLStr = " AND pc.post_content_title LIKE '%" + filter + "%' ";
//                        } else {
//                            filterSQLStr = "";
//                        }

                        if (!filter.equals("")) {
                            filterSQLStr = " AND pq.query_desc LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM post_content pc "
//                                + "LEFT JOIN member AS m ON m.id = pc.content_owner "
//                                + "WHERE pc.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pc.id_post_content DESC";
//                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
//                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
//                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
//                                + "WHERE pq.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pq.mod_date DESC";
                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
                                + "WHERE pq.published = 1 AND pq.parent = 0 AND pq.commenter_id = '" + memId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC";

                        String numrow1 = dbsession.createSQLQuery(pQueryCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pQueryCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        pQuerySQL = "SELECT  pq.query_id, pq.parent, pq.patient_id,pq.problem_category,"
                                + "pq.query_desc,pq.suffering_date,pq.query_date,pq.status,pq.reply_status,"
                                + "pq.comment_type,pq.commenter_id,"
                                + "pqc.fever, pqc.cough, pqc.tiredness, pqc.nasalcongestion, pqc.headache,  "
                                + "pi.patient_name,pi.patient_email, pi.patient_phone, pi.patient_dob, pi.patient_gender,  "
                                + "m.id queryOwnerId,m.member_name  queryOwnerName, m.picture_name queryOwnerPic,  "
                                + "pq.assign_doctor   "
                                + "FROM patient_query pq "
                                + "LEFT JOIN patient_query_covid AS pqc ON pq.query_id = pqc.query_id "
                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
                                + "WHERE pq.published = 1 AND pq.parent = 0  AND pq.commenter_id = '" + memId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        System.out.println("pQuerySQL :" + pQuerySQL);

                        pQuerySQLQry = dbsession.createSQLQuery(pQuerySQL);

                        for (Iterator pQueryItr = pQuerySQLQry.list().iterator(); pQueryItr.hasNext();) {

                            pQueryObj = (Object[]) pQueryItr.next();

                            pQueryId = pQueryObj[0].toString();
                            pQueryParent = pQueryObj[1] == null ? "" : pQueryObj[1].toString();
                            pQueryPatientId = pQueryObj[2] == null ? "" : pQueryObj[2].toString();
                            pQueryProblemCategory = pQueryObj[3] == null ? "" : pQueryObj[3].toString();
                            pQueryDesc = pQueryObj[4] == null ? "" : pQueryObj[4].toString();
                            pQuerySufferingDate = pQueryObj[5] == null ? "" : pQueryObj[5].toString();
                            pQueryDate = pQueryObj[6] == null ? "" : pQueryObj[6].toString();
                            pQueryStatus = pQueryObj[7] == null ? "" : pQueryObj[7].toString();
                            pQueryReplyStatus = pQueryObj[8] == null ? "" : pQueryObj[8].toString();
                            pQueryCommentType = pQueryObj[9] == null ? "" : pQueryObj[9].toString();
                            pQueryCommenterId = pQueryObj[10] == null ? "" : pQueryObj[10].toString();

                            fever = pQueryObj[11] == null ? "" : pQueryObj[11].toString();
                            if (fever.equals("1")) {
                                feverText = "Yes";
                            } else {
                                feverText = "No";
                            }
                            cough = pQueryObj[12] == null ? "" : pQueryObj[12].toString();
                            if (cough.equals("1")) {
                                coughText = "Yes";
                            } else {
                                coughText = "No";
                            }
                            tiredness = pQueryObj[13] == null ? "" : pQueryObj[13].toString();
                            if (tiredness.equals("1")) {
                                tirednessText = "Yes";
                            } else {
                                tirednessText = "No";
                            }
                            nasalcongestion = pQueryObj[14] == null ? "" : pQueryObj[14].toString();
                            if (nasalcongestion.equals("1")) {
                                nasalcongestionText = "Yes";
                            } else {
                                nasalcongestionText = "No";
                            }
                            headache = pQueryObj[15] == null ? "" : pQueryObj[15].toString();
                            if (headache.equals("1")) {
                                headacheText = "Yes";
                            } else {
                                headacheText = "No";
                            }

                            patientName = pQueryObj[16] == null ? "" : pQueryObj[16].toString();
                            patientEmail = pQueryObj[17] == null ? "" : pQueryObj[17].toString();
                            patientPhone = pQueryObj[18] == null ? "" : pQueryObj[18].toString();
                            patientDOB = pQueryObj[19] == null ? "" : pQueryObj[19].toString();
                            patientGender = pQueryObj[20] == null ? "" : pQueryObj[20].toString();

                            queryOwnerId = pQueryObj[21] == null ? "" : pQueryObj[21].toString();
                            queryOwnerName = pQueryObj[22] == null ? "" : pQueryObj[22].toString();
                            queryOwnerPic = pQueryObj[23] == null ? "" : pQueryObj[23].toString();
                            queryOwnerPicLink = GlobalVariable.imageMemberDirLink + queryOwnerPic;

//                            mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + contentOwnerId + "' AND SYSDATE() between mt.ed and mt.td");
//
//                            for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {
//
//                                mMemberTypeObj = (Object[]) mMemberTypeItr.next();
//                                mMemberTypeId = mMemberTypeObj[0].toString();
//                                mMemberTypeName = mMemberTypeObj[1].toString();
//
//                            }
                            assignDoctor = pQueryObj[24] == null ? "" : pQueryObj[24].toString();

                            System.out.println("pQueryId :: " + pQueryId);
                            System.out.println("assignDoctorId :: " + assignDoctor);

                       //     if (pQueryStatus.equals("1")) {

                                doctorSQL = "SELECT m.id, m.member_name,m.picture_name, "
                                        + "m.mobile, m.phone1, m.phone2, m.email_id,"
                                        + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                                        + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege  "
                                        + "FROM  member m ,member_type mt,member_additional_info ai "
                                        + "WHERE m.status = 1 AND m.id = '" + assignDoctor + "'"
                                        + "AND mt.member_type_id = 2 "
                                        + "AND m.id = mt.member_id "
                                        + "AND m.id = ai.member_id ";

                                System.out.println("doctorSQL :: " + doctorSQL);

                                doctorSQLQry = dbsession.createSQLQuery(doctorSQL);
                                if (!doctorSQLQry.list().isEmpty()) {
                                    for (Iterator doctorItr = doctorSQLQry.list().iterator(); doctorItr.hasNext();) {

                                        doctorObj = (Object[]) doctorItr.next();

                                        doctorId = doctorObj[0].toString();

                                        doctorName = doctorObj[1] == null ? "" : doctorObj[1].toString();
                                        doctorPic = doctorObj[2] == null ? "" : doctorObj[2].toString();

                                        doctorPicLink = GlobalVariable.imageMemberDirLink + doctorPic;

                                        doctorMobile = doctorObj[3] == null ? "" : doctorObj[3].toString();
                                        doctorPhone1 = doctorObj[4] == null ? "" : doctorObj[4].toString();
                                        doctorPhone2 = doctorObj[5] == null ? "" : doctorObj[5].toString();
                                        doctorEmail = doctorObj[6] == null ? "" : doctorObj[6].toString();

                                        doctorDegree0 = doctorObj[7] == null ? "" : doctorObj[7].toString();
                                        doctorDegree1 = doctorObj[8] == null ? "" : doctorObj[8].toString();
                                        doctorDegree2 = doctorObj[9] == null ? "" : doctorObj[9].toString();
                                        doctorDegree3 = doctorObj[10] == null ? "" : doctorObj[10].toString();
                                        doctorDegree4 = doctorObj[11] == null ? "" : doctorObj[11].toString();
                                        doctorRegNo = doctorObj[12] == null ? "" : doctorObj[12].toString();
                                        doctorMedicalCollege = doctorObj[13] == null ? "" : doctorObj[13].toString();

                                    }

                                }
//                            } else {
//
//                                doctorId = "";
//                                doctorName = "";
//                                doctorPic = "";
//                                doctorPicLink = "";
//                                doctorMobile = "";
//                                doctorPhone1 = "";
//                                doctorPhone2 = "";
//                                doctorEmail = "";
//
//                                doctorRegNo = "";
//                                doctorDegree0 = "";
//                                doctorDegree1 = "";
//                                doctorDegree2 = "";
//                                doctorDegree3 = "";
//                                doctorDegree4 = "";
//                                doctorMedicalCollege = "";
//                            }

                            appointmentSQL = "SELECT pa.id,pa.status ,pa.appointment_time, "
                                    + "ts.short_name,ts.name,ts.time_text,"
                                    + "dts.timeslot_date "
                                    + "FROM  patient_appointment pa ,doctor_timeslot dts,doctor_timeslot_name ts "
                                    + "WHERE pa.pquery_id = '" + pQueryId + "'"
                                    + "AND pa.member_id = '" + assignDoctor + "' "
                                    + "AND pa.doctor_timeslot_id = dts.id "
                                    + "AND dts.timeslot_id = ts.id";

                            System.out.println("appointmentSQL :: " + appointmentSQL);

                            appointmentSQLQry = dbsession.createSQLQuery(appointmentSQL);
                            if (!appointmentSQLQry.list().isEmpty()) {
                                for (Iterator appointmentItr = appointmentSQLQry.list().iterator(); appointmentItr.hasNext();) {

                                    appointmentObj = (Object[]) appointmentItr.next();

                                    appointmentId = appointmentObj[0].toString();
                                    appointmentStatus = appointmentObj[1] == null ? "" : appointmentObj[1].toString();
                                    appointmentDateTime = appointmentObj[2] == null ? "" : appointmentObj[2].toString();

                                    appointmentTimeSlotShortName = appointmentObj[3] == null ? "" : appointmentObj[3].toString();
                                    appointmentTimeSlotName = appointmentObj[4] == null ? "" : appointmentObj[4].toString();
                                    //appointmentDate = appointmentObj[1] == null ? "" : appointmentObj[1].toString();
                                }
                            } else {
                                appointmentId = "";
                                appointmentStatus = "";
                                appointmentDateTime = "";

                                appointmentTimeSlotShortName = "";
                                appointmentTimeSlotName = "";

                            }

                            postContentObj = new JSONObject();
                            postContentObj.put("pQueryId", pQueryId);

                            postContentObj.put("appointmentId", appointmentId);
                            //   postContentObj.put("appointmentDate", appointmentDate);
                            postContentObj.put("appointmentDateTime", appointmentDateTime);
                            postContentObj.put("appointmentTimeSlotShortName", appointmentTimeSlotShortName);
                            postContentObj.put("appointmentTimeSlotName", appointmentTimeSlotName);

                            postContentObj.put("assignDoctorId", assignDoctor);

                            postContentObj.put("doctorId", doctorId);
                            postContentObj.put("doctorName", doctorName);
                            postContentObj.put("doctorPicture", doctorPicLink);
                            postContentObj.put("doctorMobile", doctorMobile);
                            postContentObj.put("doctorPhone1", doctorPhone1);
                            postContentObj.put("doctorPhone2", doctorPhone2);
                            postContentObj.put("doctorEmail", doctorEmail);

                            postContentObj.put("doctorDegree0", doctorDegree0);
                            postContentObj.put("doctorDegree1", doctorDegree1);
                            postContentObj.put("doctorDegree2", doctorDegree2);
                            postContentObj.put("doctorDegree3", doctorDegree3);
                            postContentObj.put("doctorDegree4", doctorDegree4);

                            postContentObj.put("doctorRegNo", doctorRegNo);
                            postContentObj.put("doctorMedicalCollege", doctorMedicalCollege);

                            postContentObj.put("patientId", pQueryPatientId);
                            postContentObj.put("patientName", patientName);
                            postContentObj.put("patientEmail", patientEmail);
                            postContentObj.put("patientPhone", patientPhone);
                            postContentObj.put("patientDOB", patientDOB);
                            postContentObj.put("patientGender", patientGender);

                            postContentObj.put("pQueryParent", pQueryParent);
                            postContentObj.put("pQueryPatientId", pQueryPatientId);

                            postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategory);
                            postContentObj.put("pQueryProblemCategoryName", pQueryProblemCategoryName);

                            postContentObj.put("pQueryDesc", pQueryDesc);
                            postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                            postContentObj.put("pQueryDate", pQueryDate);
                            postContentObj.put("pQueryStatus", pQueryStatus);
                            postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                            postContentObj.put("pQueryCommentType", pQueryCommentType);
                            postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                            postContentObj.put("fever", fever);
                            postContentObj.put("feverText", feverText);

                            postContentObj.put("cough", cough);
                            postContentObj.put("coughText", coughText);

                            postContentObj.put("tiredness", tiredness);
                            postContentObj.put("tirednessText", tirednessText);

                            postContentObj.put("nasalcongestion", nasalcongestion);
                            postContentObj.put("nasalcongestionText", nasalcongestionText);

                            postContentObj.put("headache", headache);
                            postContentObj.put("headacheText", headacheText);

                            //   postContentObj.put("queryOwnerType", mMemberTypeName);
                            postContentObj.put("queryOwnerId", queryOwnerId);
                            postContentObj.put("queryOwnerName", queryOwnerName);
                            postContentObj.put("queryOwnerPic", queryOwnerPicLink);

                            postContentObjArr.add(postContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", postContentObjArr);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
