<%-- 
    Document   : doctorInfo
    Created on : APR 30, 2020, 12:43:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberTypeId = "";
    String memberType = "";

    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: doctorInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String memberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    String memberXId = "";
    String memberXIEBId = "";
    String memberXName = "";
    String memberXPhone1 = "";
    String memberXPhone2 = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    String memberCustomOrder = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";
    String unitName = "";
    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";

//    'organizationName' => $organizationName,
//                'orgLogo' => $orgLogo,
//                'unitName' => $posting,
//                'nirbachoniAson' => $nirbachoni_ason,
//                'ratingPoint' => $useRating,
//                'shareCount' => $shareContentPoint,
//                'uploadCount' => $uploadContentPoint,
//                'bloodDonationCount' => $bloodDonationCounter,
    String doctorRegNo = "";
    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorMedicalCollege = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
                        if (!filter.equals("")) {
                            filterSQLStr = " AND (m.member_id LIKE '%" + filter + "%' "
                                    + "OR m.member_name LIKE '%" + filter + "%' "
                                    + "OR m.mobile LIKE '%" + filter + "%' "
                                    + "OR m.email_id LIKE '%" + filter + "%' "
                                    + " ) ";
                        } else {
                            filterSQLStr = "";
                        }

                        memberCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                                + "WHERE m.status = 1 "
                                + "AND mt.member_type_id = 2 "
                                + "AND m.id = mt.member_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY m.id DESC";

                        String numrow1 = dbsession.createSQLQuery(memberCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(memberCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        logger.info("API :: memberInfo API::  currentpage : " + currentpage + " offset : " + offset + "");

//                        memberSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "md.mem_division_name,md.full_name,c.center_name, "
//                                + " m.phone1,m.phone2,mt.member_type_id    "
//                                + "FROM  member m ,member_type mt,member_division md,center c "
//                                + "WHERE m.status = 1 "
//                                + "AND m.member_division_id = md.mem_division_id "
//                                + "AND m.center_id = c.center_id "
//                                + "AND m.id = mt.member_id "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY mt.member_type_id ASC,m.id ASC  "
//                                + "LIMIT " + offset + " , " + rowsperpage + " ";
//                        memberSQL = "SELECT m.id, m.member_id,m.member_name, "
//                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
//                                + "md.mem_division_name,md.full_name,c.center_name, "
//                                + " m.phone1,m.phone2,mt.member_type_id,ml.life_status,co.custom_order    "
//                                + "FROM  member m ,member_type mt,member_custom_order co,member_life_info ml,member_division md,center c "
//                                + "WHERE m.status = 1 "
//                                + "AND m.member_division_id = md.mem_division_id "
//                                + "AND m.center_id = c.center_id "
//                                + "AND m.id = mt.member_id "
//                                + "AND m.id = co.member_id "
//                                + "AND m.id = ml.member_id "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY mt.member_type_id ASC,co.custom_order ASC  "
//                                + "LIMIT " + offset + " , " + rowsperpage + " ";
                        memberSQL = "SELECT m.id, m.member_id,m.member_name, "
                                + "m.picture_name,m.mobile,m.email_id,m.status,   "
                                + " m.phone1,m.phone2,mt.member_type_id,co.custom_order,"
                                + "m.unit_name,m.nirbachoni_ason,    "
                                + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                                + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege  "
                                + "FROM  member m ,member_type mt,member_custom_order co,member_additional_info ai "
                                + "WHERE m.status = 1 "
                                + "AND mt.member_type_id = 2 "
                                + "AND m.id = mt.member_id "
                                + "AND m.id = co.member_id "
                                + "AND m.id = ai.member_id "
                                + " " + filterSQLStr + " "
                                + "ORDER BY mt.member_type_id ASC,co.custom_order ASC  "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        logger.info("API :: memberInfo API memberSQL ::" + memberSQL);

                        memberSQLQry = dbsession.createSQLQuery(memberSQL);

                        for (Iterator memberItr = memberSQLQry.list().iterator(); memberItr.hasNext();) {

                            memberObj = (Object[]) memberItr.next();
                            memberXId = memberObj[0].toString();
                            memberXIEBId = memberObj[1] == null ? "" : memberObj[1].toString();
                            memberXName = memberObj[2] == null ? "" : memberObj[2].toString();

                            memberXPictureName = memberObj[3] == null ? "" : memberObj[3].toString();

                            memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                            logger.info("API :: memberInfo API pictureLink ::" + memberXPictureLink);

                            memberXMobile = memberObj[4] == null ? "" : memberObj[4].toString();
                            memberXEmail = memberObj[5] == null ? "" : memberObj[5].toString();
                            memberXPhone1 = memberObj[7] == null ? "" : memberObj[7].toString();
                            memberXPhone2 = memberObj[8] == null ? "" : memberObj[8].toString();

//                            memberXPhone1View = "1";
//                            memberXPhone2View = "1";
//                            memberXMobileView = "1";
//                            memberXEmailView = "1";
                            mMemberOptionViewSQL = dbsession.createSQLQuery("SELECT phone1_view,phone2_view,mobile_view,email_view FROM member_view_setting WHERE member_id='" + memberXId + "'");

                            for (Iterator mMemberOptionViewItr = mMemberOptionViewSQL.list().iterator(); mMemberOptionViewItr.hasNext();) {

                                mMemberOptionViewObj = (Object[]) mMemberOptionViewItr.next();
                                memberXPhone1View = mMemberOptionViewObj[0].toString();
                                memberXPhone2View = mMemberOptionViewObj[1].toString();
                                memberXMobileView = mMemberOptionViewObj[2].toString();
                                memberXMobileView = mMemberOptionViewObj[3].toString();

                            }

                            //  memberXDivisionShort = memberObj[7] == null ? "" : memberObj[7].toString();
                            //   memberXDivisionFull = memberObj[8] == null ? "" : memberObj[8].toString();
                            //   memberXCenter = memberObj[9] == null ? "" : memberObj[9].toString();
                            memberTypeId = memberObj[9] == null ? "" : memberObj[9].toString();

                            if (!memberTypeId.equals("")) {
                                if (memberTypeId.equals("1")) {
                                    memberType = "Member";
                                }
                                if (memberTypeId.equals("2")) {
                                    memberType = "Doctor";
                                }
                            } else {
                                memberType = "";
                            }

                            Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + memberXId + "'");

                            logger.info("API :: Login API memberOrganizationSQL ::" + mOrganizationSQL);

                            if (!mOrganizationSQL.list().isEmpty()) {
                                for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {
                                    //   addressBook = (AddressBook) mAddressItr.next();

                                    mOrganization = (MemberOrganization) mOrganizationItr.next();

                                    organizationName = mOrganization.getOrganizationInfo().getOrganizationName();
                                    organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo();
                                    organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                                }
                            }
                            memberCustomOrder = memberObj[10] == null ? "" : memberObj[10].toString();

                            unitName = memberObj[12] == null ? "" : memberObj[12].toString();
                            nirbachoniAson = memberObj[12] == null ? "" : memberObj[12].toString();

                            doctorDegree0 = memberObj[13] == null ? "" : memberObj[13].toString();
                            doctorDegree1 = memberObj[14] == null ? "" : memberObj[14].toString();
                            doctorDegree2 = memberObj[15] == null ? "" : memberObj[15].toString();
                            doctorDegree3 = memberObj[16] == null ? "" : memberObj[16].toString();
                            doctorDegree4 = memberObj[17] == null ? "" : memberObj[17].toString();

                            doctorRegNo = memberObj[18] == null ? "" : memberObj[18].toString();
                            doctorMedicalCollege = memberObj[19] == null ? "" : memberObj[19].toString();

                            memberContentObj = new JSONObject();
                            memberContentObj.put("Id", memberXId);

                            memberContentObj.put("doctorDegree0", doctorDegree0);
                            memberContentObj.put("doctorDegree1", doctorDegree1);
                            memberContentObj.put("doctorDegree2", doctorDegree2);
                            memberContentObj.put("doctorDegree3", doctorDegree3);
                            memberContentObj.put("doctorDegree4", doctorDegree4);

                            memberContentObj.put("doctorRegNo", doctorRegNo);
                            memberContentObj.put("doctorMedicalCollege", doctorMedicalCollege);

                            memberContentObj.put("organizationName", organizationName);
                            memberContentObj.put("organizationLogo", organizationLogoLink);
                            memberContentObj.put("unitName", unitName);
                            memberContentObj.put("nirbachoniAson", nirbachoniAson);
                            memberContentObj.put("ratingPoint", ratingPoint);
                            memberContentObj.put("likeCount", likeCount);
                            memberContentObj.put("shareCount", shareCount);
                            memberContentObj.put("uploadCount", uploadCount);
                            memberContentObj.put("bloodDonationCount", bloodDonationCount);

                            memberContentObj.put("MemberId", memberXIEBId);
                            memberContentObj.put("MemberTypeId", memberTypeId);
                            memberContentObj.put("MemberType", memberType);
                            memberContentObj.put("MemberLifeStatus", memberLifeStatus);
                            memberContentObj.put("MemberLifeStatusText", memberLifeStatusText);
                            memberContentObj.put("Name", memberXName);
                            memberContentObj.put("Phone1", memberXPhone1);

                            memberContentObj.put("Phone1View", memberXPhone1View);
                            memberContentObj.put("Phone2View", memberXPhone2View);
                            memberContentObj.put("MobileView", memberXMobileView);
                            memberContentObj.put("EmailView", memberXEmailView);

                            memberContentObj.put("Phone2", memberXPhone2);

                            memberContentObj.put("Mobile", memberXMobile);

                            memberContentObj.put("Email", memberXEmail);

                            memberContentObj.put("Picture", memberXPictureLink);
                            memberContentObj.put("CenterName", memberXCenter);
                            memberContentObj.put("DivisionShortName", memberXDivisionShort);
                            memberContentObj.put("DivisionFullName", memberXDivisionFull);

                            memberContentObj.put("MemberCustomOrder", memberCustomOrder);

                            memberContentObjArr.add(memberContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", memberContentObjArr);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
