<%-- 
    Document   : memberUpdateProjectInfo
    Created on : June 19, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    //  ALTER TABLE `joybandb`.`member_address` 
//CHANGE COLUMN `address_type` `address_type` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'P->Permanent Address:M->Mailing Address' ;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    Logger logger = Logger.getLogger("memberUpdateProjectInfo_jsp.class");

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String memberProjectId = "";
    String projectTitle = "";
    String projectCategory = "";
    String projectRole = "";
    String projectDetails = "";
    String startDate = "";
    String endDate = "";
    String jobDesc = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("memberProjectId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        memberProjectId = request.getParameter("memberProjectId").trim();

        projectTitle = request.getParameter("projectTitle") == null ? "" : request.getParameter("projectTitle").trim();
        projectCategory = request.getParameter("projectCategory") == null ? "" : request.getParameter("projectCategory").trim();
        projectRole = request.getParameter("projectRole") == null ? "" : request.getParameter("projectRole").trim();
        projectDetails = request.getParameter("projectDetails") == null ? "" : request.getParameter("projectDetails").trim();
        startDate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate").trim();
        endDate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate").trim();
        

        //   memberName = request.getParameter("name").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);
    
    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberUpdateProjectInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        if (!memberProjectId.equals("0")) {
                            //Update member Address

                            //update member Rating for comment_content_point filed
                            Query updateMemberProjectInfoSQL = dbsession.createSQLQuery("UPDATE  member_project_info SET "
                                    + "member_project_title = '" + projectTitle + "',"
                                    + "member_project_category = '" + projectCategory + "',"
                                    + "member_project_member_role = '" + projectRole + "',"
                                    + "member_project_details='" + projectDetails + "', "
                                    + "member_project_start_date='" + startDate + "',"
                                    + "member_project_end_date='" + endDate + "'"
                                    + "WHERE id = '" + memberProjectId + "'");

                            updateMemberProjectInfoSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Project info updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member Project updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member Project updated");
                            }

                        } else {
                            //Insert member Project

                            String idMA = getId.getID(38);
                            int regMemberProjectId = Integer.parseInt(idMA);

                            Query mProjectAddSQL = dbsession.createSQLQuery("INSERT INTO member_project_info("
                                    + "id,"
                                    + "member_id,"
                                    + "member_project_title,"
                                    + "member_project_category,"
                                    + "member_project_member_role,"
                                    + "member_project_details,"
                                    + "member_project_start_date,"
                                    + "member_project_end_date"
                                    + ") VALUES("
                                    + "" + regMemberProjectId + ","
                                    + "'" + memId + "',"
                                    + "'" + projectTitle + "',"
                                    + "'" + projectCategory + "',"
                                    + "'" + projectRole + "',"
                                    + "'" + projectDetails + "',"
                                    + "'" + startDate + "',"
                                    + "'" + endDate + "'"
                                    + "  ) ");

                            logger.info("mProjectAddSQL ::" + mProjectAddSQL);
                            System.out.println("mProjectAddSQL ::" + mProjectAddSQL);

                            mProjectAddSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Project info Added Successfully");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("User ID :" + memberId + " Member Project info Added Successfully");
                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member Project info Add");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member Project info Add");
                            }
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "ERROR!!! Member Password info wrong ");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("ERROR!!! Member Password info wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
