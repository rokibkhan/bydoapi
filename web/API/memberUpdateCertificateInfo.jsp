<%-- 
    Document   : memberUpdateCertificateInfo
    Created on : June 19, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    //  ALTER TABLE `joybandb`.`member_address` 
//CHANGE COLUMN `address_type` `address_type` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'P->Permanent Address:M->Mailing Address' ;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    Logger logger = Logger.getLogger("memberUpdateCertificateInfo_jsp.class");

    // memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;
    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    //  String filePath = GlobalVariable.imageMemberUploadPath;
    // String filePath = GlobalVariable.imageDirLink + "certificate/";
    String filePath = GlobalVariable.imagePath + "certificate/";

    // certificate
    logger.info("memberUpdateCertificateInfo API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String memberCertificateId = "";
    String shortName = "";
    String longName = "";
    String fileNameXn = "";
    String fileNameXnLink = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("memberCertificateId") && request.getParameterMap().containsKey("imageString")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        memberCertificateId = request.getParameter("memberCertificateId").trim();

        shortName = request.getParameter("shortName") == null ? "" : request.getParameter("shortName").trim();
        longName = request.getParameter("longName") == null ? "" : request.getParameter("longName").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("memberUpdateCertificateInfo API  imageBase64String :" + imageBase64String);

        System.out.println("memberUpdateCertificateInfo API imageBase64String :" + imageBase64String);

        //   fileNameXn = request.getParameter("fileNameXn") == null ? "" : request.getParameter("fileNameXn").trim();
        //   memberName = request.getParameter("name").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberUpdateCertificateInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {

            if (imageBase64String != null) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("memberUpdateProfilePictureInfo API filePath :" + filePath);
                logger.info("memberUpdateProfilePictureInfo API fileName1 :" + fileName1);

                System.out.println("memberUpdateProfilePictureInfo API filePath :" + filePath);
                System.out.println("memberUpdateProfilePictureInfo API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                fileNameXnLink = GlobalVariable.baseUrlImg + "/upload/certificate/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                fileNameXn = fileName1;

                //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
                qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

                if (!qMember.list().isEmpty()) {
                    for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                        member = (Member) itr0.next();
                        memId = member.getId();
                        memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                        dbPass = memberKeySQL.uniqueResult().toString();
                        if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                            if (!memberCertificateId.equals("0")) {
                                //Update member Address

                                //update member Rating for comment_content_point filed
                                Query updateMemberCertificateInfoSQL = dbsession.createSQLQuery("UPDATE  member_certificate SET "
                                        + "certificate_short_name = '" + shortName + "',"
                                        + "certificate_long_name = '" + longName + "',"
                                        + "certificate_file_name='" + fileNameXn + "'"
                                        + "WHERE id = '" + memberCertificateId + "'");

                                updateMemberCertificateInfoSQL.executeUpdate();

                                dbtrx.commit();

                                if (dbtrx.wasCommitted()) {

                                    postContentObj = new JSONObject();
                                    //  postContentObj.put("Id", memId);

                                    //     postContentObj.put("PictureName", memberPictureName);
                                    //    postContentObj.put("PictureLink", memberPictureNameLink);
                                    logingObj = new JSONObject();
                                    logingObj.put("ResponseCode", "1");
                                    logingObj.put("ResponseText", "Member Certificate info updated successfully");
                                    logingObj.put("ResponseData", postContentObj);

                                } else {

                                    logingObj = new JSONObject();
                                    logingObj.put("ResponseCode", "0");
                                    logingObj.put("ResponseText", "ERROR!!! When member Certificate updated ");
                                    logingObj.put("ResponseData", logingObjArray);
                                    logger.info("ERROR!!! When member Certificate updated");
                                }

                            } else {
                                //Insert member Certificate

                                String idMA = getId.getID(39);
                                int regMemberCertificateId = Integer.parseInt(idMA);

                                Query mCertificateAddSQL = dbsession.createSQLQuery("INSERT INTO member_certificate("
                                        + "id,"
                                        + "member_id,"
                                        + "certificate_short_name,"
                                        + "certificate_long_name,"
                                        + "certificate_file_name"
                                        + ") VALUES("
                                        + "" + regMemberCertificateId + ","
                                        + "'" + memId + "',"
                                        + "'" + shortName + "',"
                                        + "'" + longName + "',"
                                        + "'" + fileNameXn + "'"
                                        + "  ) ");

                                logger.info("mCertificateAddSQL ::" + mCertificateAddSQL);
                                System.out.println("mCertificateAddSQL ::" + mCertificateAddSQL);

                                mCertificateAddSQL.executeUpdate();

                                dbtrx.commit();

                                if (dbtrx.wasCommitted()) {

                                    logingObj = new JSONObject();
                                    logingObj.put("ResponseCode", "1");
                                    logingObj.put("ResponseText", "Member Certificate info Added Successfully");
                                    logingObj.put("ResponseData", logingObjArray);
                                    logger.info("User ID :" + memberId + " Member Certificate info Added Successfully");
                                } else {

                                    logingObj = new JSONObject();
                                    logingObj.put("ResponseCode", "0");
                                    logingObj.put("ResponseText", "ERROR!!! When member Certificate info Add");
                                    logingObj.put("ResponseData", logingObjArray);
                                    logger.info("ERROR!!! When member Certificate info Add");
                                }
                            }

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "2");
                            logingObj.put("ResponseText", "ERROR!!! Member Password info wrong ");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("ERROR!!! Member Password info wrong");

                        }

                    }

                } else {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "NotFound");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("User ID :" + memberId + " Not Found");

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Image is empty");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("Image is empty");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
