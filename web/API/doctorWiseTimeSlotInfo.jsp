<%-- 
    Document   : doctorWiseTimeSlotInfo
    Created on : June 26, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorWiseTimeSlotInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String doctorId = "";
    String appointmentDate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId") && request.getParameterMap().containsKey("appointmentDate")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        doctorId = request.getParameter("doctorId").trim();
        appointmentDate = request.getParameter("appointmentDate").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }

    Member member = null;

    int memId = 0;
    String dbPass = "";

    JSONObject logingObj = new JSONObject();
    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    Query memberTypeSQL = null;
    Object memberTypeObj[] = null;

    String memberTypeId = "";
    String memberTypeName = "";

    Query doctorTimeSlotSQL = null;
    Object doctorTimeSlotObj[] = null;

    String doctorTimeSlotId = "";
    String doctorSerialBaseTime = "10";
    String doctorTimeSlotSerialCount = "";
    String doctorTimeSlotMaxSerialCount = "";
    String doctorTimeSlotShortName = "";
    String doctorTimeSlotName = "";
    String doctorTimeSlotTimeText = "";
    String doctorTimeSlotTimeHour = "";
    String doctorTimeSlotTimeMinute = "";
    String doctorTimeSlotTimeNumber = "";

    JSONArray doctorTimeSlotResponseObjArr = new JSONArray();
    JSONObject doctorTimeSlotResponseObj = new JSONObject();

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    Encryption encryption = new Encryption();
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: doctorWiseTimeSlotInfo API rec :: " + rec);

    if (rec == 1) {

        try {
            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {

                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        doctorTimeSlotSQL = dbsession.createSQLQuery("SELECT "
                                + "dts.id,dts.serial_count,dts.max_serial_count,"
                                + "ts.short_name,ts.name,ts.time_text,ts.time_hour, "
                                + "ts.time_minute,ts.time_number  "
                                + "FROM doctor_timeslot dts,doctor_timeslot_name ts "
                                + "WHERE dts.status = 1 "
                                + "AND dts.member_id = " + doctorId + " "
                                + "AND dts.timeslot_date = '" + appointmentDate + "' "
                                + "AND dts.timeslot_id = ts.id "
                                + "ORDER BY ts.custom_order ASC");

                        if (!doctorTimeSlotSQL.list().isEmpty()) {
                            for (Iterator it = doctorTimeSlotSQL.list().iterator(); it.hasNext();) {

                                doctorTimeSlotObj = (Object[]) it.next();
                                doctorTimeSlotId = doctorTimeSlotObj[0].toString();
                                doctorTimeSlotSerialCount = doctorTimeSlotObj[1].toString();
                                doctorTimeSlotMaxSerialCount = doctorTimeSlotObj[2].toString();

                                doctorTimeSlotShortName = doctorTimeSlotObj[3].toString();
                                doctorTimeSlotName = doctorTimeSlotObj[4].toString();
                                doctorTimeSlotTimeText = doctorTimeSlotObj[5].toString();
                                doctorTimeSlotTimeHour = doctorTimeSlotObj[6].toString();
                                doctorTimeSlotTimeMinute = doctorTimeSlotObj[7] == null ? "" : doctorTimeSlotObj[7].toString();
                                doctorTimeSlotTimeNumber = doctorTimeSlotObj[8] == null ? "" : doctorTimeSlotObj[8].toString();

                                doctorTimeSlotResponseObj = new JSONObject();

                                doctorTimeSlotResponseObj.put("DoctorTimeslotId", doctorTimeSlotId);
                                doctorTimeSlotResponseObj.put("SerialBaseTime", doctorSerialBaseTime);
                                doctorTimeSlotResponseObj.put("SerialCountCurrent", doctorTimeSlotSerialCount);
                                doctorTimeSlotResponseObj.put("SerialCountMax", doctorTimeSlotMaxSerialCount);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotShortName", doctorTimeSlotShortName);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotName", doctorTimeSlotName);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotTimeText", doctorTimeSlotTimeText);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotTimeHour", doctorTimeSlotTimeHour);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotTimeMinute", doctorTimeSlotTimeMinute);
                                doctorTimeSlotResponseObj.put("DoctorTimeSlotTimeNumber", doctorTimeSlotTimeNumber);

                                doctorTimeSlotResponseObjArr.add(doctorTimeSlotResponseObj);

                            }

                            responseDataObj = new JSONObject();
                            responseDataObj.put("ResponseCode", "1");
                            responseDataObj.put("ResponseText", "Found");
                            responseDataObj.put("ResponseData", doctorTimeSlotResponseObjArr);
                        } else {
                            responseDataObj = new JSONObject();
                            responseDataObj.put("ResponseCode", "1");
                            responseDataObj.put("ResponseText", "NotFound");
                            responseDataObj.put("ResponseData", doctorTimeSlotResponseObjArr);

                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>