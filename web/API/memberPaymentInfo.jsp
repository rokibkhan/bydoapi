<%-- 
    Document   : memberPaymentInfo
    Created on : DEC 04, 2020, 05:35:11 AM
    Author     : Fabiha
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("memberPaymentInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();
        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("BYDO :: API :: memberPaymentInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONArray walletObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray responseObjArray = new JSONArray();

    String transactionType = "";
    String transactionDate = "";
    String amount = "";
    String accountState = "";
    String updateAccountState = "";
    String description = "";
    String status = "";
    String addUser = "";
    String addDate = "";
    String addTerm = "";
    String addIp = "";
    String modUser = "";
    String modDate = "";
    String modTerm = "";
    String modIp = "";
    int memId = 0;
    if (rec == 1) {
        try {
            
            Query qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();
            Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential"
                    + " WHERE member_id='" + memId + "'");
            if (!memberKeySQL.list().isEmpty()) {
                dbPass = memberKeySQL.uniqueResult().toString() == null ? "" : memberKeySQL.uniqueResult().toString().trim();

                if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {
                    Query walletSQL = dbsession.createSQLQuery("SELECT * "
                            + "FROM member_wallet_history WHERE member_id = '" + memId + "'");

                    if (!walletSQL.list().isEmpty()) {
                        for (Iterator it = walletSQL.list().iterator(); it.hasNext();) {
                            Object[] obj = (Object[]) it.next();
                            transactionType = obj[2] == null ? "0" : obj[2].toString().trim();
                            transactionDate = obj[3] == null ? "0" : obj[3].toString().trim();
                            amount = obj[4] == null ? "0" : obj[4].toString().trim();
                            accountState = obj[5] == null ? "0" : obj[5].toString().trim();
                            updateAccountState = obj[6] == null ? "0" : obj[6].toString().trim();
                            description = obj[7] == null ? "" : obj[7].toString().trim();
                            status = obj[8] == null ? "0" : obj[8].toString().trim();
                            addUser = obj[9] == null ? "" : obj[9].toString().trim();
                            addDate = obj[10] == null ? "" : obj[10].toString().trim();
                            addTerm = obj[11] == null ? "" : obj[11].toString().trim();
                            addIp = obj[12] == null ? "" : obj[12].toString().trim();
                            modUser = obj[13] == null ? "" : obj[13].toString().trim();
                            modDate = obj[14] == null ? "" : obj[14].toString().trim();
                            modTerm = obj[15] == null ? "" : obj[15].toString().trim();
                            modIp = obj[16] == null ? "" : obj[16].toString().trim();

                            JSONObject walletObj = new JSONObject();
                            walletObj.put("TransactionType", transactionType);
                            walletObj.put("TransactionDate", transactionDate);
                            walletObj.put("amount", amount);
                            walletObj.put("accountState", accountState);
                            walletObj.put("updateAccountState", updateAccountState);
                            walletObj.put("description", description);
                            walletObj.put("status", status);
                            walletObj.put("addUser", addUser);
                            walletObj.put("addDate", addDate);
                            walletObj.put("addTerm", addTerm);
                            walletObj.put("addIp", addIp);
                            walletObj.put("modUser", modUser);
                            walletObj.put("modDate", modDate);
                            walletObj.put("modTerm", modTerm);
                            walletObj.put("modIp", modIp);
                            walletObjArr.add(walletObj);
                        }
                        responseObj = new JSONObject();
                        responseObj.put("ResponseCode", "1");
                        responseObj.put("ResponseText", "Found");
                        responseObj.put("ResponseData", walletObjArr);
                    } else {
                        JSONObject walletObj = new JSONObject();
                        walletObj.put("TransactionType", transactionType);
                        walletObj.put("TransactionDate", transactionDate);
                        walletObj.put("Amount", amount);
                        walletObj.put("AccountState", accountState);
                        walletObj.put("UpdateAccountState", updateAccountState);
                        walletObj.put("Description", description);
                        walletObj.put("Status", status);
                        walletObj.put("AddUser", addUser);
                        walletObj.put("AddDate", addDate);
                        walletObj.put("AddTerm", addTerm);
                        walletObj.put("AddIp", addIp);
                        walletObj.put("ModUser", modUser);
                        walletObj.put("ModDate", modDate);
                        walletObj.put("ModTerm", modTerm);
                        walletObj.put("ModIp", modIp);
                        walletObjArr.add(walletObj);

                        responseObj = new JSONObject();
                        responseObj.put("ResponseCode", "1");
                        responseObj.put("ResponseText", "Found");
                        responseObj.put("ResponseData", walletObjArr);
                    }
                } else {
                    responseObj = new JSONObject();
                    responseObj.put("ResponseCode", "2");
                    responseObj.put("ResponseText", "Member password wrong");
                    responseObj.put("ResponseData", responseObjArray);
                }
            } else {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "0");
                responseObj.put("ResponseText", "Not Found");
                responseObj.put("ResponseData", responseObjArray);
            }
        }   
        }
            else                
            {
                responseObj = new JSONObject();
                responseObj.put("ResponseCode", "0");
                responseObj.put("ResponseText", "Not Found");
                responseObj.put("ResponseData", responseObjArray);
            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }
        PrintWriter writer = response.getWriter();
        writer.write(responseObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        responseObj = new JSONObject();
        responseObj.put("ResponseCode", "999");
        responseObj.put("ResponseText", "Key Validation Failed");
        responseObj.put("ResponseData", responseObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(responseObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
