<%-- 
    Document   : bloodRequestDonationInfo
    Created on : APR 29, 2020, 10:46:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("bloodRequestDonationInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";
    String memberName = "";
    String memberEmail = "";
    String memberType = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: bloodRequestDonationInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject bloodDonarInfoObj = new JSONObject();
    JSONArray bloodDonarInfoArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String bloodContentId = "";
    String bloodContentName = "";
    String bloodContentDesc = "";
    String bloodContentMobile = "";
    String bloodContentAddress = "";
    String bloodType = "";
    String bloodReqDate = "";
    String bloodReqPublished = "";

    String bloodDonationStatus = "";

    String memberXId = "";
    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";

    String ownContentFlag = "";

    Object donarObj[] = null;
    String donarSQL = "";
    Query donarSQLQry = null;
    String donateId = "";
    String donarId = "";
    String donarIEBId = "";
    String donarName = "";
    String donarPicture = "";
    String donarPictureLink = "";

    String bloodReqFromId = "";
    String userType = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
                        if (!filter.equals("")) {
                            filterSQLStr = " AND (bc.blood_content_name LIKE '%" + filter + "%' OR bc.blood_content_desc LIKE '%" + filter + "%' OR bc.blood_content_mobile LIKE '%" + filter + "%' OR bc.blood_content_address LIKE '%" + filter + "%' OR bc.blood_type LIKE '%" + filter + "%' ) ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM blood_content bc "
//                                + "LEFT JOIN member AS m ON m.id = bc.boold_req_from "
//                                + "WHERE bc.published = 1 AND bc.boold_req_from ='" + memId + "'  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY bc.id_blood_content DESC";
                        pContentCountSQL = "SELECT  count(*) FROM blood_content bc "
                                + "LEFT JOIN member AS m ON m.id = bc.boold_req_from "
                                + "WHERE bc.id_blood_content  in (  "
                                + "SELECT bc.id_blood_content FROM blood_content bc   "
                                + "WHERE bc.boold_req_from = '" + memId + "' "
                                + "UNION "
                                + "SELECT bd.blood_request_id FROM blood_donate bd  "
                                + "WHERE bd.blood_donar_id = '" + memId + "' )";

                        String numrow1 = dbsession.createSQLQuery(pContentCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pContentCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

//                        pContentSQL = "SELECT  bc.id_blood_content, bc.blood_content_name, bc.blood_content_desc,bc.blood_content_mobile,"
//                                + "bc.blood_content_address,bc.blood_type,bc.boold_req_from,bc.blood_req_date, "
//                                + "m.id,m.member_id,m.member_name  contentOwnerName, m.picture_name contentOwnerPic "
//                                + "FROM blood_content bc "
//                                + "LEFT JOIN member AS m ON m.id = bc.boold_req_from "
//                                + "WHERE bc.published = 1  AND bc.boold_req_from ='" + memId + "'  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY bc.id_blood_content DESC "
//                                + "LIMIT " + offset + " , " + rowsperpage + " ";
                        pContentSQL = "SELECT  bc.id_blood_content, bc.blood_content_name, bc.blood_content_desc,bc.blood_content_mobile,"
                                + "bc.blood_content_address,bc.blood_type,bc.boold_req_from,bc.blood_req_date, "
                                + "m.id,m.member_id,m.member_name  contentOwnerName, m.picture_name contentOwnerPic, "
                                + "bc.published,mt.member_type_id    "
                                + "FROM blood_content bc "
                                + "LEFT JOIN member AS m ON m.id = bc.boold_req_from "
                                + "LEFT JOIN member_type AS mt ON m.id = mt.member_id "
                                + "WHERE bc.id_blood_content  in ( "
                                + "SELECT bc.id_blood_content FROM blood_content bc  "
                                + "WHERE bc.boold_req_from = '" + memId + "' "
                                + "UNION "
                                + "SELECT bd.blood_request_id FROM blood_donate bd  "
                                + "WHERE bd.blood_donar_id = '" + memId + "')   "
                                + "ORDER BY bc.id_blood_content DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        pContentSQLQry = dbsession.createSQLQuery(pContentSQL);

                        for (Iterator pContentItr = pContentSQLQry.list().iterator(); pContentItr.hasNext();) {

                            pContentObj = (Object[]) pContentItr.next();
                            bloodContentId = pContentObj[0].toString();
                            bloodContentName = pContentObj[1] == null ? "" : pContentObj[1].toString();
                            bloodContentDesc = pContentObj[2] == null ? "" : pContentObj[2].toString();
                            bloodContentMobile = pContentObj[3] == null ? "" : pContentObj[3].toString();

                            bloodContentAddress = pContentObj[4] == null ? "" : pContentObj[4].toString();
                            bloodType = pContentObj[5] == null ? "" : pContentObj[5].toString();
                            bloodReqFromId = pContentObj[6] == null ? "" : pContentObj[6].toString();
                            bloodReqDate = pContentObj[7] == null ? "" : pContentObj[7].toString();
                            memberXId = pContentObj[8] == null ? "" : pContentObj[8].toString();
                            memberIEBId = pContentObj[9] == null ? "" : pContentObj[9].toString();
                            memberName = pContentObj[10] == null ? "" : pContentObj[10].toString();

                            memberPictureName = pContentObj[11] == null ? "" : pContentObj[11].toString();
                            memberPictureLink = GlobalVariable.imageMemberDirLink + memberPictureName;

                            bloodReqPublished = pContentObj[12] == null ? "" : pContentObj[12].toString();

                            memberIEBIdFirstChar = pContentObj[13] == null ? "" : pContentObj[13].toString();
                            if (!memberIEBIdFirstChar.equals("")) {
                                if (memberIEBIdFirstChar.equals("1")) {
                                    memberType = "Member";
                                }
                                if (memberIEBIdFirstChar.equals("2")) {
                                    memberType = "Doctor";
                                }
                            } else {
                                memberType = "";
                            }

                            if (bloodReqPublished.equalsIgnoreCase("3")) {
                                bloodDonationStatus = "YES";

                                donarSQL = "SELECT bd.id_blood_donate, bd.blood_donar_id, "
                                        + "m.member_id donarIEBId,m.member_name  donarName, m.picture_name donarPicture "
                                        + "FROM blood_donate bd "
                                        + "LEFT JOIN member AS m ON m.id = bd.blood_donar_id "
                                        + "WHERE bd.blood_request_id ='" + bloodContentId + "'";

                                donarSQLQry = dbsession.createSQLQuery(donarSQL);

                                for (Iterator donarItr = donarSQLQry.list().iterator(); donarItr.hasNext();) {

                                    donarObj = (Object[]) donarItr.next();
                                    donateId = donarObj[0].toString();
                                    donarId = donarObj[1] == null ? "" : donarObj[1].toString();
                                    donarIEBId = donarObj[2] == null ? "" : donarObj[2].toString();
                                    donarName = donarObj[3] == null ? "" : donarObj[3].toString();

                                    donarPicture = donarObj[4] == null ? "" : donarObj[4].toString();
                                    donarPictureLink = GlobalVariable.imageMemberDirLink + donarPicture;

                                    bloodDonarInfoObj = new JSONObject();
                                    bloodDonarInfoObj.put("DonateId", donateId);
                                    bloodDonarInfoObj.put("DonarId", donarId);
                                    bloodDonarInfoObj.put("DonarIEBId", donarIEBId);
                                    bloodDonarInfoObj.put("DonarName", donarName);
                                    bloodDonarInfoObj.put("DonarPicture", donarPictureLink);

                                    bloodDonarInfoArr.add(bloodDonarInfoObj);

                                }

                            } else {
                                bloodDonationStatus = "NO";
                                //  bloodDonarInfoObj = new JSONObject();

                            }

                            if (bloodReqFromId.equals(Integer.toString(memId))) {
                                userType = "Requester";
                            } else {
                                userType = "Donor";
                            }

                            postContentObj = new JSONObject();
                            postContentObj.put("BloodRequestId", bloodContentId);
                            postContentObj.put("BloodRequestName", bloodContentName);
                            postContentObj.put("BloodRequestDetails", bloodContentDesc);
                            postContentObj.put("BloodRequestMobileNumber", bloodContentMobile);
                            postContentObj.put("BloodRequestAddress", bloodContentAddress);
                            postContentObj.put("BloodType", bloodType);
                            postContentObj.put("BloodRequestDate", bloodReqDate);;
                            // 'bloodDonationStatus' => $bloodDonationStatus,

                            postContentObj.put("BloodDonationStatus", bloodDonationStatus);
                            postContentObj.put("BloodDonarInfo", bloodDonarInfoArr);
                            postContentObj.put("BloodUserType", userType);

                            postContentObj.put("MemberId", memberXId);
                            postContentObj.put("MemberIEBId", memberIEBId);
                            postContentObj.put("MemberName", memberName);
                            postContentObj.put("MemberTypeId", memberIEBIdFirstChar);
                            postContentObj.put("MemberType", memberType);
                            postContentObj.put("MemberPicture", memberPictureLink);
                            postContentObjArr.add(postContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", postContentObjArr);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        // finally {
        //   dbtrx.commit();
        //  }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
