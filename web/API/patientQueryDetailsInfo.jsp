<%-- 
    Document   : patientQueryDetailsInfo
    Created on : MAY 05, 2020, 12:20:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientQueryDetailsInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String qryId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("qryId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        qryId = request.getParameter("qryId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: patientQueryDetailsInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject doctorInfoObj = new JSONObject();
    JSONObject doctorInfoResponseObj = new JSONObject();
    JSONArray doctorInfoObjArray = new JSONArray();

    JSONObject queryReplyInfoObj = new JSONObject();
    JSONObject queryReplyInfoResponseObj = new JSONObject();
    JSONArray queryReplyInfoObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String pQuerySQL = null;
    String pQueryCountSQL = null;
    Query pQuerySQLQry = null;
    Object[] pQueryObj = null;

    String pQueryOwnerId = "";
    String pQueryOwnerName = "";
    String pQueryOwnerPic = "";
    String pQueryOwnerPicLink = "";

    String pQueryId = "";
    String pQueryParent = "";
    String pQueryPatientId = "";
    String pQueryProblemCategory = "";
    String pQueryDesc = "";
    String pQuerySufferingDate = "";
    String pQueryDate = "";
    String pQueryStatus = "";
    String pQueryReplyStatus = "";
    String pQueryCommentType = "";
    String pQueryCommenterId = "";

    String pQueryProblemCategoryId = "";
    String pQueryProblemCategoryName = "";

    String fever = "";
    String cough = "";
    String tiredness = "";
    String nasalcongestion = "";
    String headache = "";

    String feverText = "";
    String coughText = "";
    String tirednessText = "";
    String nasalcongestionText = "";
    String headacheText = "";

    String patientName = "";
    String patientEmail = "";
    String patientPhone = "";
    String patientDOB = "";
    String patientGender = "";

    String assignDoctor = "";

    String doctorSQL = null;
    Query doctorSQLQry = null;
    Object[] doctorObj = null;

    String doctorName = "";
    String doctorPic = "";
    String doctorPicLink = "";
    String doctorMobile = "";
    String doctorPhone1 = "";
    String doctorPhone2 = "";
    String doctorEmail = "";

    String doctorRegNo = "";
    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorMedicalCollege = "";

    String pQueryReplySQL = null;
    String pQueryReplyCountSQL = null;
    Query pQueryReplySQLQry = null;
    Object[] pQueryReplyObj = null;

    String pQueryReplyId = "";
    String pQueryReplyDesc = "";
    String pQueryReplyDate = "";
    String pQueryReplyCommentType = "";
    String pQueryReplyCommentTypeText = "";
    String pQueryReplyCommenterId = "";

    String pQueryReplyOwnerId = "";
    String pQueryReplyOwnerName = "";
    String pQueryReplyOwnerPic = "";
    String pQueryReplyOwnerPicLink = "";

    String pQueryAttachment = "";
    String pQueryAttachmentLink = "";

    String attachment = "";
    String attachmentLink = "";

    String pQueryDiseaseCategoryId = "";
    String pQueryDiseaseCategoryShortName = "";
    String pQueryDiseaseCategoryName = "";

    String appointmentSQL = "";
    Query appointmentSQLQry = null;
    Object[] appointmentObj = null;

    String appointmentId = "";
    String appointmentDate = "";
    String appointmentDateTime = "";
    String appointmentStatus = "";
    String appointmentTimeSlotShortName = "";
    String appointmentTimeSlotName = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

//                        pContentSQL = "SELECT  en.id_event,  en.event_category,en.event_title,"
//                                + "en.event_short_desc,en.event_desc,en.event_date,"
//                                + "en.event_duration,en.event_venue,en.feature_image "
//                                + "FROM event_info en "
//                                + "WHERE en.id_event = '" + evtId + "' ";
                        pQuerySQL = "SELECT  pq.query_id, pq.parent, pq.patient_id,pq.problem_category,"
                                + "pq.query_desc,pq.suffering_date,pq.query_date,pq.status,pq.reply_status,"
                                + "pq.comment_type,pq.commenter_id,"
                                + "pqc.fever, pqc.cough, pqc.tiredness, pqc.nasalcongestion, pqc.headache,  "
                                + "pi.patient_name,pi.patient_email, pi.patient_phone, pi.patient_dob, pi.patient_gender,"
                                + "pq.assign_doctor,   "
                                + "m.id queryOwnerId,m.member_name  queryOwnerName, m.picture_name queryOwnerPic,"
                                + "pq.attachment,pq.disease_category_id,dcat.short_name,dcat.name  "
                                + "FROM patient_query pq "
                                + "LEFT JOIN patient_query_covid AS pqc ON pq.query_id = pqc.query_id "
                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
                                + "LEFT JOIN doctor_category_name AS dcat ON dcat.id = pq.disease_category_id "
                                + "WHERE pq.query_id = '" + qryId + "' ";

                        pQuerySQLQry = dbsession.createSQLQuery(pQuerySQL);

                        for (Iterator pContentItr = pQuerySQLQry.list().iterator(); pContentItr.hasNext();) {

                            pQueryObj = (Object[]) pContentItr.next();
                            pQueryId = pQueryObj[0].toString();
                            pQueryParent = pQueryObj[1] == null ? "" : pQueryObj[1].toString();
                            pQueryPatientId = pQueryObj[2] == null ? "" : pQueryObj[2].toString();
                            pQueryProblemCategory = pQueryObj[3] == null ? "" : pQueryObj[3].toString();
                            pQueryDesc = pQueryObj[4] == null ? "" : pQueryObj[4].toString();
                            pQuerySufferingDate = pQueryObj[5] == null ? "" : pQueryObj[5].toString();
                            pQueryDate = pQueryObj[6] == null ? "" : pQueryObj[6].toString();
                            pQueryStatus = pQueryObj[7] == null ? "" : pQueryObj[7].toString();

                            pQueryReplyStatus = pQueryObj[8] == null ? "" : pQueryObj[8].toString();
                            pQueryCommentType = pQueryObj[9] == null ? "" : pQueryObj[9].toString();
                            pQueryCommenterId = pQueryObj[10] == null ? "" : pQueryObj[10].toString();

                            fever = pQueryObj[11] == null ? "" : pQueryObj[11].toString();
                            if (fever.equals("1")) {
                                feverText = "Yes";
                            } else {
                                feverText = "No";
                            }
                            cough = pQueryObj[12] == null ? "" : pQueryObj[12].toString();
                            if (cough.equals("1")) {
                                coughText = "Yes";
                            } else {
                                coughText = "No";
                            }
                            tiredness = pQueryObj[13] == null ? "" : pQueryObj[13].toString();
                            if (tiredness.equals("1")) {
                                tirednessText = "Yes";
                            } else {
                                tirednessText = "No";
                            }
                            nasalcongestion = pQueryObj[14] == null ? "" : pQueryObj[14].toString();
                            if (nasalcongestion.equals("1")) {
                                nasalcongestionText = "Yes";
                            } else {
                                nasalcongestionText = "No";
                            }
                            headache = pQueryObj[15] == null ? "" : pQueryObj[15].toString();
                            if (headache.equals("1")) {
                                headacheText = "Yes";
                            } else {
                                headacheText = "No";
                            }

                            patientName = pQueryObj[16] == null ? "" : pQueryObj[16].toString();
                            patientEmail = pQueryObj[17] == null ? "" : pQueryObj[17].toString();
                            patientPhone = pQueryObj[18] == null ? "" : pQueryObj[18].toString();
                            patientDOB = pQueryObj[19] == null ? "" : pQueryObj[19].toString();
                            patientGender = pQueryObj[20] == null ? "" : pQueryObj[20].toString();

                            assignDoctor = pQueryObj[21] == null ? "" : pQueryObj[21].toString();

                           // if (pQueryStatus.equals("1")) {

                                doctorSQL = "SELECT m.id, m.member_name,m.picture_name, "
                                        + "m.mobile, m.phone1, m.phone2, m.email_id,"
                                        + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                                        + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege  "
                                        + "FROM  member m ,member_type mt,member_additional_info ai "
                                        + "WHERE m.status = 1 AND m.id = '" + assignDoctor + "'"
                                        + "AND mt.member_type_id = 2 "
                                        + "AND m.id = mt.member_id "
                                        + "AND m.id = ai.member_id ";

                                doctorSQLQry = dbsession.createSQLQuery(doctorSQL);
                                if (!doctorSQLQry.list().isEmpty()) {
                                    for (Iterator doctorItr = doctorSQLQry.list().iterator(); doctorItr.hasNext();) {

                                        doctorObj = (Object[]) doctorItr.next();

                                        doctorName = doctorObj[1] == null ? "" : doctorObj[1].toString();
                                        doctorPic = doctorObj[2] == null ? "" : doctorObj[2].toString();

                                        doctorPicLink = GlobalVariable.imageMemberDirLink + doctorPic;

                                        doctorMobile = doctorObj[3] == null ? "" : doctorObj[3].toString();
                                        doctorPhone1 = doctorObj[4] == null ? "" : doctorObj[4].toString();
                                        doctorPhone2 = doctorObj[5] == null ? "" : doctorObj[5].toString();
                                        doctorEmail = doctorObj[6] == null ? "" : doctorObj[6].toString();

                                        doctorDegree0 = doctorObj[7] == null ? "" : doctorObj[7].toString();
                                        doctorDegree1 = doctorObj[8] == null ? "" : doctorObj[8].toString();
                                        doctorDegree2 = doctorObj[9] == null ? "" : doctorObj[9].toString();
                                        doctorDegree3 = doctorObj[10] == null ? "" : doctorObj[10].toString();
                                        doctorDegree4 = doctorObj[11] == null ? "" : doctorObj[11].toString();
                                        doctorRegNo = doctorObj[12] == null ? "" : doctorObj[12].toString();
                                        doctorMedicalCollege = doctorObj[13] == null ? "" : doctorObj[13].toString();

                                    }

                                    doctorInfoResponseObj = new JSONObject();
                                    doctorInfoResponseObj.put("doctorName", doctorName);
                                    doctorInfoResponseObj.put("doctorPicture", doctorPicLink);
                                    doctorInfoResponseObj.put("doctorMobile", doctorMobile);
                                    doctorInfoResponseObj.put("doctorPhone1", doctorPhone1);
                                    doctorInfoResponseObj.put("doctorPhone2", doctorPhone2);
                                    doctorInfoResponseObj.put("doctorEmail", doctorEmail);

                                    doctorInfoResponseObj.put("doctorDegree0", doctorDegree0);
                                    doctorInfoResponseObj.put("doctorDegree1", doctorDegree1);
                                    doctorInfoResponseObj.put("doctorDegree2", doctorDegree2);
                                    doctorInfoResponseObj.put("doctorDegree3", doctorDegree3);
                                    doctorInfoResponseObj.put("doctorDegree4", doctorDegree4);

                                    doctorInfoResponseObj.put("doctorRegNo", doctorRegNo);
                                    doctorInfoResponseObj.put("doctorMedicalCollege", doctorMedicalCollege);

                                    doctorInfoObjArray.add(doctorInfoResponseObj);

                                }

                                pQueryReplySQL = "SELECT  pq.query_id, pq.query_desc,pq.query_date, "
                                        + "pq.comment_type,pq.commenter_id,pq.attachment,  "
                                        + "m.id pQueryReplyOwnerId,m.member_name  pQueryReplyOwnerName, m.picture_name pQueryReplyOwnerPic  "
                                        + "FROM patient_query pq "
                                        + "LEFT JOIN member AS m ON m.id = pq.commenter_id "
                                        + "WHERE pq.parent = '" + qryId + "'  "
                                        + "LIMIT 0 , 10";

                                System.out.println("pQueryReplySQL :" + pQueryReplySQL);

                                pQueryReplySQLQry = dbsession.createSQLQuery(pQueryReplySQL);

                                for (Iterator pQueryReplyItr = pQueryReplySQLQry.list().iterator(); pQueryReplyItr.hasNext();) {

                                    pQueryReplyObj = (Object[]) pQueryReplyItr.next();

                                    pQueryReplyId = pQueryReplyObj[0].toString();
                                    pQueryReplyDesc = pQueryReplyObj[1] == null ? "" : pQueryReplyObj[1].toString();
                                    pQueryReplyDate = pQueryReplyObj[2] == null ? "" : pQueryReplyObj[2].toString();
                                    pQueryReplyCommentType = pQueryReplyObj[3] == null ? "" : pQueryReplyObj[3].toString();

                                    if (pQueryReplyCommentType.equals("1")) {
                                        pQueryReplyCommentTypeText = "Doctor";
                                    } else {
                                        pQueryReplyCommentTypeText = "Patient";
                                    }

                                    pQueryReplyCommenterId = pQueryReplyObj[4] == null ? "" : pQueryReplyObj[4].toString();

                                    attachment = pQueryReplyObj[5] == null ? "" : pQueryReplyObj[5].toString();
                                    attachmentLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + attachment;

                                    pQueryReplyOwnerId = pQueryReplyObj[6] == null ? "" : pQueryReplyObj[6].toString();
                                    pQueryReplyOwnerName = pQueryReplyObj[7] == null ? "" : pQueryReplyObj[7].toString();
                                    pQueryReplyOwnerPic = pQueryReplyObj[8] == null ? "" : pQueryReplyObj[8].toString();

                                    pQueryReplyOwnerPicLink = GlobalVariable.imageMemberDirLink + pQueryReplyOwnerPic;

                                    queryReplyInfoResponseObj = new JSONObject();

                                    queryReplyInfoResponseObj.put("pQueryReplyId", pQueryReplyId);

                                    queryReplyInfoResponseObj.put("pQueryReplyDesc", pQueryReplyDesc);
                                    queryReplyInfoResponseObj.put("pQueryReplyDate", pQueryReplyDate);
                                    queryReplyInfoResponseObj.put("pQueryReplyCommentType", pQueryReplyCommentType);
                                    queryReplyInfoResponseObj.put("pQueryReplyCommentTypeText", pQueryReplyCommentTypeText);

                                    queryReplyInfoResponseObj.put("pQueryReplyCommenterId", pQueryReplyCommenterId);

                                    queryReplyInfoResponseObj.put("attachment", attachment);
                                    queryReplyInfoResponseObj.put("attachmentLink", attachmentLink);

                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerId", pQueryReplyOwnerId);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerName", pQueryReplyOwnerName);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerPic", pQueryReplyOwnerPic);
                                    queryReplyInfoResponseObj.put("pQueryReplyOwnerPicLink", pQueryReplyOwnerPicLink);

                                    queryReplyInfoObjArray.add(queryReplyInfoResponseObj);

                                }

                         //   }//end status condition

                            pQueryOwnerId = pQueryObj[22] == null ? "" : pQueryObj[22].toString();
                            pQueryOwnerName = pQueryObj[23] == null ? "" : pQueryObj[23].toString();

                            pQueryOwnerPic = pQueryObj[24] == null ? "" : pQueryObj[24].toString();
                            pQueryOwnerPicLink = GlobalVariable.imageMemberDirLink + pQueryOwnerPic;

                            pQueryAttachment = pQueryObj[25] == null ? "" : pQueryObj[25].toString();
                            pQueryAttachmentLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + pQueryAttachment;

                            pQueryDiseaseCategoryId = pQueryObj[26] == null ? "" : pQueryObj[26].toString();
                            pQueryDiseaseCategoryShortName = pQueryObj[27] == null ? "" : pQueryObj[27].toString();
                            pQueryDiseaseCategoryName = pQueryObj[28] == null ? "" : pQueryObj[28].toString();

                            appointmentSQL = "SELECT pa.id,pa.status ,pa.appointment_time, "
                                    + "ts.short_name,ts.name,ts.time_text,"
                                    + "dts.timeslot_date "
                                    + "FROM  patient_appointment pa ,doctor_timeslot dts,doctor_timeslot_name ts "
                                    + "WHERE pa.pquery_id = '" + pQueryId + "'"
                                    + "AND pa.doctor_timeslot_id = dts.id "
                                    + "AND dts.timeslot_id = ts.id";

                            System.out.println("appointmentSQL :: " + appointmentSQL);

                            appointmentSQLQry = dbsession.createSQLQuery(appointmentSQL);
                            if (!appointmentSQLQry.list().isEmpty()) {
                                for (Iterator appointmentItr = appointmentSQLQry.list().iterator(); appointmentItr.hasNext();) {

                                    appointmentObj = (Object[]) appointmentItr.next();

                                    appointmentId = appointmentObj[0].toString();
                                    appointmentStatus = appointmentObj[1] == null ? "" : appointmentObj[1].toString();
                                    appointmentDateTime = appointmentObj[2] == null ? "" : appointmentObj[2].toString();

                                    appointmentTimeSlotShortName = appointmentObj[3] == null ? "" : appointmentObj[3].toString();
                                    appointmentTimeSlotName = appointmentObj[4] == null ? "" : appointmentObj[4].toString();
                                    //appointmentDate = appointmentObj[1] == null ? "" : appointmentObj[1].toString();
                                }
                            } else {
                                appointmentId = "";
                                appointmentStatus = "";
                                appointmentDateTime = "";

                                appointmentTimeSlotShortName = "";
                                appointmentTimeSlotName = "";

                            }

                            postContentObj = new JSONObject();
                            postContentObj.put("pQueryId", pQueryId);

                            postContentObj.put("appointmentId", appointmentId);
                            //   postContentObj.put("appointmentDate", appointmentDate);
                            postContentObj.put("appointmentDateTime", appointmentDateTime);
                            postContentObj.put("appointmentTimeSlotShortName", appointmentTimeSlotShortName);
                            postContentObj.put("appointmentTimeSlotName", appointmentTimeSlotName);
                            postContentObj.put("pQueryDiseaseCategoryId", pQueryDiseaseCategoryId);
                            postContentObj.put("pQueryDiseaseCategoryShortName", pQueryDiseaseCategoryShortName);
                            postContentObj.put("pQueryDiseaseCategoryName", pQueryDiseaseCategoryName);

                            postContentObj.put("DoctorInfo", doctorInfoObjArray);

                            postContentObj.put("QueryReplyInfo", queryReplyInfoObjArray);

                            postContentObj.put("patientId", pQueryPatientId);
                            postContentObj.put("patientName", patientName);
                            postContentObj.put("patientEmail", patientEmail);
                            postContentObj.put("patientPhone", patientPhone);
                            postContentObj.put("patientDOB", patientDOB);
                            postContentObj.put("patientGender", patientGender);

                            postContentObj.put("pQueryParent", pQueryParent);
                            postContentObj.put("pQueryPatientId", pQueryPatientId);

                            postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategory);
                            postContentObj.put("pQueryProblemCategoryName", pQueryProblemCategoryName);

                            postContentObj.put("pQueryDesc", pQueryDesc);
                            postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                            postContentObj.put("pQueryDate", pQueryDate);
                            postContentObj.put("pQueryStatus", pQueryStatus);
                            postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                            postContentObj.put("pQueryCommentType", pQueryCommentType);
                            postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                            postContentObj.put("fever", fever);
                            postContentObj.put("feverText", feverText);

                            postContentObj.put("cough", cough);
                            postContentObj.put("coughText", coughText);

                            postContentObj.put("tiredness", tiredness);
                            postContentObj.put("tirednessText", tirednessText);

                            postContentObj.put("nasalcongestion", nasalcongestion);
                            postContentObj.put("nasalcongestionText", nasalcongestionText);

                            postContentObj.put("headache", headache);
                            postContentObj.put("headacheText", headacheText);

                            postContentObj.put("pQueryAttachment", pQueryAttachment);
                            postContentObj.put("pQueryAttachmentLink", pQueryAttachmentLink);

                            //   postContentObj.put("queryOwnerType", mMemberTypeName);
                            postContentObj.put("pQueryOwnerId", pQueryOwnerId);
                            postContentObj.put("pQueryOwnerName", pQueryOwnerName);
                            postContentObj.put("pQueryOwnerPic", pQueryOwnerPic);
                            postContentObj.put("pQueryOwnerPicLink", pQueryOwnerPicLink);
                            postContentObjArr.add(postContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", postContentObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
