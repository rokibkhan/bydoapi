<%-- 
    Document   : getAboutPageMemberInfoInfo
    Created on : APR 29, 2020, 11:19:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getAboutPageMemberInfoInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query adviseBySQL = null;
    Object adviseByObj[] = null;
    String adviseById = "";
    String adviseByName = "";
    String adviseByDeignation = "";
    String adviseByPicture = "";
    String adviseByPictureLink = "";
    String adviseByShowingOrder = "";
    JSONArray adviseByResponseObjArr = new JSONArray();
    JSONObject adviseByResponseObj = new JSONObject();

    Query superviseBySQL = null;
    Object superviseByObj[] = null;
    String superviseById = "";
    String superviseByName = "";
    String superviseByDeignation = "";
    String superviseByPicture = "";
    String superviseByPictureLink = "";
    String superviseByShowingOrder = "";
    JSONArray superviseByResponseObjArr = new JSONArray();
    JSONObject superviseByResponseObj = new JSONObject();

    Query directedBySQL = null;
    Object directedByObj[] = null;
    String directedById = "";
    String directedByName = "";
    String directedByDeignation = "";
    String directedByPicture = "";
    String directedByPictureLink = "";
    String directedByShowingOrder = "";
    JSONArray directedByResponseObjArr = new JSONArray();
    JSONObject directedByResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    JSONArray moduleResponseObjArr = new JSONArray();

    JSONArray moduleDataObjArr = new JSONArray();
    JSONObject moduleDataObj = new JSONObject();

    JSONArray noticeModuleResponseObjArr = new JSONArray();
    JSONObject noticeModuleResponseObj = new JSONObject();

    JSONArray bloodModuleResponseObjArr = new JSONArray();
    JSONObject bloodModuleResponseObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("JoyBangla :: API :: getAboutPageMemberInfoInfo rec :: " + rec);
    if (rec == 1) {

        try {

            adviseBySQL = dbsession.createSQLQuery("select m.id,m.member_name,m.picture_name,"
                    + "m.posting,md.designation_id,d.designation_name,co.custom_order "
                    + "FROM member m ,member_designation md,member_custom_order co,designation d "
                    + "WHERE m.id=md.member_id "
                    + "AND m.id=co.member_id "
                    + "AND md.designation_id=d.id "
                    + "AND m.id in (1322,1311) "
                    + "ORDER BY co.custom_order ASC");

            if (!adviseBySQL.list().isEmpty()) {
                for (Iterator adviseByIt = adviseBySQL.list().iterator(); adviseByIt.hasNext();) {

                    adviseByObj = (Object[]) adviseByIt.next();
                    adviseById = adviseByObj[0].toString();
                    adviseByName = adviseByObj[1].toString();
                    adviseByPicture = adviseByObj[2].toString() == null ? "" : adviseByObj[2].toString();
                    adviseByPictureLink = GlobalVariable.imageMemberDirLink + adviseByPicture;

                    //    adviseByDeignation = adviseByObj[5].toString() == null ? "" : adviseByObj[5].toString();
                    if (adviseById.equals("1322")) {
                        adviseByShowingOrder = "1";
                        adviseByDeignation = "Chairman,Science and Technology Subcommittee,Bangladesh Awami League";
                    }
                    if (adviseById.equals("1311")) {
                        adviseByShowingOrder = "2";
                        adviseByDeignation = "Secretary,Science and Technology Affair's,Bangladesh Awami League";
                    }

                    adviseByResponseObj = new JSONObject();
                    adviseByResponseObj.put("AdviseById", adviseById);
                    adviseByResponseObj.put("AdviseByName", adviseByName);
                    adviseByResponseObj.put("AdviseByDeignation", adviseByDeignation);
                    adviseByResponseObj.put("AdviseByPicture", adviseByPictureLink);
                    adviseByResponseObj.put("AdviseByShowingOrder", adviseByShowingOrder);
                    adviseByResponseObjArr.add(adviseByResponseObj);
                }
            }

            superviseBySQL = dbsession.createSQLQuery("select m.id,m.member_name,m.picture_name,"
                    + "m.posting,md.designation_id,d.designation_name,co.custom_order "
                    + "FROM member m ,member_designation md,member_custom_order co,designation d "
                    + "WHERE m.id=md.member_id "
                    + "AND m.id=co.member_id "
                    + "AND md.designation_id=d.id "
                    + "AND m.id in (11,9,1327) "
                    + "ORDER BY co.custom_order ASC");

            if (!superviseBySQL.list().isEmpty()) {
                for (Iterator superviseByIt = superviseBySQL.list().iterator(); superviseByIt.hasNext();) {

                    superviseByObj = (Object[]) superviseByIt.next();
                    superviseById = superviseByObj[0].toString();
                    superviseByName = superviseByObj[1].toString();
                    superviseByPicture = superviseByObj[2].toString() == null ? "" : superviseByObj[2].toString();
                    superviseByPictureLink = GlobalVariable.imageMemberDirLink + superviseByPicture;

                    //    adviseByDeignation = adviseByObj[5].toString() == null ? "" : adviseByObj[5].toString();
                    if (superviseById.equals("11")) {
                        superviseByShowingOrder = "1";
                        superviseByDeignation = "Vice Chancellor,Canadian University of Bangladesh and Chairman,Computer Engineering Division,IEB";
                    }
                    if (superviseById.equals("9")) {
                        superviseByShowingOrder = "2";
                        superviseByDeignation = "Secretary,SComputer Engineering Division,IEB";
                    }
                    if (superviseById.equals("1327")) {
                        superviseByShowingOrder = "3";
                        superviseByDeignation = "Coordinator, CRI & Member, Science and Technology Affair's Sub Committee";
                    }

                    superviseByResponseObj = new JSONObject();
                    superviseByResponseObj.put("SuperviseById", superviseById);
                    superviseByResponseObj.put("SuperviseByName", superviseByName);
                    superviseByResponseObj.put("SuperviseByDeignation", superviseByDeignation);
                    superviseByResponseObj.put("SuperviseByPicture", superviseByPictureLink);
                    superviseByResponseObj.put("SuperviseByShowingOrder", superviseByShowingOrder);
                    superviseByResponseObjArr.add(superviseByResponseObj);
                }
            }

            directedBySQL = dbsession.createSQLQuery("select m.id,m.member_name,m.picture_name,"
                    + "m.posting,md.designation_id,d.designation_name,co.custom_order "
                    + "FROM member m ,member_designation md,member_custom_order co,designation d "
                    + "WHERE m.id=md.member_id "
                    + "AND m.id=co.member_id "
                    + "AND md.designation_id=d.id "
                    + "AND m.id in (20028) "
                    + "ORDER BY co.custom_order ASC");

            if (!directedBySQL.list().isEmpty()) {
                for (Iterator directedByIt = directedBySQL.list().iterator(); directedByIt.hasNext();) {

                    directedByObj = (Object[]) directedByIt.next();
                    directedById = directedByObj[0].toString();
                    directedByName = directedByObj[1].toString();
                    directedByPicture = directedByObj[2].toString() == null ? "" : directedByObj[2].toString();
                    directedByPictureLink = GlobalVariable.imageMemberDirLink + directedByPicture;

                    //    adviseByDeignation = adviseByObj[5].toString() == null ? "" : adviseByObj[5].toString();
                    if (directedById.equals("20028")) {
                        directedByShowingOrder = "1";
                        directedByDeignation = "Elected Member,Computer Engineering Division,IEB";
                    }
                    //  if (directedById.equals("1311")) {
                    //     directedByDeignation = "Secretary,Science and Technology Affair's,Bangladesh Awami League";
                    //   }

                    directedByResponseObj = new JSONObject();
                    directedByResponseObj.put("DirectedById", directedById);
                    directedByResponseObj.put("DirectedByName", directedByName);
                    directedByResponseObj.put("DirectedByDeignation", directedByDeignation);
                    directedByResponseObj.put("DirectedByPicture", directedByPictureLink);
                    directedByResponseObj.put("DirectedByShowingOrder", directedByShowingOrder);
                    directedByResponseObjArr.add(directedByResponseObj);
                }
            }

            moduleDataObj.put("AdviseBy", adviseByResponseObjArr);
            moduleDataObj.put("SuperviseBy", superviseByResponseObjArr);
            moduleDataObj.put("DirectedBy", directedByResponseObjArr);

            moduleDataObjArr.add(moduleDataObj);

            responseDataObj = new JSONObject();
            responseDataObj.put("ResponseCode", "1");
            responseDataObj.put("ResponseText", "Found");
            responseDataObj.put("ResponseData", moduleDataObjArr);

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", bannerObjArray);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>