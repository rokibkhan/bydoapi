<%-- 
    Document   : signup
    Created on : MAY 08, 2020, 8:26:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    RandomString randNumber = new RandomString();

    SendSMS sendsms = new SendSMS();
    SendEmail sendemail = new SendEmail();

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    Logger logger = Logger.getLogger("signup_jsp.class");

    String key = "";
    String fullName = "";
    String mobileNumber = "";
    String districtId = "";
    String thanaId = "";

    String memberId = "";
    String givenPassword = "";
    String postContentId = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("fullName") && request.getParameterMap().containsKey("mobileNumber") && request.getParameterMap().containsKey("districtId") && request.getParameterMap().containsKey("thanaId")) {
        key = request.getParameter("key").trim();

        fullName = request.getParameter("fullName").trim();
        mobileNumber = request.getParameter("mobileNumber").trim();
        districtId = request.getParameter("districtId").trim();
        thanaId = request.getParameter("thanaId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // String firstLetter = userInput.substring(0,1); //takes first letter
//String restOfString = userInput.substring(1); //takes rest of sentence
//firstLetter = firstLetter.toUpperCase(); //make sure to set the string, the methods return strings, they don't change the string itself
//restOfString = restOfString.toLowerCase();
//String merged = firstletter + restOfString;
    String firstLetter = mobileNumber.substring(0, 1);
    String adjustLettter1 = "+88";
    String adjustLettter2 = "88";
    String adjustLettter3 = "+";

    String mobileNumber1 = "";
    String mobileNumber2 = "";

    if (firstLetter.equals("0")) {
        mobileNumber1 = adjustLettter1 + mobileNumber;
        mobileNumber2 = adjustLettter2 + mobileNumber;

        System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 0 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("8")) {
        mobileNumber1 = adjustLettter3 + mobileNumber;
        mobileNumber2 = adjustLettter3 + mobileNumber;
        System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 8 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("+")) {
        mobileNumber1 = mobileNumber;
        mobileNumber2 = mobileNumber;

        System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: + :: mobileNumber2 ::" + mobileNumber2);

    }

    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("signup API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String ratingDate = "";
    Query postContentShareRatingAddSQL = null;

    Query postContentRatingHistoryAddSQL = null;

    Query memberAddSQL = null;

    //  String ratingPointHostorySQL = "";
    //   String ratingPointHostoryNumRows = "";
    //  java.util.Date ed = new java.util.Date();
    // java.util.Date td = new java.util.Date();
    //   td = new SimpleDateFormat("yyyy-mm-dd").parse("2099-12-31");
    String ed = dateFormatYmd.format(dateToday);
    String td = "2099-12-31";

    String memberCountryCode = "BD";
    String tempPassStatus = "2"; //2->for switch force password change ;1->already change password
    String memberTypeId = "1"; //1->member;2->doctor;
    String memberCustomOrder = "999999";
    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber + "'");

            if (qMember.list().isEmpty()) {

                String idS = getId.getID(43);
                int regMemberId = Integer.parseInt(idS);

                //String pinCode = randNumber.randomString(4,2);
                String member_temp_pass_phone = randNumber.randomString(4, 2);
                String member_temp_pass_email = randNumber.randomString(6, 2);

                String tempPassEnc = encryption.getEncrypt(member_temp_pass_phone);

                logger.info("API :: SignUp random pass ::" + encryption.getEncrypt(member_temp_pass_phone));
                //  if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                System.out.println("API :: SignUp random pass ::" + encryption.getEncrypt(member_temp_pass_phone));

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                String districtSQL = "SELECT  DISTRICT_NAME FROM district WHERE ID = '" + districtId + "'";
                String districtName = dbsession.createSQLQuery(districtSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(districtSQL).uniqueResult().toString();

                String thanaSQL = "SELECT  THANA_NAME FROM thana  WHERE ID = '" + thanaId + "'";
                String thanaName = dbsession.createSQLQuery(thanaSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(thanaSQL).uniqueResult().toString();

                String adduser = Integer.toString(memId);
                adddate = dateFormatX.format(dateToday);
                String homeDistrict = districtName;
                String homeThana = thanaName;
                String status = "0";

                System.out.println("homeDistrict ::" + homeDistrict);
                System.out.println("homeThana ::" + homeThana);

                ratingDate = dateFormatX.format(dateToday);

                //insert member
                //insert member_credential
                //insert member_type
                //insert member_custom_order
                //insert member_view_setting
                //insert member_deviceid
                //insert member_designation
                //insert member_additional_info
                //insert member_organization
                //insert member_rating_point
                //insert member_social_link
                //insert member_profile_bg
                //insert address_book
                //insert member_address
                memberAddSQL = dbsession.createSQLQuery("INSERT INTO member("
                        + "id,"
                        + "member_id,"
                        + "member_name,"
                        + "home_district,"
                        + "home_thana,"
                        + "mobile,"
                        + "country_code,"
                        + "status,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip,"
                        + "mod_date"
                        + ") VALUES("
                        + "'" + regMemberId + "',"
                        + "'" + regMemberId + "',"
                        + "'" + fullName + "',"
                        + "'" + homeDistrict + "',"
                        + "'" + homeThana + "',"
                        + "'" + mobileNumber + "',"
                        + "'" + memberCountryCode + "',"
                        + "'" + status + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("memberAddSQL ::" + memberAddSQL);
                System.out.println("memberAddSQL ::" + memberAddSQL);

                memberAddSQL.executeUpdate();

                Query mCredentialAddSQL = dbsession.createSQLQuery("INSERT INTO member_credential("
                        + "id,"
                        + "member_id,"
                        + "member_key,"
                        + "status,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + tempPassEnc + "',"
                        + "'" + tempPassStatus + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mCredentialAddSQL ::" + mCredentialAddSQL);
                System.out.println("mCredentialAddSQL ::" + mCredentialAddSQL);

                mCredentialAddSQL.executeUpdate();

//                //                member = new Member();
//                MemberType memberType = new MemberType();
//                MemberTypeInfo memberTypeInfo = new MemberTypeInfo();
////                memberId1 = 0;
////                memberId1 = Integer.parseInt(memberId);
//
//                member.setId(Integer.parseInt(memberId));
//                memberType.setMember(member);
//                memberTypeInfo.setMemberTypeId(memberShipType);
//                memberType.setMemberTypeInfo(memberTypeInfo);
//                memberType.setId(Integer.parseInt(memberTypeId));
//                memberType.setTd(endDate1);
//                memberType.setEd(startDate1);
//
//                dbsession.save(memberType);
                Query mMemberTypeAddSQL = dbsession.createSQLQuery("INSERT INTO member_type("
                        + "id,"
                        + "member_id,"
                        + "member_type_id,"
                        + "ed,"
                        + "td"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + memberTypeId + "',"
                        + "'" + ed + "',"
                        + "'" + td + "'"
                        + "  ) ");

                logger.info("mMemberTypeAddSQL ::" + mMemberTypeAddSQL);
                System.out.println("mCredentialAddSQL ::" + mMemberTypeAddSQL);

                mMemberTypeAddSQL.executeUpdate();

                Query mMemberCustomOrderAddSQL = dbsession.createSQLQuery("INSERT INTO member_custom_order("
                        + "id,"
                        + "member_id,"
                        + "custom_order,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + memberCustomOrder + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberCustomOrderAddSQL ::" + mMemberCustomOrderAddSQL);
                System.out.println("mMemberCustomOrderAddSQL ::" + mMemberCustomOrderAddSQL);

                mMemberCustomOrderAddSQL.executeUpdate();

                Query mMemberViewSettingAddSQL = dbsession.createSQLQuery("INSERT INTO member_view_setting("
                        + "id,"
                        + "member_id,"
                        + "mobile_view,"
                        + "phone1_view,"
                        + "phone2_view,"
                        + "email_view,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'1',"
                        + "'1',"
                        + "'1',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberViewSettingAddSQL ::" + mMemberViewSettingAddSQL);
                System.out.println("mMemberViewSettingAddSQL ::" + mMemberViewSettingAddSQL);

                mMemberViewSettingAddSQL.executeUpdate();

                Query mMemberDeviceIdAddSQL = dbsession.createSQLQuery("INSERT INTO member_deviceid("
                        + "id,"
                        + "member_id,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                logger.info("mMemberDeviceIdAddSQL ::" + mMemberDeviceIdAddSQL);
                System.out.println("mMemberDeviceIdAddSQL ::" + mMemberDeviceIdAddSQL);

                mMemberDeviceIdAddSQL.executeUpdate();

                Query mMemberDesignationAddSQL = dbsession.createSQLQuery("INSERT INTO member_designation("
                        + "id,"
                        + "member_id,"
                        + "designation_id,"
                        + "status,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'1',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                logger.info("mMemberDesignationAddSQL ::" + mMemberDesignationAddSQL);
                System.out.println("mMemberDesignationAddSQL ::" + mMemberDesignationAddSQL);

                mMemberDesignationAddSQL.executeUpdate();

                Query mMemberAdditionalInfoAddSQL = dbsession.createSQLQuery("INSERT INTO member_additional_info("
                        + "id,"
                        + "member_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberAdditionalInfoAddSQL ::" + mMemberAdditionalInfoAddSQL);
                System.out.println("mMemberAdditionalInfoAddSQL ::" + mMemberAdditionalInfoAddSQL);

                mMemberAdditionalInfoAddSQL.executeUpdate();

                Query mMemberOrganizationAddSQL = dbsession.createSQLQuery("INSERT INTO member_organization("
                        + "id,"
                        + "member_id,"
                        + "organization_id,"
                        + "status,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'1',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                logger.info("mMemberOrganizationAddSQL ::" + mMemberOrganizationAddSQL);
                System.out.println("mMemberOrganizationAddSQL ::" + mMemberOrganizationAddSQL);

                mMemberOrganizationAddSQL.executeUpdate();

                Query mMemberRatingPointAddSQL = dbsession.createSQLQuery("INSERT INTO member_rating_point("
                        + "id,"
                        + "member_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberRatingPointAddSQL ::" + mMemberRatingPointAddSQL);
                System.out.println("mMemberRatingPointAddSQL ::" + mMemberRatingPointAddSQL);

                mMemberRatingPointAddSQL.executeUpdate();

                Query mMemberSocialLinkAddSQL = dbsession.createSQLQuery("INSERT INTO member_social_link("
                        + "id,"
                        + "member_id,"
                        + "link_type_id,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'1',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberSocialLinkAddSQL ::" + mMemberSocialLinkAddSQL);
                System.out.println("mMemberSocialLinkAddSQL ::" + mMemberSocialLinkAddSQL);

                mMemberSocialLinkAddSQL.executeUpdate();
                
                
                Query mMemberProfileBgAddSQL = dbsession.createSQLQuery("INSERT INTO member_profile_bg("
                        + "id,"
                        + "member_id,"
                        + "bg_file_name,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'no_image.jpg',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberProfileBgAddSQL ::" + mMemberProfileBgAddSQL);
                System.out.println("mMemberProfileBgAddSQL ::" + mMemberProfileBgAddSQL);

                mMemberProfileBgAddSQL.executeUpdate();
                
                
                Query mMemberCareerObjectiveAddSQL = dbsession.createSQLQuery("INSERT INTO member_career_objective("
                        + "id,"
                        + "member_id,"
                        + "objectives,"
                        + "update_time"
                        + ") VALUES("
                        + "" + regMemberId + ","
                        + "" + regMemberId + ","
                        + "'',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("mMemberCareerObjectiveAddSQL ::" + mMemberCareerObjectiveAddSQL);
                System.out.println("mMemberCareerObjectiveAddSQL ::" + mMemberCareerObjectiveAddSQL);

                mMemberCareerObjectiveAddSQL.executeUpdate();
                

                //insert address_book
                //insert member_address
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {

                    //  String smsMsg = "Your verification code is" + member_temp_pass_phone + " Email code is " + member_temp_pass_email;
                    String smsMsg = "Your JoyBangla temporary password is " + member_temp_pass_phone + ".Please change password after successfully login.";
                    //  String receipentMobileNo = "+8801755656354";
                    String receipentMobileNo = mobileNumber;
                    logger.info("WEB :: forgotPasswordSubmitData ::SMS : " + smsMsg);
                    logger.info("WEB :: forgotPasswordSubmitData ::receipentMobileNo : " + receipentMobileNo);
                    //    System.out.println(SendSMS(sms, receipentMobileNo));

                    //  String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);
                    String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);

                    logger.info("WEB :: forgotPasswordSubmitData ::sendsms1::" + sendsms1);

                    responseObj = new JSONObject();
                    responseObj.put("tempMemId", regMemberId);
                    responseObj.put("name", fullName);
                    responseObj.put("mobile", mobileNumber);
                    responseObj.put("tempPass", member_temp_pass_phone);
                    responseObj.put("tempPassEnc", tempPassEnc);
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "1");
                    logingObj.put("ResponseText", "Member info added successfully");
                    logingObj.put("ResponseData", responseObj);
                } else {

                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "ERROR!!! When member info added ");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("ERROR!!! When member info added");
                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Mobile number already exist.");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("mobile number :" + mobileNumber + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        // finally {
        //     dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
