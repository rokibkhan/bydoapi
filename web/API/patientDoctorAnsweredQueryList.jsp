<%-- 
    Document   : patientDoctorAnsweredQueryList
    Created on : MAY 06, 2020, 10:35:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientDoctorAnsweredQueryList_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: patientDoctorAnsweredQueryList rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String contentId = "";
    String contentCategory = "";
    String contentType = "";
    String contentTitle = "";
    String contentShortDesc = "";
    String contentDesc = "";
    String contentDate = "";
    String contentPostLink = "";
    String contentPostPicture = "";
    String contentPostShare = "";

    String pictureOpt = "";
    String videoOpt = "";

    String contentOwnerId = "";
    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";

    String ownContentFlag = "";

    // String contentTotalLikeCount = "";
    //  String contentTotalCommentCount = "";
    //  String contentTotalShareCount = "";
    int totalLikeCount = 0;
    String totalLikeCountText = "";
    int totalCommentCount = 0;
    int totalShareCount = 0;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    String pQuerySQL = null;
    String pQueryCountSQL = null;
    Query pQuerySQLQry = null;
    Object[] pQueryObj = null;

    String pQueryId = "";
    String pQueryParent = "";
    String pQueryPatientId = "";
    String pQueryProblemCategory = "";
    String pQueryDesc = "";
    String pQuerySufferingDate = "";
    String pQueryDate = "";
    String pQueryStatus = "";
    String pQueryReplyStatus = "";
    String pQueryCommentType = "";
    String pQueryCommenterId = "";

    String pQueryProblemCategoryId = "";
    String pQueryProblemCategoryName = "";

    String fever = "";
    String cough = "";
    String tiredness = "";
    String nasalcongestion = "";
    String headache = "";

    String feverText = "";
    String coughText = "";
    String tirednessText = "";
    String nasalcongestionText = "";
    String headacheText = "";

    String patientName = "";
    String patientEmail = "";
    String patientPhone = "";
    String patientDOB = "";
    String patientGender = "";

    String queryOwnerId = "";
    String queryOwnerName = "";
    String queryOwnerPic = "";
    String queryOwnerPicLink = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
//                        if (!filter.equals("")) {
//                            filterSQLStr = " AND pc.post_content_title LIKE '%" + filter + "%' ";
//                        } else {
//                            filterSQLStr = "";
//                        }

                        if (!filter.equals("")) {
                            filterSQLStr = " AND pq.query_desc LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM post_content pc "
//                                + "LEFT JOIN member AS m ON m.id = pc.content_owner "
//                                + "WHERE pc.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pc.id_post_content DESC";
//                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
//                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
//                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
//                                + "WHERE pq.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pq.mod_date DESC";
                        /*
                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
                               + "WHERE pq.published = 1 AND pq.parent = 0 AND pq.assign_doctor = '" + memId + "' "
                               + " " + filterSQLStr + " "
                               + "ORDER BY pq.mod_date DESC";
                        */
                        pQueryCountSQL = "SELECT  count(*) FROM patient_query pq "
                                + "WHERE pq.parent = 0 AND pq.assign_doctor = '" + memId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC";

                        String numrow1 = dbsession.createSQLQuery(pQueryCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pQueryCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;
                        /*
                        pQuerySQL = "SELECT  pq.query_id, pq.parent, pq.patient_id,pq.problem_category,"
                                + "pq.query_desc,pq.suffering_date,pq.query_date,pq.status,pq.reply_status,"
                                + "pq.comment_type,pq.commenter_id,"
                                + "pqc.fever, pqc.cough, pqc.tiredness, pqc.nasalcongestion, pqc.headache,  "
                                + "pi.patient_name,pi.patient_email, pi.patient_phone, pi.patient_dob, pi.patient_gender,  "
                                + "m.id queryOwnerId,m.member_name  queryOwnerName, m.picture_name queryOwnerPic  "
                                + "FROM patient_query pq "
                                + "LEFT JOIN patient_query_covid AS pqc ON pq.query_id = pqc.query_id "
                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
                                + "WHERE pq.published = 1 AND pq.parent = 0  AND pq.assign_doctor = '" + memId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";
                        
                        */
                        
                        pQuerySQL = "SELECT  pq.query_id, pq.parent, pq.patient_id,pq.problem_category,"
                                + "pq.query_desc,pq.suffering_date,pq.query_date,pq.status,pq.reply_status,"
                                + "pq.comment_type,pq.commenter_id,"
                                + "pqc.fever, pqc.cough, pqc.tiredness, pqc.nasalcongestion, pqc.headache,  "
                                + "pi.patient_name,pi.patient_email, pi.patient_phone, pi.patient_dob, pi.patient_gender,  "
                                + "m.id queryOwnerId,m.member_name  queryOwnerName, m.picture_name queryOwnerPic  "
                                + "FROM patient_query pq "
                                + "LEFT JOIN patient_query_covid AS pqc ON pq.query_id = pqc.query_id "
                                + "LEFT JOIN patient_info AS pi ON pq.patient_id = pi.patient_id "
                                + "LEFT JOIN member AS m ON m.id = pq.member_id "
                                + "WHERE pq.parent = 0  AND pq.assign_doctor = '" + memId + "' "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pq.mod_date DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";
                        

                        System.out.println("pQuerySQL :" + pQuerySQL);

                        pQuerySQLQry = dbsession.createSQLQuery(pQuerySQL);

                        for (Iterator pQueryItr = pQuerySQLQry.list().iterator(); pQueryItr.hasNext();) {

                            pQueryObj = (Object[]) pQueryItr.next();

                            pQueryId = pQueryObj[0].toString();
                            pQueryParent = pQueryObj[1] == null ? "" : pQueryObj[1].toString();
                            pQueryPatientId = pQueryObj[2] == null ? "" : pQueryObj[2].toString();
                            pQueryProblemCategory = pQueryObj[3] == null ? "" : pQueryObj[3].toString();
                            pQueryDesc = pQueryObj[4] == null ? "" : pQueryObj[4].toString();
                            pQuerySufferingDate = pQueryObj[5] == null ? "" : pQueryObj[5].toString();
                            pQueryDate = pQueryObj[6] == null ? "" : pQueryObj[6].toString();
                            pQueryStatus = pQueryObj[7] == null ? "" : pQueryObj[7].toString();
                            pQueryReplyStatus = pQueryObj[8] == null ? "" : pQueryObj[8].toString();
                            pQueryCommentType = pQueryObj[9] == null ? "" : pQueryObj[9].toString();
                            pQueryCommenterId = pQueryObj[10] == null ? "" : pQueryObj[10].toString();

                            fever = pQueryObj[11] == null ? "" : pQueryObj[11].toString();
                            if (fever.equals("1")) {
                                feverText = "Yes";
                            } else {
                                feverText = "No";
                            }
                            cough = pQueryObj[12] == null ? "" : pQueryObj[12].toString();
                            if (cough.equals("1")) {
                                coughText = "Yes";
                            } else {
                                coughText = "No";
                            }
                            tiredness = pQueryObj[13] == null ? "" : pQueryObj[13].toString();
                            if (tiredness.equals("1")) {
                                tirednessText = "Yes";
                            } else {
                                tirednessText = "No";
                            }
                            nasalcongestion = pQueryObj[14] == null ? "" : pQueryObj[14].toString();
                            if (nasalcongestion.equals("1")) {
                                nasalcongestionText = "Yes";
                            } else {
                                nasalcongestionText = "No";
                            }
                            headache = pQueryObj[15] == null ? "" : pQueryObj[15].toString();
                            if (headache.equals("1")) {
                                headacheText = "Yes";
                            } else {
                                headacheText = "No";
                            }

                            patientName = pQueryObj[16] == null ? "" : pQueryObj[16].toString();
                            patientEmail = pQueryObj[17] == null ? "" : pQueryObj[17].toString();
                            patientPhone = pQueryObj[18] == null ? "" : pQueryObj[18].toString();
                            patientDOB = pQueryObj[19] == null ? "" : pQueryObj[19].toString();
                            patientGender = pQueryObj[20] == null ? "" : pQueryObj[20].toString();

                            queryOwnerId = pQueryObj[21] == null ? "" : pQueryObj[21].toString();
                            queryOwnerName = pQueryObj[22] == null ? "" : pQueryObj[22].toString();
                            queryOwnerPic = pQueryObj[23] == null ? "" : pQueryObj[23].toString();
                            queryOwnerPicLink = GlobalVariable.imageMemberDirLink + queryOwnerPic;

//                            mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + contentOwnerId + "' AND SYSDATE() between mt.ed and mt.td");
//
//                            for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {
//
//                                mMemberTypeObj = (Object[]) mMemberTypeItr.next();
//                                mMemberTypeId = mMemberTypeObj[0].toString();
//                                mMemberTypeName = mMemberTypeObj[1].toString();
//
//                            }
                            postContentObj = new JSONObject();
                            postContentObj.put("pQueryId", pQueryId);

                            postContentObj.put("patientId", pQueryPatientId);
                            postContentObj.put("patientName", patientName);
                            postContentObj.put("patientEmail", patientEmail);
                            postContentObj.put("patientPhone", patientPhone);
                            postContentObj.put("patientDOB", patientDOB);
                            postContentObj.put("patientGender", patientGender);

                            postContentObj.put("pQueryParent", pQueryParent);
                            postContentObj.put("pQueryPatientId", pQueryPatientId);

                            postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategory);
                            postContentObj.put("pQueryProblemCategoryName", pQueryProblemCategoryName);

                            postContentObj.put("pQueryDesc", pQueryDesc);
                            postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                            postContentObj.put("pQueryDate", pQueryDate);
                            postContentObj.put("pQueryStatus", pQueryStatus);
                            postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                            postContentObj.put("pQueryCommentType", pQueryCommentType);
                            postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                            postContentObj.put("fever", fever);
                            postContentObj.put("feverText", feverText);

                            postContentObj.put("cough", cough);
                            postContentObj.put("coughText", coughText);

                            postContentObj.put("tiredness", tiredness);
                            postContentObj.put("tirednessText", tirednessText);

                            postContentObj.put("nasalcongestion", nasalcongestion);
                            postContentObj.put("nasalcongestionText", nasalcongestionText);

                            postContentObj.put("headache", headache);
                            postContentObj.put("headacheText", headacheText);

                            //   postContentObj.put("queryOwnerType", mMemberTypeName);
                            postContentObj.put("queryOwnerId", queryOwnerId);
                            postContentObj.put("queryOwnerName", queryOwnerName);
                            postContentObj.put("queryOwnerPic", queryOwnerPicLink);

                            postContentObjArr.add(postContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", postContentObjArr);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
