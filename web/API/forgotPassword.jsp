<%-- 
    Document   : forgotPassword
    Created on : APR 29, 2020, 11:40:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("forgotPassword_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String memberMobileNumber = "";

    getRegistryID getId = new getRegistryID();
    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();
    
    String adddate = "";
    
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberMobileNumber")) {
        key = request.getParameter("key").trim();

        //    memberId = request.getParameter("memberId").trim();
        memberMobileNumber = request.getParameter("memberMobileNumber").trim();

        memberId = memberMobileNumber;

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String firstLetter = memberMobileNumber.substring(0, 1);
    String adjustLettter1 = "+88";
    String adjustLettter2 = "88";
    String adjustLettter3 = "+";

    String mobileNumber1 = "";
    String mobileNumber2 = "";

    if (firstLetter.equals("0")) {
        mobileNumber1 = adjustLettter1 + memberMobileNumber;
        mobileNumber2 = adjustLettter2 + memberMobileNumber;

        System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 0 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("8")) {
        mobileNumber1 = adjustLettter3 + memberMobileNumber;
        mobileNumber2 = adjustLettter3 + memberMobileNumber;
        System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 8 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("+")) {
        mobileNumber1 = memberMobileNumber;
        mobileNumber2 = memberMobileNumber;

        System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: + :: mobileNumber2 ::" + mobileNumber2);

    }

    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberMobile = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: forgotPassword API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    SendSMS sendsms = new SendSMS();
    SendEmail sendemail = new SendEmail();

    RandomString randNumber = new RandomString();

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    //String pinCode = randNumber.randomString(4,2);
    String member_pass_code_phone = randNumber.randomString(4, 2);
    String member_pass_code_email = randNumber.randomString(6, 2);

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "' AND status = '1'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    memberName = member.getMemberName();

                    memberMobile = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();

                }
                
                // Check sent otp exceed limit 5
                
                int otpcount = 0;
                
                    
                String today = dateFormatYmd.format(dateToday);
                Query memberOtpCount = dbsession.createSQLQuery("SELECT count(otp) FROM  member_otp  where member_id = '" + memId  + "' AND status = '1' AND  DATE(otp_sent_date) = '"+today +"'");

                 if (!memberOtpCount.list().isEmpty()) {
                     otpcount = Integer.parseInt(memberOtpCount.uniqueResult().toString());
                     
                     
                 } else {
                    // System.out.println("Do not Get Member ID of  " + username);
                 }
                
                

                //check New Mobile Number with old Mobile number
                
                
                if(otpcount >= 5)
                {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "Your daily OTP limit of Forget Password has exceeded.");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("Your daily OTP limit of Forget Password has exceeded.");

                    PrintWriter writer = response.getWriter();
                    writer.write(logingObj.toJSONString());
                    response.getWriter().flush();
                    response.getWriter().close();
                    return;

                }
                
                
                //    if (memberMobile.equalsIgnoreCase(memberMobileNumber)) {
                //  String smsMsg = "Your verification code is" + member_temp_pass_phone + " Email code is " + member_temp_pass_email;
                //  String smsMsg = "Your JoyBangla APP temporary password is " + member_pass_code_phone + ".Please change password after successfully login.";
                String smsMsg = "# " + member_pass_code_phone + " is your BYDO App OTP code.Never share this code with anyone. 4/w+ZFwk7Zq";

                //  String receipentMobileNo = "+8801755656354";
                String receipentMobileNo = memberMobile;
                logger.info("API :: SMS : " + smsMsg);
                logger.info("API :: receipentMobileNo : " + receipentMobileNo);
                //    System.out.println(SendSMS(sms, receipentMobileNo));

                String sendsms1 = sendsms.sendSMS(smsMsg, receipentMobileNo);

                logger.info("API :: sendsms1::" + sendsms1);
                
                //
                if(! (sendsms1.equals("sending failed!") || sendsms1.equals("Exception or server unreachable")))
                {
                    String memberotpId = getId.getID(76);
                    InetAddress ip;
                    String hostname = "";
                    String ipAddress = "";
                    try {
                        ip = InetAddress.getLocalHost();
                        hostname = ip.getHostName();
                        ipAddress = ip.getHostAddress();



                    } catch (UnknownHostException e) {

                        e.printStackTrace();
                    }
                    
                    
                    adddate = dateFormatX.format(dateToday);
                    String adduser = Integer.toString(memId);
                    String status = "1";
                    
                    Query memberOtpAddSQL = dbsession.createSQLQuery("INSERT INTO member_otp("
                        + "id,"
                        + "member_id,"
                        + "otp,"
                        + "otp_type,"
                        + "otp_sent_date,"
                        + "status,"
                        + "ed,"
                        + "td,"                        
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip,"
                        + "mod_date"
                        + ") VALUES("
                        + "'" + memberotpId + "',"
                        + "'" + memId + "',"
                        + "'" + member_pass_code_phone + "',"
                        + "'" + "FORGET_PASSWORD" + "',"
                        + "'" +  adddate+ "',"
                        + "'" + status + "',"
                        + "'" + adddate + "',"
                        + "'" + "2020-09-30" + "',"                        
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "',"
                        + "'" + adddate + "'"
                        + "  ) ");

                logger.info("memberOtpAddSQL ::" + memberOtpAddSQL);
                System.out.println("memberOtpAddSQL ::" + memberOtpAddSQL);

                memberOtpAddSQL.executeUpdate();


                    
                }

                //update password in system
                String newPassEnc = new Encryption().getEncrypt(member_pass_code_phone);

                Query passUpdateSQL = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + newPassEnc + "',update_time=now(),status= '2' WHERE member_id='" + memId + "'");
                logger.info("API :: forgotPassword API passUpdateSQL :: " + passUpdateSQL);
                passUpdateSQL.executeUpdate();
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {

                    responseObj = new JSONObject();
                    responseObj.put("memId", memId);
                    responseObj.put("memberId", memberId);
                    responseObj.put("memberName", memberName);
                    responseObj.put("memberEmail", memberEmail);
                    responseObj.put("memberMobile", memberMobile);
                    responseObj.put("sendSMSId", sendsms1);
                    responseObj.put("pass", member_pass_code_phone);

                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "1");
                    logingObj.put("ResponseText", "SMS sent successfully");
                    logingObj.put("ResponseData", responseObj);

                } else {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "ERROR!!! When member password update ");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("ERROR!!! When member password update ");
                }

//                } else {
//                    logingObj = new JSONObject();
//                    logingObj.put("ResponseCode", "0");
//                    logingObj.put("ResponseText", "Member mobile number not match ");
//                    logingObj.put("ResponseData", logingObjArray);
//                    logger.info("User ID :" + memberId + " Not Found");
//
//                }
            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Member not found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        
        //
        
        
        }
    
        else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
