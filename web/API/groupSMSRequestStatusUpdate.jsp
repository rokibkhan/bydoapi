<%-- 
    Document   : groupSMSTotalMemberInfo
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    String key = "";
    String memberId = "";
    String givenPassword = "";

    String transId = "";
    String transStatus = "";
    String transRefNo = "";
    String transRefDesc = "";

    

    String adddate = "";
    String paymentDate = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    getRegistryID getId = new getRegistryID();

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("transId") && request.getParameterMap().containsKey("transStatus") && request.getParameterMap().containsKey("transRefNo") && request.getParameterMap().containsKey("transRefDesc")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        transId = request.getParameter("transId").trim();
        transStatus = request.getParameter("transStatus").trim();
        transRefNo = request.getParameter("transRefNo").trim();
        transRefDesc = request.getParameter("transRefDesc").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: groupSMSRateInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;
    Query feeCheckSQL = null;
    Query feeUpdSQL = null;

    // JSONArray requestResponseObjArr = new JSONArray();
    JSONArray requestResponseArr = new JSONArray();
    JSONObject requestResponseObj = new JSONObject();

    JSONArray paymentTransaResponseArr = new JSONArray();
    JSONObject paymentTransaResponseObj = new JSONObject();

    JSONObject paymentRequestObj = new JSONObject();
    JSONArray paymentRequestArr = new JSONArray();

    Query smsRequestAddSQL = null;
    Query smsPaymentFeeAddSQL = null;

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        feeCheckSQL = dbsession.createSQLQuery("SELECT * FROM group_sms_request WHERE MEMBER_ID = '" + memId + "' AND REQUEST_ID='" + transId + "' AND STATUS='" + transStatus + "'");

                        logger.info("API :: feeCheckSQL ::" + feeCheckSQL);

                        if (feeCheckSQL.list().isEmpty()) {
                            
                            logger.info("API :: feeCheckSQL  Inner::" + feeCheckSQL);

                            // String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
                            // String ref_no = bankTransactionID;
                            feeUpdSQL = dbsession.createSQLQuery("UPDATE  group_sms_request SET REF_NO='" + transRefNo + "',REF_DESCRIPTION ='" + transRefDesc + "', PAY_OPTION = '1',MOD_USER ='" + memId + "',MOD_DATE=now(),STATUS='" + transStatus + "' WHERE REQUEST_ID='" + transId + "'");

                            feeUpdSQL.executeUpdate();
                            
                            

                            paymentRequestObj = new JSONObject();
                            paymentRequestObj.put("TransactionId", transId);
                            paymentRequestObj.put("TransactionStatus", transStatus);
                            paymentRequestObj.put("ReferenceNo", transRefNo);
                            paymentRequestObj.put("ReferenceDesc", transRefDesc);

                            paymentRequestArr.add(paymentRequestObj);

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Found");
                            logingObj.put("ResponseData", paymentRequestArr);

                        } else {
                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "Error!!! Trnasaction status already updated");
                            logingObj.put("ResponseData", paymentRequestArr);
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
