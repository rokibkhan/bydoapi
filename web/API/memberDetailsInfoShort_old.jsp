<%-- 
    Document    : memberDetailsInfoShort
    Created on  : APR 28, 2020, 9:42:05 PM
    Author      : TAHAJJAT
    
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("mId")) {
    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("mId")) {
        key = request.getParameter("key").trim();

        //        postContentCategory = request.getParameter("contentCategory") == null ? "" : request.getParameter("contentCategory").trim();
        memberId = request.getParameter("memberId") == null ? "" : request.getParameter("memberId").trim();

        givenPassword = request.getParameter("password") == null ? "" : request.getParameter("password").trim();
        mId = request.getParameter("mId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    String memId = "";

    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";
    String unitName = "";
    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberDetailsInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject professionObj = new JSONObject();
    JSONObject professionResponseObj = new JSONObject();
    JSONArray professionObjArray = new JSONArray();

    JSONObject trainingObj = new JSONObject();
    JSONObject trainingResponseObj = new JSONObject();
    JSONArray trainingObjArray = new JSONArray();

    JSONObject publicationObj = new JSONObject();
    JSONObject publicationResponseObj = new JSONObject();
    JSONArray publicationObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMemberCheckSQL = null;
    Object[] qMemberCheckObj = null;

    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    Query mMemberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    int memberXId = 0;
    String memberXIEBId = "";
    String memberXName = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String bacgroundPictureName = "";
    String bacgroundPictureLink = "";

    if (rec == 1) {

        try {
            /*
            // qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE member_id = '" + memberId + "'");
            qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE mobile = '" + memberId1 + "'");

            if (!qMemberCheckSQL.list().isEmpty()) {
             
                for (Iterator qMemberCheckItr = qMemberCheckSQL.list().iterator(); qMemberCheckItr.hasNext();) {

                    qMemberCheckObj = (Object[]) qMemberCheckItr.next();
                    memId = qMemberCheckObj[0].toString();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");
                    dbPass = memberKeySQL.uniqueResult().toString();
                    
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {
                        
             */

            mMemberSQL = dbsession.createQuery(" from Member WHERE id = '" + mId + "'");

            for (Iterator memberItr = mMemberSQL.list().iterator(); memberItr.hasNext();) {
                member = (Member) memberItr.next();
                memberXId = member.getId();

                memberXIEBId = member.getMemberId();
                memberXName = member.getMemberName();

                //instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();
                fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                motherName = member.getMotherName() == null ? "" : member.getMotherName();

                dob = member.getDob();
                gender = member.getGender() == null ? "" : member.getGender().trim();

                mobileNo = member.getMobile() == null ? "" : member.getMobile();
                phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                memberXName = member.getMemberName();

                unitName = member.getUnitName() == null ? "" : member.getUnitName();
                nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();

                Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + mId + "'");

                logger.info("API :: Login API memberOrganizationSQL ::" + mOrganizationSQL);

                if (!mOrganizationSQL.list().isEmpty()) {
                    for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {
                        //   addressBook = (AddressBook) mAddressItr.next();

                        mOrganization = (MemberOrganization) mOrganizationItr.next();

                        organizationName = mOrganization.getOrganizationInfo().getOrganizationName();
                        organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo();
                        organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                    }
                }

                memberXPictureName = member.getPictureName();

                memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                // bacgroundPictureName = "no_image.jpg";
                Query memberBgPictureSQL = dbsession.createSQLQuery("SELECT bg_file_name FROM member_profile_bg where member_id='" + mId + "'");
                bacgroundPictureName = memberBgPictureSQL.uniqueResult().toString();

                bacgroundPictureLink = GlobalVariable.imageDirLink + "backgroundpicture/" + bacgroundPictureName;

//                            memberXPhone1View = "1";
//                            memberXPhone2View = "1";
//                            memberXMobileView = "1";
//                            memberXMobileView = "1";
                mMemberOptionViewSQL = dbsession.createSQLQuery("SELECT phone1_view,phone2_view,mobile_view,email_view FROM member_view_setting WHERE member_id='" + mId + "'");
                if (!mMemberOptionViewSQL.list().isEmpty()) {
                    for (Iterator mMemberOptionViewItr = mMemberOptionViewSQL.list().iterator(); mMemberOptionViewItr.hasNext();) {

                        mMemberOptionViewObj = (Object[]) mMemberOptionViewItr.next();
                        memberXPhone1View = mMemberOptionViewObj[0].toString();
                        memberXPhone2View = mMemberOptionViewObj[1].toString();
                        memberXMobileView = mMemberOptionViewObj[2].toString();
                        memberXMobileView = mMemberOptionViewObj[3].toString();

                    }
                }

                mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + mId + "' AND SYSDATE() between mt.ed and mt.td");
                if (!mMemberTypeSQL.list().isEmpty()) {
                    for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                        mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                        mMemberTypeId = mMemberTypeObj[0].toString();
                        mMemberTypeName = mMemberTypeObj[1].toString();

                    }
                }

                responseObj = new JSONObject();
                responseObj.put("Id", memberXId);

                responseObj.put("organizationName", organizationName);
                responseObj.put("organizationLogo", organizationLogoLink);
                responseObj.put("unitName", unitName);
                responseObj.put("nirbachoniAson", nirbachoniAson);
                responseObj.put("ratingPoint", ratingPoint);
                responseObj.put("likeCount", likeCount);
                responseObj.put("shareCount", shareCount);
                responseObj.put("uploadCount", uploadCount);
                responseObj.put("bloodDonationCount", bloodDonationCount);

                responseObj.put("MemberLifeStatus", memberLifeStatus);
                responseObj.put("MemberLifeStatusText", memberLifeStatusText);
                responseObj.put("MemberTypeId", mMemberTypeId);
                responseObj.put("MemberTypeName", mMemberTypeName);
                responseObj.put("MemberId", memberXIEBId);
                responseObj.put("Name", memberXName);

                responseObj.put("FatherName", fatherName);
                responseObj.put("MotherName", motherName);
                responseObj.put("PlaceOfBirth", placeOfBirth);
                responseObj.put("DateOfBirth", dob);
                responseObj.put("Gender", gender);
                responseObj.put("BloodGroup", bloodGroup);

                responseObj.put("Mobile", mobileNo);
                responseObj.put("Phone1", phone1);
                responseObj.put("Phone2", phone2);
                responseObj.put("Email", memberEmail);

                responseObj.put("Phone1View", memberXPhone1View);
                responseObj.put("Phone2View", memberXPhone2View);
                responseObj.put("MobileView", memberXMobileView);
                responseObj.put("EmailView", memberXEmailView);

                responseObj.put("Picture", memberXPictureLink);

                responseObj.put("BacgroundPicture", bacgroundPictureLink);

                responseObj.put("CenterId", memberCenterId);
                responseObj.put("CenterName", memberXCenter);

                responseObj.put("DivisionId", memberDivisionId);
                responseObj.put("DivisionShortName", memberXDivisionShort);
                responseObj.put("DivisionFullName", memberXDivisionFull);
                responseObj.put("Address", addressObjArray);
                responseObj.put("Education", educationObjArray);
                responseObj.put("Profession", professionObjArray);
                responseObj.put("Training", trainingObjArray);
                responseObj.put("Publication", publicationObjArray);

                memberContentObjArr.add(responseObj);

            }

            logingObj = new JSONObject();
            logingObj.put("ResponseCode", "1");
            logingObj.put("ResponseText", "Found");
            logingObj.put("ResponseData", memberContentObjArr);
            /*
                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }                    

                }
            
                        
            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
             */
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        //  finally {
        //      dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
