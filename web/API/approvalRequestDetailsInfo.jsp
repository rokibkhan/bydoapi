<%-- 
    Document   : approvalRequestDetailsInfo
    Created on : NOV 08, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("approvalRequestDetailsInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String appRequestMemberId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("requestMemId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        appRequestMemberId = request.getParameter("requestMemId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: approvalRequestDetailsInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject mAddressResponseObj = new JSONObject();
    JSONArray mAddressObjArray = new JSONArray();

    JSONObject pAddressResponseObj = new JSONObject();
    JSONArray pAddressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject subjectResponseObj = new JSONObject();
    JSONArray subjectObjArray = new JSONArray();

    JSONObject professionObj = new JSONObject();
    JSONObject professionResponseObj = new JSONObject();
    JSONArray professionObjArray = new JSONArray();

    JSONObject documentResponseObj = new JSONObject();
    JSONArray documentObjArray = new JSONArray();

    JSONObject approvalRequestObj = new JSONObject();
    JSONArray approvalRequestObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String requestId = "";
    String requestStatus = "";
    String requestStatusText = "";
    String requestStatusOpt = "";
    String requestDate = "";
    String requestMemId = "";
    String requestMemDetailsLink = "";
    String requestMemName = "";
    String requestMemPictureName = "";
    String requestMemPictureLink = "";
    String requestMemFatherName = "";
    String requestMemMotherName = "";
    String requestMemPlaceOfBirth = "";
    String requestMemDOB = "";
    String requestMemAge = "";
    String requestMemNationality = "";
    String requestMemGender = "";
    String requestMemApplyFor = "";
    String requestMemOldMemberId = "";
    String requestMemPhone1 = "";
    String requestMemPhone2 = "";
    String requestMemMobile = "";
    String requestMemEmailId = "";

    if (rec == 1) {

        try {

            //    qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        pContentSQL = "SELECT pt.id,pt.member_id tempMemberId,pt.proposer_id,pt.status,pt.request_date, "
                                + "mt.member_name,mt.picture_name, mt.father_name, mt.mother_name,  "
                                + "mt.place_of_birth,mt.dob, mt.age,mt.nationality,mt.gender,mt.apply_for,mt.old_member_id,"
                                + "mt.phone1,mt.phone2,mt.mobile,mt.email_id   "
                                + "FROM member_proposer_info_temp pt,member_temp mt "
                                + "WHERE  pt.member_id = mt.member_id "
                                + "AND pt.member_id = '" + appRequestMemberId + "' "
                                + "AND pt.proposer_id=" + memId + "";

                        pContentSQLQry = dbsession.createSQLQuery(pContentSQL);

                        for (Iterator pContentItr = pContentSQLQry.list().iterator(); pContentItr.hasNext();) {

                            pContentObj = (Object[]) pContentItr.next();
                            requestId = pContentObj[0].toString();
                            requestMemId = pContentObj[1] == null ? "" : pContentObj[1].toString();
                            requestStatus = pContentObj[3] == null ? "" : pContentObj[3].toString();

                            if (requestStatus.equals("0")) {
                                requestStatusText = "Wating for Approval";
                            }
                            if (requestStatus.equals("1")) {
                                requestStatusText = "Approved";
                            }
                            if (requestStatus.equals("2")) {
                                requestStatusText = "Declined";
                            }

                            requestDate = pContentObj[4] == null ? "" : pContentObj[4].toString();
                            requestMemName = pContentObj[5] == null ? "" : pContentObj[5].toString();
                            requestMemPictureName = pContentObj[6] == null ? "" : pContentObj[6].toString();
                            requestMemPictureLink = GlobalVariable.imageMemberDirLink + requestMemPictureName;

                            requestMemFatherName = pContentObj[7] == null ? "" : pContentObj[7].toString();
                            requestMemMotherName = pContentObj[8] == null ? "" : pContentObj[8].toString();

                            requestMemPlaceOfBirth = pContentObj[9] == null ? "" : pContentObj[9].toString();
                            requestMemDOB = pContentObj[10] == null ? "" : pContentObj[10].toString();
                            requestMemAge = pContentObj[11] == null ? "" : pContentObj[11].toString();
                            requestMemNationality = pContentObj[12] == null ? "" : pContentObj[12].toString();
                            requestMemGender = pContentObj[13] == null ? "" : pContentObj[13].toString();
                            requestMemApplyFor = pContentObj[14] == null ? "" : pContentObj[14].toString();
                            requestMemOldMemberId = pContentObj[15] == null ? "" : pContentObj[15].toString();
                            requestMemPhone1 = pContentObj[16] == null ? "" : pContentObj[16].toString();
                            requestMemPhone2 = pContentObj[17] == null ? "" : pContentObj[17].toString();
                            requestMemMobile = pContentObj[18] == null ? "" : pContentObj[18].toString();
                            requestMemEmailId = pContentObj[19] == null ? "" : pContentObj[19].toString();

                            //Request Member Address 
                            Query madrSQL = null;
                            Object[] madrObject = null;

                            Query padrSQL = null;
                            Object[] padrObject = null;

                            String mAddressId = "";
                            String mCountryId = "";
                            int mDistrictId = 0;
                            String mThanaId = "";
                            String mThanaName = "";
                            String mThanaDistrictName = "";
                            String mAddressLine1 = "";
                            String mAddressLine2 = "";
                            String mZipOffice = "";
                            String mZipCode = "";
                            String mCountry = "Bangladesh";

                            String pAddressId = "";
                            String pCountryId = "";
                            int pDistrictId = 0;
                            String pThanaId = "";
                            String pThanaName = "";
                            String pThanaDistrictName = "";
                            String pAddressLine1 = "";
                            String pAddressLine2 = "";
                            String pZipOffice = "";
                            String pZipCode = "";
                            String pCountry = "Bangladesh";

                            String mAddressStr = "";
                            String pAddressStr = "";

                            Thana thana = null;

                            Query mailAddrSQL = dbsession.createSQLQuery("select * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + appRequestMemberId + " and address_Type='M' ) ");
                            for (Iterator itr2 = mailAddrSQL.list().iterator(); itr2.hasNext();) {
                                madrObject = (Object[]) itr2.next();
                                mAddressId = madrObject[0].toString();
                                mAddressLine1 = madrObject[1].toString();
                                mAddressLine2 = madrObject[2].toString();
                                mThanaId = madrObject[3].toString();

                                Query mThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                for (Iterator itrmThana = mThanaSQL.list().iterator(); itrmThana.hasNext();) {
                                    thana = (Thana) itrmThana.next();

                                    mThanaName = thana.getThanaName();
                                    mDistrictId = thana.getDistrict().getId();
                                    mThanaDistrictName = thana.getDistrict().getDistrictName();
                                }

                                mZipCode = madrObject[4].toString();
                                mZipOffice = madrObject[8].toString();

                                mAddressStr = mAddressLine1 + "," + mAddressLine2 + "<br>" + mThanaName + "," + mThanaDistrictName + "<br>" + mZipOffice + "-" + mZipCode;

                            }
                            Query permAddrSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address_temp where member_id= " + appRequestMemberId + " and address_Type='P' ) ");
                            for (Iterator itr3 = permAddrSQL.list().iterator(); itr3.hasNext();) {

                                padrObject = (Object[]) itr3.next();
                                pAddressId = padrObject[0].toString();
                                pAddressLine1 = padrObject[1].toString();
                                pAddressLine2 = padrObject[2].toString();
                                pThanaId = padrObject[3].toString();

                                Query pThanaSQL = dbsession.createQuery(" from Thana where id='" + mThanaId + "'");
                                for (Iterator itrpThana = pThanaSQL.list().iterator(); itrpThana.hasNext();) {
                                    thana = (Thana) itrpThana.next();

                                    pThanaName = thana.getThanaName();
                                    pDistrictId = thana.getDistrict().getId();
                                    pThanaDistrictName = thana.getDistrict().getDistrictName();
                                }

                                pZipCode = padrObject[4].toString();
                                pZipOffice = padrObject[8].toString();

                                pAddressStr = pAddressLine1 + "," + pAddressLine2 + "<br>" + pThanaName + "," + pThanaDistrictName + "<br>" + pZipOffice + "-" + pZipCode;

                            }

                            mAddressResponseObj = new JSONObject();

                            mAddressResponseObj.put("AddressId", mAddressId);
                            mAddressResponseObj.put("AddressLine1", mAddressLine1);
                            mAddressResponseObj.put("AddressLine2", mAddressLine2);
                            mAddressResponseObj.put("ThanaId", mThanaId);
                            mAddressResponseObj.put("ThanaName", mThanaName);
                            mAddressResponseObj.put("DistrictId", mDistrictId);
                            mAddressResponseObj.put("ThanaDistrictName", mThanaDistrictName);
                            mAddressResponseObj.put("Country", mCountry);
                            mAddressResponseObj.put("ZipCode", mZipCode);
                            mAddressResponseObj.put("ZipOffice", mZipOffice);
                            mAddressResponseObj.put("AddressStr", mAddressStr);

                            mAddressObjArray.add(mAddressResponseObj);

                            pAddressResponseObj = new JSONObject();

                            pAddressResponseObj.put("AddressId", pAddressId);
                            pAddressResponseObj.put("AddressLine1", pAddressLine1);
                            pAddressResponseObj.put("AddressLine2", pAddressLine2);
                            pAddressResponseObj.put("ThanaId", pThanaId);
                            pAddressResponseObj.put("ThanaName", pThanaName);
                            pAddressResponseObj.put("DistrictId", pDistrictId);
                            pAddressResponseObj.put("ThanaDistrictName", pThanaDistrictName);
                            pAddressResponseObj.put("Country", pCountry);
                            pAddressResponseObj.put("ZipCode", pZipCode);
                            pAddressResponseObj.put("ZipOffice", pZipOffice);
                            pAddressResponseObj.put("AddressStr", pAddressStr);

                            pAddressObjArray.add(pAddressResponseObj);

                            //   MemberEducationInfoTemp memberEducation
                            Query eduTempSQL = null;
                            int degreeId = 0;
                            String degreeName = "";
                            String instituteName = "";
                            String boardUniversityName = "";
                            String yearOfPassing = "";
                            String resultTypeName = "";
                            String result = "";
                            int educationfInfoId = 0;
                            String memberSubdivisionFullName = "";

                            MemberEducationInfoTemp mei = null;

                            eduTempSQL = dbsession.createQuery("from MemberEducationInfoTemp where  member_id=" + appRequestMemberId + " ");

                            if (!eduTempSQL.list().isEmpty()) {
                                for (Iterator eduTempItr = eduTempSQL.list().iterator(); eduTempItr.hasNext();) {
                                    mei = (MemberEducationInfoTemp) eduTempItr.next();

                                    educationfInfoId = mei.getId();
                                    degreeId = mei.getDegree().getDegreeId();
                                    degreeName = mei.getDegree().getDegreeName();

                                    instituteName = mei.getInstituteName() == null ? "" : mei.getInstituteName();

                                    boardUniversityName = mei.getUniversity().getUniversityLongName();
                                    yearOfPassing = mei.getYearOfPassing();
                                    resultTypeName = mei.getResultType().getResultTypeName();
                                    result = mei.getResult();
                                    memberSubdivisionFullName = "";
                                    degreeName = degreeName + memberSubdivisionFullName;

                                    educationResponseObj = new JSONObject();
                                    educationResponseObj.put("EducationId", educationfInfoId);
                                    educationResponseObj.put("DegreeId", degreeId);
                                    educationResponseObj.put("DegreeName", degreeName);
                                    educationResponseObj.put("InstituteName", instituteName);
                                    educationResponseObj.put("BoardUniversityName", boardUniversityName);
                                    educationResponseObj.put("YearOfPassing", yearOfPassing);
                                    educationResponseObj.put("ResultTypeName", resultTypeName);
                                    educationResponseObj.put("Result", result);

                                    educationObjArray.add(educationResponseObj);

                                }
                            }

                            Object[] subjectObject = null;
                            String subjectName = "";
                            String subjectUniversityName = "";

                            Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                    + "member_subject_info_temp msb,university_subject usb,university u "
                                    + "WHERE msb.subject_id = usb.subject_id "
                                    + "AND usb.university_id = u.university_id "
                                    + "AND msb.member_id='" + appRequestMemberId + "'");
                            if (!subjectSQL.list().isEmpty()) {
                                for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {
                                    subjectObject = (Object[]) subjectItr.next();
                                    subjectName = subjectObject[0].toString();
                                    subjectUniversityName = subjectObject[1].toString();

                                    subjectResponseObj = new JSONObject();
                                    subjectResponseObj.put("SubjectName", subjectName);
                                    subjectResponseObj.put("SubjectUniversityName", subjectUniversityName);
                                    subjectObjArray.add(subjectResponseObj);

                                }

                            }

                            Query profTempSQL = null;
                            int profTempId = 0;
                            String organizationName = "";
                            String designationName = "";
                            String startDateProTemp = "";
                            String endDateProTemp = "";

                            MemberProfessionalInfoTemp profTemp = null;

                            profTempSQL = dbsession.createQuery("from MemberProfessionalInfoTemp where  member_id=" + appRequestMemberId + " ");
                            for (Iterator profTempItr = profTempSQL.list().iterator(); profTempItr.hasNext();) {
                                profTemp = (MemberProfessionalInfoTemp) profTempItr.next();

                                profTempId = profTemp.getId();
                                organizationName = profTemp.getCompanyName().toString();
                                designationName = profTemp.getDesignation().toString();
                                startDateProTemp = profTemp.getFromDate().toString();
                                endDateProTemp = profTemp.getTillDate().toString();

                                professionResponseObj = new JSONObject();
                                professionResponseObj.put("ProfessionalId", profTempId);
                                professionResponseObj.put("OrganizationName", organizationName);
                                professionResponseObj.put("DesignationName", designationName);
                                professionResponseObj.put("StartDateProTemp", startDateProTemp);
                                professionResponseObj.put("EndDateProTemp", endDateProTemp);
                                professionObjArray.add(professionResponseObj);
                            }

                            Query relDocSQL = null;
                            Object[] relDocbject = null;

                            String documentId = "";
                            String documentName = "";
                            String documentName1 = "";
                            String documentLink = "";

                            relDocSQL = dbsession.createSQLQuery("SELECT * FROM member_document_info_temp WHERE member_id = '" + appRequestMemberId + "'");
                            for (Iterator relDocItr = relDocSQL.list().iterator();
                                    relDocItr.hasNext();) {
                                relDocbject = (Object[]) relDocItr.next();

                                documentId = relDocbject[2].toString();
                                documentName = relDocbject[3].toString();
                                documentName1 = relDocbject[4].toString();

                                documentLink = GlobalVariable.imageDirLink + "document/" + documentName1;

                                documentResponseObj = new JSONObject();
                                documentResponseObj.put("DocumentId", documentId);
                                documentResponseObj.put("DocumentName", documentName);
                                documentResponseObj.put("DocumentName1", documentName1);
                                documentResponseObj.put("DocumentLink", documentLink);
                                documentObjArray.add(documentResponseObj);

                            }

                            approvalRequestObj = new JSONObject();
                            approvalRequestObj.put("RequestId", requestId);
                            approvalRequestObj.put("RequestMemId", requestMemId);
                            approvalRequestObj.put("RequestStatus", requestStatus);
                            approvalRequestObj.put("RequestStatusText", requestStatusText);
                            approvalRequestObj.put("RequestDate", requestDate);
                            approvalRequestObj.put("RequestMemName", requestMemName);
                            approvalRequestObj.put("RequestMemFatherName", requestMemFatherName);
                            approvalRequestObj.put("RequestMemMotherName", requestMemMotherName);

                            approvalRequestObj.put("RequestMemPlaceOfBirth", requestMemPlaceOfBirth);
                            approvalRequestObj.put("RequestMemDOB", requestMemDOB);
                            approvalRequestObj.put("RequestMemAge", requestMemAge);

                            approvalRequestObj.put("RequestMemNationality", requestMemNationality);
                            approvalRequestObj.put("RequestMemGender", requestMemGender);
                            approvalRequestObj.put("RequestMemApplyFor", requestMemApplyFor);
                            approvalRequestObj.put("RequestMemOldMemberId", requestMemOldMemberId);
                            approvalRequestObj.put("RequestMemPhone1", requestMemPhone1);
                            approvalRequestObj.put("RequestMemPhone2", requestMemPhone2);
                            approvalRequestObj.put("RequestMemMobile", requestMemMobile);
                            approvalRequestObj.put("RequestMemEmailId", requestMemEmailId);

                            approvalRequestObj.put("RequestMemPictureName", requestMemPictureName);
                            approvalRequestObj.put("RequestMemPictureLink", requestMemPictureLink);

                            approvalRequestObj.put("MailingAddress", mAddressObjArray);
                            approvalRequestObj.put("PermanentAddress", pAddressObjArray);
                            approvalRequestObj.put("Education", educationObjArray);
                            approvalRequestObj.put("Subject", subjectObjArray);
                            approvalRequestObj.put("Profession", professionObjArray);
                            approvalRequestObj.put("Document", documentObjArray);

                            approvalRequestObjArr.add(approvalRequestObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", approvalRequestObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
