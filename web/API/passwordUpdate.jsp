<%-- 
    Document   : passwordUpdate
    Created on : APR 28, 2020, 10:20:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("passwordUpdate_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String newPassword = "";

    Member member = null;
    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String dbPassStatus = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();
    JSONObject bannerResponseObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerResponseObj);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("newPassword")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        newPassword = request.getParameter("newPassword").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerResponseObj);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: passwordUpdate rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();
    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;
    Object[] memberKeyObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    //  memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");
                    //  dbPass = memberKeySQL.uniqueResult().toString();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key,status FROM member_credential where member_id='" + memId + "'");

                    for (Iterator memberKeyItr = memberKeySQL.list().iterator(); memberKeyItr.hasNext();) {
                        memberKeyObj = (Object[]) memberKeyItr.next();
                        dbPass = memberKeyObj[0].toString();
                        dbPassStatus = memberKeyObj[1] == null ? "" : memberKeyObj[1].toString();
                    }

                    logger.info("PasswordUpdate API dbPass ::" + dbPass);
                    logger.info("PasswordUpdate API status ::" + dbPassStatus);

                    logger.info("PasswordUpdate API givenPassword ::" + encryption.getEncrypt(givenPassword));

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //update password in system
                        String newPassEnc = new Encryption().getEncrypt(newPassword);

                        Query passUpdateSQL = dbsession.createSQLQuery("UPDATE  member_credential SET member_key ='" + newPassEnc + "',update_time=now(),status= '1' WHERE member_id='" + memId + "'");

                        logger.info("PasswordUpdate API passUpdateSQL ::" + passUpdateSQL);

                        passUpdateSQL.executeUpdate();

                        dbtrx.commit();

                        if (dbtrx.wasCommitted()) {

                            responseObj = new JSONObject();
                            responseObj.put("memId", memId);
                            responseObj.put("forcePassStatus", dbPassStatus);
                            responseObj.put("memberId", memberId);

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Password change successfully");
                            logingObj.put("ResponseData", responseObj);

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "ERROR!!! When member Password updated ");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("ERROR!!! When member Password updated");
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "0");
                        logingObj.put("ResponseText", "Old password not match");
                        logingObj.put("ResponseData", responseObj);
                        logger.info("User ID :" + memberId + " Old password not match");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Member not found");
                logingObj.put("ResponseData", responseObj);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", logingObj);

        }

        //finally {
        //   dbtrx.commit();
        //  }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", responseObj);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
