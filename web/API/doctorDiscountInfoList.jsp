<%-- 
    Document   : doctorDiscountInfoList
    Created on : Nov 15, 2020, 2:43:21 PM
    Author     : ROKIB
--%>



<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorDiscountInfoList.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String doctorId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();
    
    String[] starttimesplit;
    String[] endtimesplit;

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();
        doctorId = request.getParameter("doctorId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query doctorDiscountFeeSQL = null;
    Object doctorDiscountFeeObj[] = null;

    String doctorDiscountFixedFee = "";
    String doctorDiscountPercentageFee = "";
    String discountType = "";
    String doctorDiscountTypeText = "";
    String discountFeeStartDate = "";
    String discountFeeEndDate = "";
    
    JSONArray doctorDiscountFeeResponseObjArr = new JSONArray();
    JSONObject doctorDiscountFeeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    if (rec == 1) {

        try {

            doctorDiscountFeeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_discount where doctor_id = '"+doctorId +"' ORDER BY ID ASC");

            if (!doctorDiscountFeeSQL.list().isEmpty()) {
                for (Iterator it = doctorDiscountFeeSQL.list().iterator(); it.hasNext();) {

                    doctorDiscountFeeObj = (Object[]) it.next();
                    discountType = doctorDiscountFeeObj[2].toString();
                    doctorDiscountFixedFee = doctorDiscountFeeObj[3].toString();
                    doctorDiscountPercentageFee = doctorDiscountFeeObj[4].toString();
                    discountFeeStartDate = doctorDiscountFeeObj[5].toString();
                    discountFeeEndDate = doctorDiscountFeeObj[6].toString();
                    
                    starttimesplit = discountFeeStartDate.split(" ");
                    endtimesplit = discountFeeEndDate.split(" ");
                    
                    if (discountType.equals("1")) {
                        doctorDiscountTypeText = "PercentageAmount";
                    }
                    else
                    {
                        doctorDiscountTypeText = "FixedAmount";
                    }

                    doctorDiscountFeeResponseObj = new JSONObject();

                    doctorDiscountFeeResponseObj.put("doctorId", doctorId);
                    doctorDiscountFeeResponseObj.put("discountType", discountType);
                    doctorDiscountFeeResponseObj.put("doctorDiscountTypeText", doctorDiscountTypeText);
                    doctorDiscountFeeResponseObj.put("doctorDiscountFixedFee", Math.round(Double.parseDouble(doctorDiscountFixedFee)));
                    doctorDiscountFeeResponseObj.put("doctorDiscountPercentageFee", Math.round(Double.parseDouble(doctorDiscountPercentageFee)));
                    doctorDiscountFeeResponseObj.put("discountFeeStartDate", starttimesplit[0]);
                    doctorDiscountFeeResponseObj.put("discountFeeEndDate", endtimesplit[0]);

                    doctorDiscountFeeResponseObjArr.add(doctorDiscountFeeResponseObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", doctorDiscountFeeResponseObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", doctorDiscountFeeResponseObjArr);

            }
        } catch (Exception e) {

        } 
        
//        finally {
//            dbtrx.commit();
//
//        }

        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", doctorDiscountFeeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>