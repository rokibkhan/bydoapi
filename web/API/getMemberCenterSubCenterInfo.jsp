<%-- 
    Document   : getDistrictInfo
    Created on : OCT 28, 2019, 1:32:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query centerSQL = null;
    Object centerObj[] = null;
    String centerId = "";
    String centerName = "";

    JSONArray centerResponseObjArr = new JSONArray();
    JSONObject centerResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    Query subCenterSQL = null;
    Object subCenterObj[] = null;
    String subCenterId = "";
    String subCenterName = "";

    JSONArray subCenterResponseObjArr = new JSONArray();
    JSONObject subCenterResponseObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    if (rec == 1) {

        try {

            centerSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_name ASC");

            if (!centerSQL.list().isEmpty()) {
                for (Iterator it = centerSQL.list().iterator(); it.hasNext();) {

                    centerObj = (Object[]) it.next();
                    centerId = centerObj[0].toString().trim();
                    centerName = centerObj[1].toString();

                    centerResponseObj = new JSONObject();

                    centerResponseObj.put("centerId", centerId);
                    centerResponseObj.put("centerName", centerName);

                    //subCenterSQL = dbsession.createSQLQuery("SELECT  * FROM subCenter  WHERE DISTRICT_ID ='" + centerId + "' ORDER BY THANA_NAME  ASC");
                    subCenterSQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='4' AND center_parent_id ='" + centerId + "' ORDER BY center_name ASC");

                    for (Iterator subCenterItr = subCenterSQL.list().iterator(); subCenterItr.hasNext();) {

                        subCenterObj = (Object[]) subCenterItr.next();
                        subCenterId = subCenterObj[0].toString().trim();
                        subCenterName = subCenterObj[1].toString();

                        subCenterResponseObj = new JSONObject();

                        subCenterResponseObj.put("subCenterId", subCenterId);
                        subCenterResponseObj.put("subCenterName", subCenterName);

                        subCenterResponseObjArr.add(subCenterResponseObj);

                    }

                    centerResponseObj.put("subCenter", subCenterResponseObjArr);
                    subCenterResponseObjArr = new JSONArray();

                    centerResponseObjArr.add(centerResponseObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", centerResponseObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", centerResponseObjArr);

            }
        } catch (Exception e) {

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", centerResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>