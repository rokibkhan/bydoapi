<%-- 
    Document   : groupSMSTotalMemberInfo
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String messageBody = "";
    String messageBodyCount = "";

    String smsTotalMember = "";
    String smsTotalCost = "";
    String smsPerRate = "";

    String mTypeId = "";
    String mDivisionId = "";
    String mCenterId = "";
    String mSubCenterId = "";
    String mUniversityId = "";

    String adddate = "";
    String paymentDate = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    getRegistryID getId = new getRegistryID();

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: groupSMSRequestList rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject totalMemberCountObj = new JSONObject();
    JSONArray totalMemberCountObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String totalPhoneNumberCount = "";
    String perSMSCost = "";
    String totalCost = "";

    double totalCostAmount = 0.0d;

    // JSONArray requestResponseObjArr = new JSONArray();
    JSONArray requestResponseArr = new JSONArray();
    JSONObject requestResponseObj = new JSONObject();

    JSONArray paymentTransaResponseArr = new JSONArray();
    JSONObject paymentTransaResponseObj = new JSONObject();

    String memberTypeId = "";
    String memberTypeName = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    String divisionId = "";
    String divisionShortName = "";
    String divisionLongName = "";

    JSONArray divisionResponseObjArr = new JSONArray();
    JSONObject divisionResponseObj = new JSONObject();

    String centerId = "";
    String centerName = "";

    JSONArray centerResponseObjArr = new JSONArray();
    JSONObject centerResponseObj = new JSONObject();

    String subCenterId = "";
    String subCenterName = "";

    JSONArray subCenterResponseObjArr = new JSONArray();
    JSONObject subCenterResponseObj = new JSONObject();

    String universityId = "";
    String universityShortName = "";
    String universityLongName = "";

    JSONArray universityResponseObjArr = new JSONArray();
    JSONObject universityResponseObj = new JSONObject();

    Query smsRequestAddSQL = null;
    Query smsPaymentFeeAddSQL = null;

    String transactionId = "";
    String transactionDate = "";
    String transactionStatus = "";
    String transactionStatusText = "";
    String transactionStatusText1 = "";
    String totalSMSCost = "";
    String referenceNo = "";
    String referenceDesc = "";
    String transactionType = "";
    String paidDate = "";
    String dueDate = "";
    String feeYear = "";
    String paymentOption = "";

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //   logger.info("API :: groupSMSRequestList API OK");
                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
                        if (!filter.equals("")) {
                            filterSQLStr = " AND mt.member_name LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM post_request pn "
//                                + "WHERE pn.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pn.id_request_content DESC";
                        pContentCountSQL = "SELECT count(*) FROM group_sms_request WHERE MEMBER_ID ='" + memId + "' "
                                + " ORDER BY REQUEST_ID DESC ";

                        //    logger.info("API :: groupSMSRequestList API OK :: pContentCountSQL ::" + pContentCountSQL);
                        String numrow1 = dbsession.createSQLQuery(pContentCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pContentCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        logger.info("API :: groupSMSRequestList API OK :: numrows ::" + numrows);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        logger.info("API :: groupSMSRequestList API::  currentpage : " + currentpage + " offset : " + offset + "");

                        pContentSQL = "SELECT * FROM group_sms_request WHERE MEMBER_ID ='" + memId + "' "
                                + "ORDER BY REQUEST_ID DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        logger.info("API :: groupSMSRequestList API pContentSQL ::" + pContentSQL);

                        pContentSQLQry = dbsession.createSQLQuery(pContentSQL);

                        logger.info("API :: groupSMSRequestList API pContentSQLQry ::" + pContentSQLQry);

                        for (Iterator pContentItr = pContentSQLQry.list().iterator(); pContentItr.hasNext();) {

                            logger.info("API :: groupSMSRequestList API transactionId ::");

                            pContentObj = (Object[]) pContentItr.next();
                            transactionId = pContentObj[0].toString();                            
                            
                            logger.info("API :: groupSMSRequestList API transactionId ::" + transactionId);

                            smsTotalMember = pContentObj[7] == null ? "" : pContentObj[7].toString();
                            messageBody = pContentObj[8] == null ? "" : pContentObj[8].toString();
                            messageBodyCount = pContentObj[9] == null ? "" : pContentObj[9].toString();
                            smsPerRate = pContentObj[10] == null ? "" : pContentObj[10].toString();

                            totalSMSCost = pContentObj[11] == null ? "0" : pContentObj[11].toString();

                            referenceNo = pContentObj[15] == null ? "" : pContentObj[15].toString();
                            referenceDesc = pContentObj[16] == null ? "" : pContentObj[16].toString();

                            transactionDate = pContentObj[17] == null ? "" : pContentObj[17].toString();
                            paidDate = pContentObj[18] == null ? "" : pContentObj[18].toString();
                            paymentOption = pContentObj[19] == null ? "" : pContentObj[19].toString();
                            transactionStatus = pContentObj[20] == null ? "" : pContentObj[20].toString();

                            if (transactionStatus.equals("0")) {
                                transactionStatusText = "Due";
                                transactionStatusText1 = "Created and Waiting for Payment";
                            }
                            if (transactionStatus.equals("1")) {
                                transactionStatusText = "Paid";
                                transactionStatusText1 = "Payment Complete and Waiting for Approval";
                            }
                            if (transactionStatus.equals("2")) {
                                transactionStatusText = "Approved";
                                transactionStatusText1 = "Approved and Waiting for Process Start";
                            }

                            if (transactionStatus.equals("3")) {
                                transactionStatusText = "Processing";
                                transactionStatusText1 = "Processing";
                            }
                            if (transactionStatus.equals("4")) {
                                transactionStatusText = "Partially Completed";
                                transactionStatusText1 = "Partially Completed";
                            }

                            if (transactionStatus.equals("5")) {
                                transactionStatusText = "Completed";
                                transactionStatusText1 = "Processing Completed";
                            }

                            if (transactionStatus.equals("6")) {
                                transactionStatusText = "Fail";
                                transactionStatusText1 = "Payment Fail";
                            }

                            requestResponseObj = new JSONObject();
                            requestResponseObj.put("RequestId", transactionId);
                            requestResponseObj.put("RequestStatus", transactionStatus);
                            requestResponseObj.put("RequestStatusText", transactionStatusText);
                            requestResponseObj.put("RequestStatusText1", transactionStatusText1);
                            requestResponseObj.put("RequestTransactionDate", transactionDate);
                            requestResponseObj.put("RequestTransactionPaidDate", paidDate);
                            requestResponseObj.put("TotalSMSCount", smsTotalMember);
                            requestResponseObj.put("TotalSMSCost", totalSMSCost);
                            requestResponseObj.put("PerSMSCost", smsPerRate);
                            requestResponseObj.put("MessageBody", messageBody);
                            requestResponseObj.put("MessageBodyCount", messageBodyCount);
                            requestResponseObj.put("RequestPayOption", paymentOption);
                            requestResponseArr.add(requestResponseObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", requestResponseArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
