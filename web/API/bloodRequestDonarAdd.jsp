<%-- 
    Document   : bloodRequestDonarAdd
    Created on : APR 29, 2020, 10:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("bloodRequestDonarAdd_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();
    getRegistryID getId = new getRegistryID();

    String key = "";
    String memberId = "";
    String givenPassword = "";

    String bloodRequestId = "";
    String bloodRequestDonarId = "";
    String bloodReqDate = "";

    String adddate = "";
    String paymentDate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("bloodRequestId") && request.getParameterMap().containsKey("bloodRequestDonarId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        bloodRequestId = request.getParameter("bloodRequestId").trim();
        bloodRequestDonarId = request.getParameter("bloodRequestDonarId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: bloodRequestDonarAdd rec ::" + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String memberXId = "";
    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";

    Query bloodDonarAddSQL = null;

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {

                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        //check requestId and donar already exits
                        pContentCountSQL = "SELECT  count(*) FROM blood_donate "
                                + "WHERE blood_donar_id ='" + bloodRequestDonarId + "' AND blood_request_id ='" + bloodRequestId + "'";

                        String numrow = dbsession.createSQLQuery(pContentCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pContentCountSQL).uniqueResult().toString();
                        //  int numrows = Integer.parseInt(numrow1);

                        if (numrow.equals("0")) {

                            InetAddress ip;
                            String hostname = "";
                            String ipAddress = "";
                            try {
                                ip = InetAddress.getLocalHost();
                                hostname = ip.getHostName();
                                ipAddress = ip.getHostAddress();

                            } catch (UnknownHostException e) {

                                e.printStackTrace();
                            }

                            adddate = dateFormatX.format(dateToday);
                            paymentDate = dateFormatYmd.format(dateToday);

                            bloodReqDate = adddate;

                            String adduser = Integer.toString(memId);
                            String published = "1";

                            String idS = getId.getID(61);
                            int bloodDonateId = Integer.parseInt(idS);

                            //insert group_sms_request
                            bloodDonarAddSQL = dbsession.createSQLQuery("INSERT INTO blood_donate("
                                    + "id_blood_donate,"
                                    + "blood_donar_id,"
                                    + "blood_request_id,"
                                    + "blood_donate_date,"
                                    + "published,"
                                    + "add_date,"
                                    + "add_user,"
                                    + "add_term,"
                                    + "add_ip"
                                    + ") VALUES("
                                    + "'" + bloodDonateId + "',"
                                    + "'" + bloodRequestDonarId + "',"
                                    + "'" + bloodRequestId + "',"
                                    + "'" + bloodReqDate + "',"
                                    + "'" + published + "',"
                                    + "'" + adddate + "',"
                                    + "'" + adduser + "',"
                                    + "'" + hostname + "',"
                                    + "'" + ipAddress + "'"
                                    + "  ) ");

                            bloodDonarAddSQL.executeUpdate();

                            //update request status
                            Query approveRequestSQL = dbsession.createSQLQuery("UPDATE  blood_content SET published='3',mod_date='" + adddate + "' WHERE id_blood_content='" + bloodRequestId + "'");
                            approveRequestSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                //update request status
                                //      $udata = array('published' => '3');
                                //     $updateInfo = $dataList->updateDataInfo('blood_content', 'id_blood_content', $bloodReqID, $udata);
                                //update user blood count for bloodCount field
                                //    $oldBloodCounter = $dataList->showTableOneFieldById('user_info', 'user_id', $donarID, 'user_blood_donation_point');
                                //    $newBloodCounter = (1 + $oldBloodCounter);
                                //   $udataXbdcount = array('user_blood_donation_point' => $newBloodCounter);
                                //    $updateInfoXbdcount = $dataList->updateDataInfo('user_info', 'user_id', $donarID, $udataXbdcount);
                                postContentObj = new JSONObject();
                                postContentObj.put("BloodDonateId", bloodDonateId);
                                postContentObj.put("BloodRequestDonarId", bloodRequestDonarId);
                                postContentObj.put("BloodRequestId", bloodRequestId);
                                postContentObj.put("BloodDonateDate", bloodReqDate);

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Found");
                                logingObj.put("ResponseData", postContentObj);
                            } else {
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "Error!!! When system working");
                                logingObj.put("ResponseData", postContentObj);
                            }

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "Already add this donar for this blood request");
                            logingObj.put("ResponseData", postContentObj);

                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//             dbtrx.commit();
//       }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
