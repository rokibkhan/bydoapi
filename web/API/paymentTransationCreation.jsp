<%-- 
    Document   : paymentTransationCreation
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    getRegistryID getId = new getRegistryID();
    String idS = getId.getID(23);
    int feeTransID = Integer.parseInt(idS);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    String memberNID = "";
    String paymentDate = "";
    String paymentFeeYear = "1";
    String renewalTypeId = "1";

    String memberTypeID = "";

    String annualFeeAmount = "0.00";
    String feeAmount = "";
    String paymentRefNo = "";
    String paymentRefDescription = "";

    //  double value = Double.valueOf(str);
    //double feeAmount = 0.0d;

    String adddate = "";

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        //paymentFeeYear = request.getParameter("paymentFeeYear");
        //renewalTypeId = request.getParameter("renewalTypeId").trim();

        //memberTypeID = request.getParameter("memberTypeId").trim();

        //annualFeeAmount = request.getParameter("annualFeeAmount").trim();
        feeAmount = request.getParameter("feeAmount").trim();
        //  paymentRefNo = request.getParameter("paymentRefNo") == null ? "" : request.getParameter("paymentRefNo").trim();
        //   paymentRefDescription = request.getParameter("paymentRefDescription") == null ? "" : request.getParameter("paymentRefDescription").trim();

        //  double value = Double.valueOf(str);
        //   feeAmount = Double.valueOf(renewalTypeId) * Double.valueOf(annualFeeAmount);
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    
    
    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("memberPaymentInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject transactionCreationObj = new JSONObject();
    JSONArray transactionCreationArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    Query feeCheckSQL = null;
    Query memberFeeAddSQL = null;

    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String transactionId = "";
    String transactionDate = "";
    // String transactionStatus = "0"; //Due
    //  String transactionStatus = "1"; //paid
    String transactionStatus = "2"; //fail
    String transactionStatusText = "Due";
    //  String billType = "Renewal Fee";
    String billType = "1"; //  1=Renewal,5=Unknown, 3=Life, 4=Above65,2=Registration                          
    String payOption = "1"; //1=online

    String billAmount = "";
    String referenceNo = "";
    String referenceDesc = "";
    String transactionType = "";
    String paidDate = "";
    String dueDate = "";
    String feeYear = "";
    String paymentOption = "";

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where id = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String totalPaymentDue = "0";
                        //String dueSumSQL = "select sum(amount) from member_fee  WHERE  member_id=" + memId + " AND status ='0'";

                        //totalPaymentDue = dbsession.createSQLQuery(dueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(dueSumSQL).uniqueResult().toString();
                        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

                        logger.info("Member Deatils :: totalPaymentDue :: " + totalPaymentDue);

                        //check total due
                        //if (totalPaymentDue.equals("0")) {

                        //    feeCheckSQL = dbsession.createSQLQuery("SELECT * FROM member_fee WHERE member_id = '" + memberNID + "' AND fee_year = '" + paymentFeeYear + "' AND status = 1");

                         //   logger.info("feeCheckSQL ::" + feeCheckSQL);

                           // if (feeCheckSQL.list().isEmpty()) {

                                adddate = dateFormatX.format(dateToday);
                                paymentDate = dateFormatYmd.format(dateToday);

                                String adduser = Integer.toString(memId);

                                // String txn_date = paymentDate;                            
                                String txn_date = adddate;

                                logger.info("memId ::" + memId);
                                logger.info("paymentDate :: " + adddate);
                                //logger.info("paymentFeeYear ::" + paymentFeeYear);
                                //logger.info("memberRenewalTypeId ::" + renewalTypeId);
                                //logger.info("memberTypeID ::" + memberTypeID);
                                //logger.info("annualFeeAmount ::" + annualFeeAmount);
                                logger.info("feeAmount ::" + feeAmount);

                                memberFeeAddSQL = dbsession.createSQLQuery("INSERT INTO member_fee("
                                        + "txn_id,"
                                        + "member_id,"
                                        + "ref_no,"
                                        + "ref_description,"
                                        + "txn_date,"
                                        + "paid_date,"
                                        + "fee_year,"
                                        + "amount,"
                                        + "status,"
                                        + "due_date,"
                                        + "bill_type,"
                                        + "pay_option ,"
                                        + "add_user,"
                                        + "add_date,"
                                        + "mod_user,"
                                        + "mod_date"
                                        + ") VALUES("
                                        + "'" + feeTransID + "',"
                                        + "'" + memId + "',"
                                        + "'" + paymentRefNo + "',"
                                        + "'" + paymentRefDescription + "',"
                                        + "'" + txn_date + "',"
                                        + "'" + paymentDate + "',"
                                        + "'" + paymentFeeYear + "',"
                                        + "'" + feeAmount + "',"
                                        + "'" + transactionStatus + "',"
                                        + "'" + paymentDate + "',"
                                        + "'" + billType + "',"
                                        + "'" + payOption + "',"
                                        + "'" + adduser + "',"
                                        + "'" + adddate + "',"
                                        + "'" + adduser + "',"
                                        + "'" + adddate + "'"
                                        + "  ) ");

                                logger.info("memberFeeAddSQL ::" + memberFeeAddSQL);

                                memberFeeAddSQL.executeUpdate();

                                transactionCreationObj = new JSONObject();
                                transactionCreationObj.put("TransactionId", feeTransID);
                                transactionCreationObj.put("BillAmount", feeAmount);
                                transactionCreationObj.put("TransactionStatus", transactionStatus);
                                //transactionCreationObj.put("TransactionStatusText", transactionStatusText);
                                transactionCreationObj.put("TransactionDate", txn_date);
                                transactionCreationObj.put("TransactionType", billType);
                                //transactionCreationObj.put("FeeYear", paymentFeeYear);

                                transactionCreationArr.add(transactionCreationObj);

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Found");
                                logingObj.put("ResponseData", transactionCreationArr);

                            /*} else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "Error!!! Trnasaction info already exits");
                                logingObj.put("ResponseData", transactionCreationArr);
                            }

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "Error!!! Please pay your total due TK" + totalPaymentDue + " in payment history then renew membership");
                            logingObj.put("ResponseData", transactionCreationArr);
                        }*/

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
