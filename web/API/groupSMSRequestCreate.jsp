<%-- 
    Document   : groupSMSTotalMemberInfo
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String messageBody = "";
    String messageBodyCount = "";

    String smsTotalMember = "";
    String smsTotalCost = "";
    String smsPerRate = "";

    String mTypeId = "";
    String mDivisionId = "";
    String mCenterId = "";
    String mSubCenterId = "";
    String mUniversityId = "";

    String adddate = "";
    String paymentDate = "";

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    getRegistryID getId = new getRegistryID();

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("messageBody") && request.getParameterMap().containsKey("messageBodyCount") && request.getParameterMap().containsKey("smsTotalMember") && request.getParameterMap().containsKey("smsPerRate")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        messageBody = request.getParameter("messageBody").trim();
        messageBodyCount = request.getParameter("messageBodyCount").trim();

        smsTotalMember = request.getParameter("smsTotalMember").trim();
        smsTotalCost = request.getParameter("smsTotalCost").trim();
        smsPerRate = request.getParameter("smsPerRate").trim();

        mTypeId = request.getParameter("mTypeId") == null ? "" : request.getParameter("mTypeId").trim();
        mDivisionId = request.getParameter("mDivisionId") == null ? "" : request.getParameter("mDivisionId").trim();
        mCenterId = request.getParameter("mCenterId") == null ? "" : request.getParameter("mCenterId").trim();
        mSubCenterId = request.getParameter("mSubCenterId") == null ? "" : request.getParameter("mSubCenterId").trim();
        mUniversityId = request.getParameter("mUniversityId") == null ? "" : request.getParameter("mUniversityId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: groupSMSRateInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject totalMemberCountObj = new JSONObject();
    JSONArray totalMemberCountObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String totalPhoneNumberCount = "";
    String perSMSCost = "";
    String totalCost = "";

    double totalCostAmount = 0.0d;

    // JSONArray requestResponseObjArr = new JSONArray();
    JSONArray requestResponseArr = new JSONArray();
    JSONObject requestResponseObj = new JSONObject();

    JSONArray paymentTransaResponseArr = new JSONArray();
    JSONObject paymentTransaResponseObj = new JSONObject();

    String memberTypeId = "";
    String memberTypeName = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    String divisionId = "";
    String divisionShortName = "";
    String divisionLongName = "";

    JSONArray divisionResponseObjArr = new JSONArray();
    JSONObject divisionResponseObj = new JSONObject();

    String centerId = "";
    String centerName = "";

    JSONArray centerResponseObjArr = new JSONArray();
    JSONObject centerResponseObj = new JSONObject();

    String subCenterId = "";
    String subCenterName = "";

    JSONArray subCenterResponseObjArr = new JSONArray();
    JSONObject subCenterResponseObj = new JSONObject();

    String universityId = "";
    String universityShortName = "";
    String universityLongName = "";

    JSONArray universityResponseObjArr = new JSONArray();
    JSONObject universityResponseObj = new JSONObject();

    Query smsRequestAddSQL = null;
    Query smsPaymentFeeAddSQL = null;

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        adddate = dateFormatX.format(dateToday);
                        paymentDate = dateFormatYmd.format(dateToday);

                        String adduser = Integer.toString(memId);

                        String billType = "Renewal Fee";

                        String requestStatus = "0";

                        // String txn_date = paymentDate;                            
                        String txn_date = adddate;
                        String payOption = "1";

                        String idS = getId.getID(23);
                        int smsRequestTransId = Integer.parseInt(idS);

                        logger.info("API :: smsRequestTransId  :: " + smsRequestTransId);

                        //insert group_sms_request
                        smsRequestAddSQL = dbsession.createSQLQuery("INSERT INTO group_sms_request("
                                + "REQUEST_ID,"
                                + "MEMBER_ID,"
                                + "M_TYPE_ID,"
                                + "M_DIVISION_ID,"
                                + "M_CENTER_ID,"
                                + "M_SUB_CENTER_ID,"
                                + "UNIVERSITY_ID,"
                                + "TOTAL_SMS_COUNT,"
                                + "SMS_BODY,"
                                + "SMS_BODY_COUNT,"
                                + "PER_SMS_COST,"
                                + "TOTAL_SMS_COST,"
                                + "TXN_DATE,"
                                + "PAID_DATE,"
                                + "PAY_OPTION,"
                                + "STATUS,"
                                + "ADD_DATE,"
                                + "ADD_USER,"
                                + "ADD_TERM,"
                                + "ADD_IP"
                                + ") VALUES("
                                + "'" + smsRequestTransId + "',"
                                + "'" + memId + "',"
                                + "'" + mTypeId + "',"
                                + "'" + mDivisionId + "',"
                                + "'" + mCenterId + "',"
                                + "'" + mSubCenterId + "',"
                                + "'" + mUniversityId + "',"
                                + "'" + smsTotalMember + "',"
                                + "'" + messageBody + "',"
                                + "'" + messageBodyCount + "',"
                                + "'" + smsPerRate + "',"
                                + "'" + smsTotalCost + "',"
                                + "'" + txn_date + "',"
                                + "'" + txn_date + "',"
                                + "'" + payOption + "',"
                                + "'" + requestStatus + "',"
                                + "'" + adddate + "',"
                                + "'" + adduser + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("API :: smsRequestAddSQL ::" + smsRequestAddSQL);

                        smsRequestAddSQL.executeUpdate();

                        requestResponseObj = new JSONObject();
                        requestResponseObj.put("RequestId", smsRequestTransId);
                        requestResponseObj.put("RequestStatus", requestStatus);
                        requestResponseObj.put("RequestDate", txn_date);
                        requestResponseObj.put("RequestPaidDate", txn_date);
                        requestResponseObj.put("RequestPayOption", payOption);
                        requestResponseObj.put("TotalSMSCount", smsTotalMember);
                        requestResponseObj.put("TotalSMSCost", smsTotalCost);
                        requestResponseObj.put("PerSMSCost", smsPerRate);
                        requestResponseObj.put("MessageBody", messageBody);
                        requestResponseObj.put("MessageBodyCount", messageBodyCount);
                        //    requestResponseArr.add(requestResponseObj);

                        memberTypeResponseObj = new JSONObject();
                        memberTypeResponseObj.put("MemberTypeId", memberTypeId);
                        memberTypeResponseObj.put("MemberTypeName", memberTypeName);
                        //   memberTypeResponseObjArr.add(memberTypeResponseObj);

                        divisionResponseObj = new JSONObject();
                        divisionResponseObj.put("DivisionId", divisionId);
                        divisionResponseObj.put("DivisionShortName", divisionShortName);
                        divisionResponseObj.put("DivisionLongName", divisionLongName);
                        //  divisionResponseObjArr.add(divisionResponseObj);

                        centerResponseObj = new JSONObject();
                        centerResponseObj.put("CenterId", centerId);
                        centerResponseObj.put("CenterName", centerName);
                        //   centerResponseObjArr.add(centerResponseObj);

                        subCenterResponseObj = new JSONObject();
                        subCenterResponseObj.put("subCenterId", subCenterId);
                        subCenterResponseObj.put("subCenterName", subCenterName);
                        //  subCenterResponseObjArr.add(subCenterResponseObj);

                        universityResponseObj = new JSONObject();
                        universityResponseObj.put("UniversityId", universityId);
                        universityResponseObj.put("UniversityShortName", universityShortName);
                        universityResponseObj.put("UniversityLongName", universityLongName);
                        //  universityResponseObjArr.add(universityResponseObj);

                        totalMemberCountObj = new JSONObject();
                        totalMemberCountObj.put("MemberType", memberTypeResponseObj);
                        totalMemberCountObj.put("Division", divisionResponseObj);
                        totalMemberCountObj.put("Center", centerResponseObj);
                        totalMemberCountObj.put("SubCenter", subCenterResponseObj);
                        totalMemberCountObj.put("University", universityResponseObj);
                        totalMemberCountObj.put("RequestInfo", requestResponseObj);

                        totalMemberCountObjArr.add(totalMemberCountObj);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", totalMemberCountObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
