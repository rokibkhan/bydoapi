/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.util.Random;

/**
 *
 * @author Akter
 */
public class RandomString {

    /**
     * generate string consists of random characters.
     *
     * @param length length of generated string
     * @return string
     */
    public static String randomString(int length, int type) {
        String base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabsdefghijklmnopqrstuvwxyz";
        
        
        if (type == 1) {
            base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabsdefghijklmnopqrstuvwxyz";
        } else if (type == 2) {
            base = "0123456789";
        }

        StringBuilder b = new StringBuilder();
        for (int i = 0; i < length; i++) {
            b.append(base.charAt(random.nextInt(base.length())));
        }
        return b.toString();
    }

    private static final Random random = new Random();

//    public static void main(String args[]) {
//        int length = 4; 
//        int type = 2; // type 1=alphanumeric 2= numeric 0-9
//        System.out.println(randomString(length, type));
//    }
}
