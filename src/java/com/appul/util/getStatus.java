/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyStatus;
import java.util.*;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class getStatus {

    Session dbsession = null;
    org.hibernate.Transaction dbtrx = null;

    public String getStatusName(String status) {
         dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyStatus where status='" + status + "'");
        Iterator itr = q1.list().iterator();


        String statusName = "";

        if (itr.hasNext()) {
            SyStatus stat = (SyStatus) itr.next();
            statusName = stat.getStatusDesc();
        } else {
            statusName = "";
        }
        dbtrx.commit();
        dbsession.close();
        return statusName;
    }
    
    public String getStatusCode(String statusName) {
        dbsession = HibernateUtil.getSessionFactory().openSession();
        dbtrx = dbsession.beginTransaction();
        Query q1 = dbsession.createQuery("from SyStatus where statusDesc='" + statusName + "'");
        Iterator itr = q1.list().iterator();


        String statusCode = "";

        if (itr.hasNext()) {
            SyStatus stat = (SyStatus) itr.next();
            statusCode = stat.getStatusDesc();
        } else {
            statusCode = "";
        }
        dbtrx.commit();
        dbsession.close();
        return statusCode;
    }

}
