/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.util.HibernateUtil;
import com.appul.entity.SyRegister;
import java.lang.*;
import java.util.*;
import org.hibernate.*;


/**
 *
 * @author Akter
 */
public class getRegistryID {

    public String getID(int regCode) {
        int regLen = 0;
        String regLpad = "";
        String regLpadChar = "";
        String regPfx = "";
        String regSfx = "";
        long regNextVal = 0;
        String regID="";

        Session dbsession = null;
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        Query q1 = null;
        SyRegister syregister = null;
        q1 = dbsession.createQuery("from SyRegister register where regCode=" + regCode);
        Iterator itr1 = q1.list().iterator();

        if (itr1.hasNext()){
            syregister = (SyRegister) itr1.next();
            regLen = syregister.getRegLen();

            if (syregister.getRegLpad()==null){
                regLpad ="";
            } else{
                regLpad =syregister.getRegLpad();
            }

            if (syregister.getRegLpadChar()==null){
                regLpadChar ="";
            } else{
                regLpadChar = syregister.getRegLpadChar();
            }

            if (syregister.getRegPfx()==null){
                regPfx ="";
            } else{
                regPfx = syregister.getRegPfx();
            }

            if (syregister.getRegSfx()==null){
                regSfx ="";
            } else{
                regSfx = syregister.getRegSfx();
            }
                        
            regNextVal = syregister.getRegNextval();



            int lenNextVal = Long.toString(regNextVal).length();
            int lenLpad = regLen - regPfx.length() - regSfx.length() - lenNextVal;
            String nextStr=Long.toString(regNextVal);

            if(regLpad.equals("Y") && regLpadChar!=null && regLpadChar.equals("")==false){
                for(int i=0;i<lenLpad;i++){
                    nextStr=regLpadChar+nextStr;
                }
                regID=regPfx+nextStr+regSfx;
            } else {
                regID=regPfx+Long.toString(regNextVal)+regSfx;
            }
            dbsession.createQuery("update SyRegister syreg set syreg.regNextval=syreg.regNextval+1 where regCode=" + regCode).executeUpdate();
            dbtrx.commit();
           // dbsession.close();
        }
        dbsession.close();
        return regID;
    }
}
