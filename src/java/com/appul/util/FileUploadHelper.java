/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.entity.SendsmsSetting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import javax.servlet.http.Part;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author HP
 */
public class FileUploadHelper {
    
    public static String getFileName(final Part part) 
	{
	    final String partHeader = part.getHeader("content-disposition");
	    System.out.println("Part Header = {0}" +  partHeader);
	    for (String content : part.getHeader("content-disposition").split(";")) {
	        if (content.trim().startsWith("filename")) {
	            return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	        }
	    }
	    return null;
	}
    
    public static void uploadFile(Part filePart, String fileName, String path)
		{

		 System.out.println("fileNamefileNamefileNamefileName "+fileName+"      "+path);
		    OutputStream out = null;
		    InputStream filecontent = null;    
		    File dir=new File(path);
		    
		    if(!dir.exists())
	        {
		    	dir.mkdir();
		    	System.out.println("created directory " + path);
	        }

		    try 
			{
		        out = new FileOutputStream(new File(path + File.separator
		                + fileName));
		        filecontent = filePart.getInputStream();

		        int read = 0;
		        final byte[] bytes = new byte[1024];
		       
		        //MediaMetadataRetriever 
		        while ((read = filecontent.read(bytes)) != -1) 
				{
		            out.write(bytes, 0, read);
		        }
		      //  out.
		        System.out.println("New file " + fileName + " created at " + path);

		    }
			catch (IOException fne) 
			{
		    	System.out.println("You either did not specify a file to upload or are "
		                + "trying to upload a file to a protected or nonexistent "
		                + "location.");
		    	System.out.println("ERROR: " + fne.getMessage());
		    }
			finally 
			{
		        if (out != null) 
				{
		            try 
					{
						out.close();
					}
					catch (IOException e) 
					{
						e.printStackTrace();
					}
		        }
		        if (filecontent != null) 
				{
		            try 
					{
						filecontent.close();
					}
					catch (IOException e) 
					{
						e.printStackTrace();
					}
		        }	       
		    }		  
		}

}
