package com.appul.util;


import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DayDifference {

    public static void main(String args[]) {
        String datestart = "2016-04-25";
        String dateend = "2016-11-22";
        int diff = 0;

        try {

            //System.out.println("Adding Date");
            diff = getDateDiff(datestart, dateend);
            System.out.println(diff);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getDateDiff(String datestart, String dateend) throws Exception {

        //Total time for one day
        int one_day = 1000 * 60 * 60 * 24;
        Date dtstart = getFormatedDate(datestart);
        Date dtend = getFormatedDate(dateend);
        int diff = 0;

        System.out.println("dtstart "+dtstart);
        System.out.println("dtend   "+dtend);
        
        diff = (int) Math.ceil((dtend.getTime() - dtstart.getTime()) / (one_day));

        return diff;
    }

    public static Date getFormatedDate(String dateIn) throws Exception {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateOut = dateFormat.parse(dateIn);
        return dateOut;
    }
}
