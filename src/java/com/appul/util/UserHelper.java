/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import com.appul.entity.SyUser;
import java.sql.SQLException;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class UserHelper {

    Session dbsession = null;

 

    public String getUserPass(String UserID) throws SQLException {
        SyUser user = null;
        String password = null;
        

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx = dbsession.beginTransaction();

            Query q = dbsession.createQuery("from SyUser as syuser where syuser.userId = '" + UserID + "'");
            user = (SyUser) q.uniqueResult();
            password = user.getUserPassword();
            tx.commit();

        } catch (Exception e) {
            System.out.println("Could not resolve User ID: " + UserID);
            password = "123";

        }
        dbsession.flush();
        dbsession.close();
        return password;
    }

    public int setUserPass(String UserID, String NewPass) {

        int rec = -1;
        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction tx = null;
        try {
            tx = dbsession.beginTransaction();
            SyUser syuser = (SyUser) dbsession.get(SyUser.class, UserID);
            syuser.setUserPassword(NewPass);
            dbsession.flush();
            tx.commit();

        } catch (HibernateException e) {
            tx.rollback();
            e.printStackTrace();
        } finally {
            dbsession.close();
        }
        return rec;
        //return "test";
    }

}
