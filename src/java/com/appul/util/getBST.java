/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

import java.lang.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 *
 * @author Akter
 */
public class getBST {

    public static void main(String args) {

        try {
            System.out.println(getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDateTime() {
        String dateTime = null;

        // get the supported ids for GMT+06:00 (Bangladesh Standard Time)
        String[] ids = TimeZone.getAvailableIDs(-6 * 60 * 60 * 1000);

        if (ids.length == 0) {
            System.exit(0);
        }

        // begin output
        //System.out.println("Current Time");

        // create a Bangladesh Standard Time time zone
        SimpleTimeZone bst = new SimpleTimeZone(-6 * 60 * 60 * 1000, ids[0]);


        // set up rules for daylight savings time
        /*
        bst.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
        bst.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
         */

        // create a GregorianCalendar with the Bangladesh Daylight time zone
        // and the current date and time
        Calendar calendar = new GregorianCalendar(bst);
        //Date trialTime = new Date();
        //calendar.setTime(trialTime);

        calendar.set(Calendar.YEAR,2010);
        calendar.set(Calendar.MONTH,10);
        calendar.set(Calendar.DAY_OF_MONTH,31);
        calendar.set(Calendar.HOUR_OF_DAY,12);
        calendar.set(Calendar.MINUTE,12);

//        calendar.set(Calendar.MILLISECOND, 0);
        //calendar.setTimeZone(TimeZone.getTimeZone(System.getProperty( "user.timezone" )));

        //calendar.setTimeZone("Asia/Dhaka");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //Date date = new Date();
        //return dateFormat.format(date);

        //dateTime = Integer.toString(calendar.get(Calendar.DATE));
        //dateTime = calendar.getTimeZone().toString();
        dateTime = dateFormat.format(calendar.getTime());
        return dateTime;
    }
}
