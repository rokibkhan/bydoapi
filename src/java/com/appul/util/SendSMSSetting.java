/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;
import com.appul.entity.SendsmsSetting;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;


/**
 *
 * @author rokib
 */
public class SendSMSSetting {
     public static SendsmsSetting getSendsmsSetting( ) {
      
         SendsmsSetting ssmsset = new SendsmsSetting();
         Session dbsession = HibernateUtil.getSessionFactory().openSession();
         
         
        try {
         
            Query SQL = dbsession.createSQLQuery("SELECT sms_code,sendurlhost,sendurlapi, apikey, type, senderid"
                                            + " FROM  sendsms_setting where status = 1 ");
         
         Object[] obj = null;
         
         for (Iterator it = SQL.list().iterator(); it.hasNext();) {
         
            obj = (Object[]) it.next();
            ssmsset.setSmsCode(Integer.parseInt(obj[0].toString()));
            ssmsset.setSendurlhost(obj[1].toString());
            ssmsset.setSendurlapi(obj[2].toString());
            ssmsset.setApikey(obj[3].toString());
            ssmsset.setType(obj[4].toString());
            ssmsset.setSenderid(obj[5].toString());
         
         }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return ssmsset;
    }

    
}
