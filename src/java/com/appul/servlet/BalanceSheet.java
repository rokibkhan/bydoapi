/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

/**
 *
 * @author Akter
 */
public class BalanceSheet {

    public static Logger logger = Logger.getLogger("BalanceSheet");
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    java.sql.Statement stmt = null;
    String comName = null;
    String address = null;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    String daFormate = this.df.format(new Date());
    String sDate = "";
    java.text.DecimalFormat decf = new java.text.DecimalFormat("##,##0.00");
    String trPeriod = "";
    String startDate = "";

    public int MakeBalanceSheet(String startDate) throws SQLException {
        int ret = 0;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        System.out.println(this.startDate);
        try {
          
            this.stmt = this.con.createStatement();
            int check = 0;
            int retainedCheck = 0;
            this.stmt.executeUpdate("TRUNCATE TABLE temp_balancesheet");
            String accType = "";
            String fLevel = "";
            String sLevel = "";
            String pLevel = "";
            String ckAccType = "";
            String ckFLevel = "";
            String ckSLevel = "";
            int numer = 0;
            double Total = 0.0D;
            double TotalCurAss = 0.0D;
            double TotalFixAss = 0.0D;
            double TotalAssets = 0.0D;
            double totalLiabilities = 0.0D;
            double incTotal = 0.0D;
            double AssetTotalCurent = 0.0D;
//            String accQuery = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, acc_plevel.plevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE  ((acc_chi2_type.acc_c2name = 'Current Assets') OR(acc_chi2_type.acc_c2name = 'Fixed Assets')) AND (voucher_child.voucher_date BETWEEN '" + this.startDate + "' AND  '" + eDate + "') AND(voucher_child.del_or_up = 0) GROUP BY acc_slevel.slevel_name ORDER BY acc_chi2_type.acc_c2name DESC";
            String accQuery = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE  ((acc_chi2_type.acc_c2name = 'Current Assets') OR(acc_chi2_type.acc_c2name = 'Fixed Assets')) AND (voucher_child.voucher_date <='" + startDate + "') AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ORDER BY acc_chi2_type.acc_c2name DESC";

            java.sql.ResultSet rs1 = this.stmt.executeQuery(accQuery);
            while (rs1.next()) {
                numer = rs1.getRow();
            }

            int i = 0;
            String[] accTyp = new String[numer];
            String[] fLeve = new String[numer];
            String[] sLeve = new String[numer];
            java.sql.ResultSet rs2 = this.stmt.executeQuery(accQuery);
            while (rs2.next()) {
                accTyp[i] = rs2.getString("acc_c2name");
                fLeve[i] = rs2.getString("flevel_name");
                sLeve[i] = rs2.getString("slevel_name");
                i++;
            }
            for (int a = 0; a < accTyp.length; a++) {
                accType = accTyp[a];
                fLevel = fLeve[a];
                sLevel = sLeve[a];
                String Sum = "";

                if (fLevel.equals("Fixed Assets")) {
                    if (!ckFLevel.equals(fLevel)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevel + "','','','','','','')");
                        ckFLevel = fLevel;
                    }

//                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevel + "') AND (voucher_child.voucher_date BETWEEN '" + this.startDate + "' AND  '" + eDate + "') AND(voucher_child.del_or_up = 0)" + " GROUP BY acc_slevel.slevel_name ";
                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevel + "') AND (voucher_child.voucher_date <= '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs3 = this.stmt.executeQuery(L3NameQuer);
                    while (rs3.next()) {
                        Sum = rs3.getString("amount");
                        Total = Double.parseDouble(Sum);
                        TotalCurAss += Total;
                    }

                    String dd = "";
                    if (Total < 0.0D) {
                        incTotal = Total * -1.0D;
                        dd = this.decf.format(incTotal);
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevel + "','','" + this.decf.format(incTotal) + "','','','')");
                    } else {
                        dd = this.decf.format(Total);
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevel + "','','" + this.decf.format(Total) + "','','','')");
                    }
                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalCurAss < 0.0D) {
                    AssetTotalCurent = TotalCurAss * -1.0D;
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalCurAss) + "','')");
                }

                check = 0;
            }

            for (int a = 0; a < accTyp.length; a++) {
                accType = accTyp[a];
                fLevel = fLeve[a];
                sLevel = sLeve[a];
//                pLevel = pLeve[a];
                String Sum = "";

                if (fLevel.equals("Current Assets")) {
                    if (!ckFLevel.equals(fLevel)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevel + "','','','','','','')");
                        ckFLevel = fLevel;
                    }

                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevel + "') AND (voucher_child.voucher_date <= '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs3 = this.stmt.executeQuery(L3NameQuer);
                    while (rs3.next()) {
                        Sum = rs3.getString("amount");
                        Total = Double.parseDouble(Sum);
                        TotalFixAss += Total;
                    }

                    if (Total < 0.0D) {
                        incTotal = Total * -1.0D;
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevel + "','','" + this.decf.format(incTotal) + "','','','')");
                    } else {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevel + "','','" + this.decf.format(Total) + "','','','')");
                    }
                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalFixAss < 0.0D) {
                    AssetTotalCurent = TotalFixAss * -1.0D;

                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalFixAss) + "','')");
                }

                check = 0;
            }
            TotalAssets = TotalCurAss + TotalFixAss;
            if (TotalAssets < 0.0D) {
                AssetTotalCurent = TotalAssets * -1.0D;
                this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','Total Assets','','" + this.decf.format(AssetTotalCurent) + "')");
            } else {
                this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','Total Assets','','" + this.decf.format(TotalAssets) + "')");
            }

            int numer1 = 0;
            String ckAccTypeEx = "";
            String ckFLevelEx = "";
            String ckSLevelEx = "";
            String accTypeEx = "";
            String fLevelEx = "";
            String sLevelEx = "";
//            String pLevelEx = "";
            String ckTypeEx = "";
            String ckAccGenLi = "";
            String SumEx = "";
            String accQueryLiab = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE ((acc_chi2_type.acc_c2name = 'Capital/ Equity') OR(acc_chi2_type.acc_c2name = 'Current Liabilities') OR(acc_chi2_type.acc_c2name = 'Long Term Liabilities')) AND (voucher_child.voucher_date <= '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name";

            double TotalFund = 0.0D;
            double TotalExp = 0.0D;
            double TotalCurLiab = 0.0D;
            double TotalCapital = 0.0D;
            double TotalLongLiab = 0.0D;
            double TotalReEarn = 0.0D;
            double TotalInEX = 0.0D;
            java.sql.ResultSet rs5 = this.stmt.executeQuery(accQueryLiab);

            while (rs5.next()) {
                numer1 = rs5.getRow();
            }

            int j = 0;
            String[] accTypEx = new String[numer1];
            String[] fLeveEx = new String[numer1];
            String[] sLeveEx = new String[numer1];
//            String[] pLeveEx = new String[numer1];

            java.sql.ResultSet rs6 = this.stmt.executeQuery(accQueryLiab);
            while (rs6.next()) {
                accTypEx[j] = rs6.getString("acc_c2name");
                fLeveEx[j] = rs6.getString("flevel_name");
                sLeveEx[j] = rs6.getString("slevel_name");
//                pLeveEx[j] = rs6.getString("plevel_name");
                j++;
            }
            for (int b = 0; b < accTypEx.length; b++) {
                accTypeEx = accTypEx[b];
                fLevelEx = fLeveEx[b];
                sLevelEx = sLeveEx[b];
//                pLevelEx = pLeveEx[b];

                if (fLevelEx.equals("Capital/Equity")) {
                    if (!ckFLevelEx.equals(fLevelEx)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevelEx + "','','','','','','')");
                        ckFLevelEx = fLevelEx;
                    }

                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevelEx + "') AND (voucher_child.voucher_date <=  '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs7 = this.stmt.executeQuery(L3NameQuer);

                    while (rs7.next()) {
                        SumEx = rs7.getString("amount");
                        TotalExp = Double.parseDouble(SumEx);
                        TotalCurLiab += TotalExp;
                    }
                    System.out.println(SumEx + "==" + TotalExp);
                    if (TotalExp < 0.0D) {
                        incTotal = TotalExp * -1.0D;
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(incTotal) + "','','','')");
                    } else {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(TotalExp) + "','','','')");
                    }
                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalCurLiab < 0.0D) {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalCurLiab) + "','')");
                }

                check = 0;
            }

            for (int b = 0; b < accTypEx.length; b++) {
                accTypeEx = accTypEx[b];
                fLevelEx = fLeveEx[b];
                sLevelEx = sLeveEx[b];
//                pLevelEx = pLeveEx[b];

                if (fLevelEx.equals("Fund")) {
                    if (!ckFLevelEx.equals(fLevelEx)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevelEx + "','','','','','','')");
                        ckFLevelEx = fLevelEx;
                    }

                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevelEx + "') AND (voucher_child.voucher_date <= '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs7 = this.stmt.executeQuery(L3NameQuer);

                    while (rs7.next()) {
                        SumEx = rs7.getString("amount");
                        TotalExp = Double.parseDouble(SumEx);
                        TotalFund += TotalExp;
                    }

                    incTotal = TotalExp * -1.0D;
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(incTotal) + "','','','')");

                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalFund < 0.0D) {
                    AssetTotalCurent = TotalFund * -1.0D;
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalFund) + "','')");
                }
                check = 0;
            }

            String lQuery = "SELECT SUM(voucher_child.amount) AS amount FROM    (   (   (    acc_slevel INNER JOIN acc_plevel ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE ((acc_chi2_type.acc_c2name = 'Income') OR(acc_chi2_type.acc_c2name = 'Expenditures')) AND (voucher_child.voucher_date <=  '" + startDate + "' ) AND(voucher_child.del_or_up = 0)";

            java.sql.ResultSet rsQuen = this.stmt.executeQuery(lQuery);

            String expence = "";

            while (rsQuen.next()) {

                expence = rsQuen.getString("amount");
                expence = expence == null ? "0" : expence;
                TotalExp = Double.parseDouble(expence);
//                TotalExp += TotalExp;
                TotalFund += TotalExp;

            }

            incTotal = Math.ceil(TotalExp * -1.0D);
            this.stmt.executeUpdate("insert into temp_balancesheet values('','','Retained Earnings','','" + this.decf.format(incTotal) + "','','','')");

            incTotal = Math.ceil(TotalExp * -1.0D);
            this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(incTotal) + "','')");

            for (int b = 0; b < accTypEx.length; b++) {
                accTypeEx = accTypEx[b];
                fLevelEx = fLeveEx[b];
                sLevelEx = sLeveEx[b];
//                pLevelEx = pLeveEx[b];

                if (fLevelEx.equals("Current Liabilities")) {
                    if (!ckFLevelEx.equals(fLevelEx)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevelEx + "','','','','','','')");
                        ckFLevelEx = fLevelEx;
                    }

                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevelEx + "') AND (voucher_child.voucher_date <= '" + startDate + "') AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs7 = this.stmt.executeQuery(L3NameQuer);
                    while (rs7.next()) {
                        SumEx = rs7.getString("amount");
                        TotalExp = Double.parseDouble(SumEx);
                        TotalCapital += TotalExp;
                    }
                    if (TotalExp < 0.0D) {
                        incTotal = TotalExp * -1.0D;
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(incTotal) + "','','','')");
                    } else {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(TotalExp) + "','','','')");
                    }
                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalCapital < 0.0D) {
                    AssetTotalCurent = TotalCapital * -1.0D;
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalCapital) + "','')");
                }

                check = 0;
            }
            for (int b = 0; b < accTypEx.length; b++) {
                accTypeEx = accTypEx[b];
                fLevelEx = fLeveEx[b];
                sLevelEx = sLeveEx[b];
//                pLevelEx = pLeveEx[b];

                if (fLevelEx.equals("Long Term Liabilities")) {
                    if (!ckFLevelEx.equals(fLevelEx)) {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','" + fLevelEx + "','','','','','','')");
                        ckFLevelEx = fLevelEx;
                    }

                    String L3NameQuer = "SELECT acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name, SUM(voucher_child.amount) AS amount FROM    (   (   (   acc_slevel INNER JOIN acc_plevel  ON (acc_slevel.slevel_id = acc_plevel.slevel_id)) INNER JOIN acc_flevel ON (acc_flevel.flevel_id = acc_slevel.flevel_id)) INNER JOIN acc_chi2_type ON (acc_chi2_type.acc_chi2_id = acc_flevel.acc_chi2_id)) INNER JOIN voucher_child ON (acc_plevel.plevel_id = voucher_child.plevel_id) WHERE     (acc_slevel.slevel_name = '" + sLevelEx + "') AND (voucher_child.voucher_date <= '" + startDate + "' ) AND(voucher_child.del_or_up = 0) GROUP BY acc_chi2_type.acc_c2name, acc_flevel.flevel_name, acc_slevel.slevel_name ";

                    java.sql.ResultSet rs7 = this.stmt.executeQuery(L3NameQuer);
                    while (rs7.next()) {
                        SumEx = rs7.getString("amount");
                        TotalExp = Double.parseDouble(SumEx);
                        TotalLongLiab += TotalExp;
                    }

                    if (TotalExp < 0.0D) {
                        incTotal = TotalExp * -1.0D;
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(incTotal) + "','','','')");
                    } else {
                        this.stmt.executeUpdate("insert into temp_balancesheet values('','','" + sLevelEx + "','','" + this.decf.format(TotalExp) + "','','','')");
                    }
                    check = 1;
                }
            }
            if (check == 1) {
                if (TotalLongLiab < 0.0D) {
                    AssetTotalCurent = TotalLongLiab * -1.0D;
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(AssetTotalCurent) + "','')");
                } else {
                    this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','','" + this.decf.format(TotalLongLiab) + "','')");
                }

                check = 0;
            }

            totalLiabilities = TotalCurLiab + TotalCapital + TotalLongLiab + TotalFund;
            if (totalLiabilities < 0.0D) {
                AssetTotalCurent = totalLiabilities * -1.0D;
                this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','Total Fund & Liabilities','','" + this.decf.format(AssetTotalCurent) + "')");
            } else {
                this.stmt.executeUpdate("insert into temp_balancesheet values('','','','','','Total Fund & Liabilities','','" + this.decf.format(totalLiabilities) + "')");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ret = 0;
        } finally {
            this.con.commit();
            this.con.close();
            ret = 1;
            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Balance Sheet Preparation Completed");
            System.out.println("Balance Sheet Preparation Completed");
        }

        return ret;
    }

}
