/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.NewsCategory;
import com.appul.entity.NewsInfo;
import com.appul.util.DBAccess;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "EngineeringNews", urlPatterns = {"/EngineeringNews"})
@MultipartConfig
public class EngineeringNews extends HttpServlet {

    private boolean isMultipart;
    private String filePath;

    // private int maxFileSize = 500 * 1024;
    //   private int maxMemSize = 400 * 1024;
    private int maxFileSize = 50 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 40 * 1024 * 1024;
    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(53);
        int engineeringnewsId = Integer.parseInt(idS);

        String sessionIdH = "";
        String userNameH = "";
        String strMsg = "";

        HttpSession session = request.getSession(false);
        userNameH = session.getAttribute("username").toString().toUpperCase();
        sessionIdH = session.getId();

        filePath = GlobalVariable.engineeringnewsUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {

            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
            response.sendRedirect(GlobalVariable.baseUrl + "/engineeringnewsManagement/engineeringnewsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".PDF";
            String fileNameCoverPage = filePrefixToday + "_" + rand1 + "_" + rand2 + "_cover_page.JPEG";

            int engineeringnewsOfficeId = rand1;

            String name = "";
            String value = "";
            String engineeringnewsTitle = "";
            String engineeringnewsDesc = "";
            String engineeringnewsDate = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    System.out.println("FileName:: " + fieldName);
                    if (fieldName.equalsIgnoreCase("file")) {
                        System.out.println("FileName file:: " + fieldName);
                        fileName = fi.getName();
                        contentType = fi.getContentType();
                        System.out.println("FileName contentType:: " + contentType);
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();

                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                            //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                            file = new File(filePath + fileName1);
                        } else {
                            //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                            //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                            file = new File(filePath + fileName1);
                        }
                        fi.write(file);
                        System.out.println("Uploaded Filename: " + fileName + "<br>");
                        System.out.println("Uploaded Filename: " + fileName1 + "<br>");
                    }
                    if (fieldName.equalsIgnoreCase("fileCover")) {
                        System.out.println("FileName fileCover:: " + fieldName);
                        fileName = fi.getName();
                        contentType = fi.getContentType();
                        System.out.println("FileName contentType:: " + contentType);
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();

                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                            //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                            file = new File(filePath + fileNameCoverPage);
                        } else {
                            //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                            //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                            file = new File(filePath + fileNameCoverPage);
                        }
                        fi.write(file);
                        System.out.println("Uploaded Filename Cover Page: " + fileName + "<br>");
                        System.out.println("Uploaded Filename Cover Page: " + fileNameCoverPage + "<br>");
                    }
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                    System.out.println("FileName name:: " + name);
                    System.out.println("FileName value:: " + value);
                }

                
                if (name.equalsIgnoreCase("engineeringnewsTitle")) {
                    engineeringnewsTitle = value;
                }

                if (name.equalsIgnoreCase("engineeringnewsDesc")) {
                    engineeringnewsDesc = value;
                }

               

//                if (name.equalsIgnoreCase("sticky")) {
//                    sticky = value;
//                }
            }

//            out.println("photoCaption: " + photoCaption + "<br>");
//            out.println("fileName1: " + fileName1 + "<br>");
            System.out.println("Photo userNameHAA:: " + userNameH);
            System.out.println("Photo sessionIdHAA:: " + sessionIdH);

            //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
            //if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);
            String adduser = userNameH;
            String addterm = InetAddress.getLocalHost().getHostName().toString();
            String addip = InetAddress.getLocalHost().getHostAddress().toString();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            //insert gallary
            String newsCategoryId = "1";
            String pictureName = fileName1;
            String pictureThumb = fileName1;
            String showingOrder = "1";
            String news_date_time = "";

            String fileNameUrl = GlobalVariable.engineeringnewsDirLink + fileName1;

            //   String techDistrictId = request.getParameter("techDistrictId").trim();
            Query q4 = dbsession.createSQLQuery("INSERT INTO engineeringnews_info("
                    + "id_engineeringnews,"
                    + "engineeringnewsOfficeId,"
                    + "engineeringnews_title,"
                    + "engineeringnews_desc,"
                    + "cover_page_picture,"
                    + "cover_page_thumb,"
                    + "engineeringnews_link,"
                    + "engineeringnews_date,"
                    + "add_date,"
                    + "add_user,"
                    + "add_term,"
                    + "add_ip) values( "
                    + "'" + engineeringnewsId + "',"
                    + "'" + engineeringnewsOfficeId + "',"
                    + "'" + engineeringnewsTitle + "',"
                    + "'" + engineeringnewsDesc + "',"
                    + "'" + fileNameCoverPage + "',"
                    + "'" + fileNameCoverPage + "',"
                    + "'" + fileNameUrl + "',"
                    + "'" + adddate + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + addterm + "',"
                    + "'" + addip + "')");

            System.out.println("SQL::" + q4);

            q4.executeUpdate();

            /*
            NewsInfo news = new NewsInfo();
            NewsCategory newsCat = new NewsCategory();

            news.setIdNews(newsId);
            // newsCat.setIdCat(Integer.parseInt(newsCategoryId));
            news.setNewsCategory(Integer.parseInt(newsCategoryId));
            news.setNewsTitle(newsTitle);
            news.setNewsDate(journalDate);
            news.setNewsDesc(journalDesc);
            news.setNewsShortDesc(newsShortDesc);
            news.setFeatureImage(pictureName);
            news.setShowingOrder(showingOrder);
            news.setAddDate(adddate);
            news.setAddUser(adduser);
            news.setAddTerm(addterm);
            news.setAddIp(addip);
            dbsession.save(news);
             */
            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {

                strMsg = "Engineering News Added Successfully";
                response.sendRedirect(GlobalVariable.baseUrl + "/engineeringnewsManagement/engineeringnewsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;
            } else {
                dbtrx.rollback();

                strMsg = "Error!!! When Engineering News add";
                response.sendRedirect(GlobalVariable.baseUrl + "/engineeringnewsManagement/engineeringnewsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;
            }

            //    out.println("photoCaption: " + photoCaption + "<br>");
            //    out.println("fileName1: " + fileName1 + "<br>");
        } catch (Exception ex) {
            System.out.println(ex);
        }

        strMsg = "Error!!! When adding Engineering News.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/engineeringnewsManagement/engineeringnewsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        return;
    }
}
