/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

/**
 *
 * @author Akter
 */
public class UserBean {

    private String username;
    private String password;
    private int storeId;
    public boolean valid;

    public String getPassword() {
        return password;
    }

    public void setPassword(String newPassword) {
        password = newPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUserName(String newUsername) {
        username = newUsername;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int newStoreId) {
        storeId = newStoreId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean newValid) {
        valid = newValid;
    }
}