/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.DBAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RollList", urlPatterns = {"/RollList"})
public class RollList extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            DBAccess dbconn = new DBAccess();
            Connection con = dbconn.dbsrc();
            PreparedStatement ps = con.prepareStatement("select * from image");
            ResultSet rs = ps.executeQuery();
            out.println("<h1>Roll List</h1>");
            while (rs.next()) {
                out.println("<h3>" + rs.getString("id") + "</h3>");
                out.println("<img width='300' height='300' src=DisplayImage?id=" + rs.getString("id") + "></img> <p/>");
            }
            con.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
