/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.EventCategory;
import com.appul.entity.EventInfo;
import com.appul.util.DBAccess;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "Events", urlPatterns = {"/Events"})
@MultipartConfig
public class Events extends HttpServlet {

    private boolean isMultipart;
    private String filePath;
    //  private int maxFileSize = 500 * 1024;
    //   private int maxMemSize = 400 * 1024;
    // private int maxFileSize = 500 * 1024;
    //   private int maxMemSize = 400 * 1024;

    private int maxFileSize = 5 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 4 * 1024 * 1024;
    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(36);
        int eventsId = Integer.parseInt(idS);

        String sessionIdH = "";
        String userNameH = "";
        String strMsg = "";

        String sendRedirectPage = "";

        HttpSession session = request.getSession(false);
        userNameH = session.getAttribute("username").toString().toUpperCase();
        sessionIdH = session.getId();

        filePath = GlobalVariable.imageNewsUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {

            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
            response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

            String name = "";
            String value = "";
            String eventTitle = "";
            String eventDuration = "";
            String eventVenue = "";
            String eventDate = "";
            String eventCategoryId = "";
            String eventDesc = "";
            String eventShortDesc = "";
            String sticky = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    System.out.println("FileName:: " + fieldName);
                    fileName = fi.getName();
                    contentType = fi.getContentType();
                    System.out.println("FileName contentType:: " + contentType);
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                        file = new File(filePath + fileName1);
                    } else {
                        //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        file = new File(filePath + fileName1);
                    }
                    fi.write(file);
                    System.out.println("Uploaded Filename: " + fileName + "<br>");
                    System.out.println("Uploaded Filename: " + fileName1 + "<br>");
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                    System.out.println("FileName name:: " + name);
                    System.out.println("FileName value:: " + value);
                }

                if (name.equalsIgnoreCase("eventTitle")) {
                    eventTitle = value;
                }

                if (name.equalsIgnoreCase("eventDuration")) {
                    eventDuration = value;
                }
                if (name.equalsIgnoreCase("eventVenue")) {
                    eventVenue = value;
                }
                if (name.equalsIgnoreCase("eventDate")) {
                    eventDate = value;
                }
                if (name.equalsIgnoreCase("eventCategoryId")) {
                    eventCategoryId = value;
                }
                if (name.equalsIgnoreCase("eventDesc")) {
                    eventDesc = value;
                }

                if (name.equalsIgnoreCase("eventShortDesc")) {
                    eventShortDesc = value;
                }

//                if (name.equalsIgnoreCase("sticky")) {
//                    sticky = value;
//                }
            }

//            out.println("photoCaption: " + photoCaption + "<br>");
//            out.println("fileName1: " + fileName1 + "<br>");
            System.out.println("Photo userNameHAA:: " + userNameH);
            System.out.println("Photo sessionIdHAA:: " + sessionIdH);

            //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
            //if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);

            System.out.println("adddate   adddate:: " + adddate);

            String adduser = userNameH;
            String addterm = InetAddress.getLocalHost().getHostName().toString();
            String addip = InetAddress.getLocalHost().getHostAddress().toString();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            System.out.println("entryDate   entryDate:: " + entryDate);

            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            //insert gallary
        //    String eventCategoryId = "1";
            String pictureName = fileName1;
            String pictureThumb = fileName1;
            String showingOrder = "1";
            String news_date_time = "";

            //   String techDistrictId = request.getParameter("techDistrictId").trim();
            /*
            Query q4 = dbsession.createSQLQuery("INSERT INTO event_info("
                    + "id_event,"
                    + "event_category,"
                    + "event_title,"
                    + "event_duration,"
                    + "event_venue,"
                    + "event_short_desc,"
                    + "event_desc,"
                    + "event_date_time,"
                    + "showing_order,"
                    + "feature_image,"
                    + "ADD_DATE,"
                    + "ADD_USER,"
                    + "ADD_TERM,"
                    + "ADD_IP) values( "
                    + "'" + eventsId + "',"
                    + "'" + eventCategoryId + "',"
                    + "'" + eventTitle + "',"
                    + "'" + eventDuration + "',"
                    + "'" + eventVenue + "',"
                    + "'" + eventShortDesc + "',"
                    + "'" + eventDesc + "',"
                    + "'" + eventDate + "',"
                    + "'" + showingOrder + "',"
                    + "'" + fileName1 + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + addterm + "',"
                    + "'" + addip + "')");

            q4.executeUpdate();
             */
//           Member member = new Member();
//            int memberId1 = 0;
//            int profInfoId1 = 0;
//            String memberId = session.getAttribute("memberId").toString();
//            memberId1 = Integer.parseInt(memberId);
//            profInfoId1 = Integer.parseInt(profInfoId);
//
//            member.setId(memberId1);
//            memberProfessionalInfo.setId(profInfoId1);
//            memberProfessionalInfo.setMember(member);
//            memberProfessionalInfo.setCompanyName(memProfOrgName);
//            memberProfessionalInfo.setDesignation(memProfDesignation);
//            memberProfessionalInfo.setFromDate(startDate1);
//            memberProfessionalInfo.setTillDate(endDate1);
//            memberProfessionalInfo.setCompanyType(comapyType);
//            memberProfessionalInfo.setJd(jobDescription);
//            dbsession.save(memberProfessionalInfo);
            //     String fff = dateFormatX.format(eventDate);
            EventInfo event = new EventInfo();
            EventCategory eventCat = new EventCategory();

            Date eventDate1;
            Date eventDate2;
            String eventDateX = "";
            Date eventDateXn;

            //   java.util.Date eventDate1 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(eventDate);
            //     DateFormat eventDateF = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            //     Date eventDate1 = eventDateF.parse(eventDate);
//            DateFormat dateFormatJ = new SimpleDateFormat("yyyy-MM-dd");
//            Date date = new Date();
//            String entryDate = dateFormatJ.format(date);
//ticketStartDate1 = objH[3].toString();
//        strDate = dateFormat.parse(ticketStartDate1);
            //       ticketStartDate = dateFormat1.format(strDate);
//            DateFormat dateFormatJ = new SimpleDateFormat("yyyy-MM-dd");
//            DateFormat dateFormatK = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            eventDate1 = dateFormatJ.parse(eventDate);
//            eventDateX = dateFormatK.format(eventDate1);
//            eventDateXn = dateFormatK.parse(eventDateX);
//
//            System.out.println("eventDate eventDate:: " + eventDate);
//
//            System.out.println("eventDate1 eventDate1:: " + eventDate1);
//            System.out.println("eventDateX eventDateX:: " + eventDateX);
//            System.out.println("eventDateXn eventDateXn:: " + eventDateXn);
            //     String string = "January 2, 2010";
            //      DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
            //     Date date = format.parse(string);
            //     System.out.println(date); // Sat Jan 02 00:00:00 GMT 2010
//            String dateXcOut = "";
//            DateFormat dFormatIn = new SimpleDateFormat("yyyy-MM-dd");
//            DateFormat dFormatOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            //  Date dateXcIn = dFormatOut.parse(dFormatIn.format(eventDate+" 00:00:00"));
//            String eventDateZ = "2020-05-04 00:00:00";
//            Date dateXcIn = dFormatOut.parse(dFormatIn.format(eventDateZ));
            //    dateXcOut = dFormatOut.format(dateXcOut);
            //     Date dateXcOutIn = dFormatOut.parse(dateXcOut);
            //        System.out.println("dateXcIn dateXcIn:: " + dateXcIn);
            //    System.out.println("dateXcOut dateXcOut:: " + dateXcOut);
            //     System.out.println("dateXcOutIn dateXcOutIn:: " + dateXcOutIn);
            event.setIdEvent(eventsId);
            eventCat.setIdCat(Integer.parseInt(eventCategoryId));
            event.setEventCategory(eventCat);
            event.setEventTitle(eventTitle);
            event.setEventVenue(eventVenue);
            event.setEventDuration(eventDuration);
            event.setEventShortDesc(eventShortDesc);
            event.setEventDesc(eventDesc);
            event.setEventDate(eventDate);
            //    event.setEventDateTime(date);
            event.setFeatureImage(fileName1);
            event.setShowingOrder(showingOrder);

            event.setAddDate(adddate);
            event.setAddUser(adduser);
            event.setAddTerm(addterm);
            event.setAddIp(addip);

            dbsession.save(event);

            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {

                strMsg = "Events Added Successfully";
                response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                //    sendRedirectPage = GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg;

                //    System.out.println("sendRedirectPage wasCommitted:: " + sendRedirectPage);
                return;

            } else {
                dbtrx.rollback();

                strMsg = "Error!!! When Events add";
                response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);

                //  sendRedirectPage = GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg;
                //  System.out.println("sendRedirectPage rollback:: " + sendRedirectPage);
                return;
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }

        //  strMsg = "Error!!! When adding news.Please try again";
        //  response.sendRedirect(GlobalVariable.baseUrl + "/eventsManagement/eventsAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        //   sendRedirectPage = "GlobalVariable.baseUrl + \"/eventsManagement/eventsAdd.jsp?sessionid=\" + sessionIdH + \"&strMsg=\" + strMsg";
        //   RequestDispatcher dd = request.getRequestDispatcher(sendRedirectPage);
        //   dd.forward(request, response);
        //  System.out.println("sendRedirectPage:: " + sendRedirectPage);
        //   RequestDispatcher dd = request.getRequestDispatcher(sendRedirectPage);
        //   dd.forward(request, response);
    }
}
