/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.Member;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author NAHID
 */
@WebServlet(name = "VideoCommentAdd", urlPatterns = {"/VideoCommentAdd"})
public class VideoCommentAdd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet videoCommentAdd</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet videoCommentAdd at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         System.out.println("process ==== " + request.getParts());

        // String filePath = GlobalVariable.imageUploadPath + "chattingfile/";
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        Logger logger = Logger.getLogger("postContentRequest_jsp.class");

        SessionImpl sessionImpl = (SessionImpl) dbsession;
        Connection con = sessionImpl.connection();

        getRegistryID getId = new getRegistryID();

        String imageBase64String = "";
        String filetype = "";
        // String filePath = GlobalVariable.imageUploadPath;
        //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
        //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
        //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
        //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
        //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

        // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
        // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
        // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
        String filePath = GlobalVariable.imageUploadPath + "postcontent/";

        logger.info("postContentRequest API filePath :" + filePath);

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
        Date dateToday = new Date();

        //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
        //   String imageUrl = new GlobalVariable().imageUrl;
        //   String imagePath = new GlobalVariable().imagePath;
        String key = "";
        String memberId = "";
        String givenPassword = "";

        String postContentType = "";
//        String postContentCategory = "";
//        String postContentTitle = "";
//        String postContentShortDesc = "";
        String postContentDesc = "";
//        String postContentPostLink = "";
        String postContentPicture = "";
        String postContentDate = "";
        String adddate = "";
        String vidId = "";
        String Par = "";
        String Commenter = "";
        String FileLink = "null";

        JSONArray bannerObjArray = new JSONArray();
        JSONObject bannerObj = new JSONObject();

        if (request.getMethod().equals("GET")) {

            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "GET Method is not allowed here!.");
            bannerObj.put("ResponseData", bannerObjArray);
            PrintWriter writer = response.getWriter();
            writer.write(bannerObj.toJSONString());
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }

        if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("commentfrom") && request.getParameterMap().containsKey("videoid") && request.getParameterMap().containsKey("commentparent") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("contentType")) {
            key = request.getParameter("key").trim();

            memberId = request.getParameter("memberId").trim();
            givenPassword = request.getParameter("password").trim();
            
            vidId = request.getParameter("videoid").trim();
            Commenter = request.getParameter("commentfrom").trim();
            Par = request.getParameter("commentparent").trim();
            postContentType = request.getParameter("contentType") == null ? "TX" : request.getParameter("contentType").trim();
//            postContentCategory = request.getParameter("contentCategory") == null ? "" : request.getParameter("contentCategory").trim();
//            postContentTitle = request.getParameter("contentTitle") == null ? "" : request.getParameter("contentTitle").trim();
//            postContentShortDesc = request.getParameter("contentShortDesc") == null ? "" : request.getParameter("contentShortDesc").trim();
            postContentDesc = request.getParameter("commentdesc") == null ? "" : request.getParameter("commentdesc").trim();
//            postContentPostLink = request.getParameter("contentPostLink") == null ? "" : request.getParameter("contentPostLink").trim();

            filetype = request.getParameter("fileType") == null ? "" : request.getParameter("fileType");
            //imageBase64String = request.getParameter("imageString");
            logger.info("postContentRequest API  imageBase64String :" + filetype);

            logger.info("postContentRequest API imageBase64String :" + filetype);

        } else {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Wrong parameter.");
            bannerObj.put("ResponseData", bannerObjArray);
            PrintWriter writer = response.getWriter();
            writer.write(bannerObj.toJSONString());
            response.getWriter().flush();
            response.getWriter().close();

            return;
        }
        String adjustLettter1 = "+88";
        String adjustLettter2 = "+";
        String memberId1 = "";
        String firstLetter = memberId.substring(0, 1);

        if (firstLetter.equals("0")) {
            memberId1 = adjustLettter1 + memberId;
        }
        else if (firstLetter.equals("8")) {
            memberId1 = adjustLettter2 + memberId;
        }
        else if (firstLetter.equals("+")) {
            memberId1 = memberId;
        }
        else{
            memberId = "0"+memberId;
            memberId1 = adjustLettter1 + memberId;
        }
        Member member = null;
        int memId = 0;
        String dbPass = "";

        logger.info("User ID :" + memberId);
        //   int rec = SystemToken.KeyValidation(key);
        int rec = 1;

        logger.info("JoyBangla :: API :: postContentRequest API rec :: " + rec);

        Encryption encryption = new Encryption();

        JSONObject logingObj = new JSONObject();

        JSONObject postContentObj = new JSONObject();
        JSONArray postContentObjArr = new JSONArray();

        JSONObject responseObj = new JSONObject();
        JSONArray logingObjArray = new JSONArray();
        Query qMember = null;
        Query memberKeySQL = null;

        String filterSQLStr = "";
        Object searchObj[] = null;

        //int total_pages = 0;
        String pContentSQL = null;
        String pContentCountSQL = null;
        Query pContentSQLQry = null;
        Object[] pContentObj = null;

        String contentId = "";
        String contentCategory = "";
        String contentType = "";
        String contentTitle = "";
        String contentShortDesc = "";
        String contentDesc = "";
        String contentDate = "";
        String contentPostLink = "";
        String contentPostPicture = "";
        String contentPostPictureLink = "";

        String memberIEBId = "";
        String memberIEBIdFirstChar = "";
        String memberPictureName = "";
        String memberPictureLink = "";

        String ownContentFlag = "";

        Query postContentRequestAddSQL = null;

        Query contentRatingAddSQL = null;

        String memMemberName = "";

System.out.println("_____________________________________________I'm Here__________________________________________________________________________--");
        
//_________________________________________________________________________________NAHID___________________________________________________________________________________________
        try {
            
            if (postContentType.equalsIgnoreCase("PC")) {
            String fileType = request.getParameter("fileType");
            // isMultipart = ServletFileUpload.isMultipartContent(request);
            // logger.debug("process mobile resu 130 ");
            // String path = getServletContext().getRealPath("/img2/");
            String path = GlobalVariable.imageUploadPath ;
            String distinctpath = "videocomment/";
            
            if (fileType.equals("1")) {
                distinctpath = distinctpath;
            }
            path = path + distinctpath;
//            else if (fileType.equals("2")) {
//                distinctpath = distinctpath + "audio/";
//            } else if (fileType.equals("3")) {
//                distinctpath = distinctpath + "video/";
//            }

            path = path + distinctpath;

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);
            String FileNamePrefix = filePrefixToday + "_" + rand1 + "_" + rand2;
            // FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
            Collection<Part> parts = new ArrayList<Part>();
            try {

                parts = request.getParts();

                System.out.println("process mobile resu  131 partsize==== " + parts.size());
            } catch (Exception e) {
                System.out.println("process mobile resu  13000 partsize==== " + e.getMessage());
                // TODO: handle exception
            }

            System.out.println("2");

            if (parts != null && parts.size() > 0) {
                System.out.println("3");
                Part filePart_recordFile = request.getPart("file");
                if (filePart_recordFile != null) {
                    // System.out.println("4");
                    String Value = (String) getFileName(filePart_recordFile);
                    System.out.println("process mobile resu  131 ");
                    if (Value != null) {
                        // Value = Jsoup.clean(Value,Whitelist.simpleText());
                    }
                    System.out.println("recordFile = " + Value);
                    if (Value != null && !Value.equalsIgnoreCase("")) {
                        if (Value != null && !Value.equalsIgnoreCase("")) {
                            String FileName = FileNamePrefix + "_" + Value;
                            // appUser.profileImageFile = (FileName);

                            boolean ret = uploadFile(filePart_recordFile, FileName, path);

                            if (ret == true) {
                                FileLink = path+ FileName;
                                JSONObject Obj = new JSONObject();
                               // Obj.put("FullAccessPath", GlobalVariable.baseUrlImg + distinctpath + FileName);
                                Obj.put("BasePath", distinctpath);
                                Obj.put("FileName", FileName);
                                Obj.put("ResponseText", "Success");
                                Obj.put("ResponseCode", "1");
                                PrintWriter writer = response.getWriter();
                                writer.write(Obj.toJSONString());
                                response.getWriter().flush();
                                response.getWriter().close();
                            } else {
                                JSONObject Obj = new JSONObject();
                                // Obj.put("FullAccessPath", GlobalVariable.baseUrlImg+distinctpath+FileName);
                                // Obj.put("BasePath", distinctpath);
                                // Obj.put("FileName", FileName);
                                Obj.put("ResponseText", "Failed");
                                Obj.put("ResponseCode", "0");
                                PrintWriter writer = response.getWriter();
                                writer.write(Obj.toJSONString());
                                response.getWriter().flush();
                                response.getWriter().close();
                            }

                        }
                    } else {
                        System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                    }
                    // System.out.println("process profile image "+appUser.profileImageFile);
                } else {
                    // appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
                    // System.out.println("process profile image from elseeeeee===
                    // "+appUser.profileImageFile);

                }
            } else {
                // appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
            }

            // boolean result = (boolean) app_userDAO.updateWithuserName(appUser);

            // return result;
        }
        
        
        //_______________________________________________________________NNNNNNNNNNNNNNNNNNNNNNNNN_____________________________________________________________________
        
        qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

                if (!qMember.list().isEmpty()) {
                    for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                        member = (Member) itr0.next();
                        memId = member.getId();
                        memMemberName = member.getMemberName().toString();
                        memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                        dbPass = memberKeySQL.uniqueResult().toString();
                        if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                            String idS = getId.getID(60);
                            int postContentRequestId = Integer.parseInt(idS);

                            InetAddress ip;
                            String hostname = "";
                            String ipAddress = "";
                            try {
                                ip = InetAddress.getLocalHost();
                                hostname = ip.getHostName();
                                ipAddress = ip.getHostAddress();

                            } catch (UnknownHostException e) {

                                e.printStackTrace();
                            }

                            String adduser = Integer.toString(memId);
                            adddate = dateFormatX.format(dateToday);
                            String customOrderby = "0";
                            String published = "1";
                            
                            getRegistryID getIdd = new getRegistryID();
                            String idSS = getIdd.getID(68);
                            int videocommentId = Integer.parseInt(idSS);

//                            System.out.println("videoCenterInfoShow :: videoId " + videoId);

                            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                            Date now = new Date();
                            String strDateTime = sdfDate.format(now);

//                            System.out.println("videoCenterInfoShow :: videoId " + videoId);

                            SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                            Date now1 = new Date();
                            String strDateTime1 = sdfDate1.format(now1);

                            Query videocommentAddSQL = dbsession.createSQLQuery("INSERT INTO video_comment("
                                                            + "id,"
                                                            + "parent,"
                                                            + "commment_desc,"
                                                            + "picture_link,"
                                                            + "video_id,"
                                                            + "commment_from,"
                                                            + "commment_date"                
                                                            + ") VALUES("
                                                            + "'" + videocommentId + "',"
                                                            + "'" + Par + "',"        
                                                            + "'" + postContentDesc  + "',"
                                                            + "'" + FileLink + "',"
                                                            + "'" + vidId + "',"
                                                            + "'" + Commenter+ "',"
                                                            + "'" + strDateTime1 + "'"
                                                            + "  ) ");


                                                    System.out.println("videocommentAddSQL :: videocommentAddSQL " + videocommentAddSQL);

                                                    videocommentAddSQL.executeUpdate();



                                                    dbtrx.commit();


                            //insert post_content_rating_point
                            contentRatingAddSQL = dbsession.createSQLQuery("INSERT INTO post_content_rating_point("
                                    + "id,"
                                    + "content_id,"
                                    + "like_content_point,"
                                    + "comment_content_point,"
                                    + "share_content_point,"
                                    + "update_time"
                                    + ") VALUES("
                                    + "'" + postContentRequestId + "',"
                                    + "'" + postContentRequestId + "',"
                                    + "'0',"
                                    + "'0',"
                                    + "'0',"
                                    + "'" + adddate + "'"
                                    + "  ) ");

                            logger.info("contentRatingAddSQL ::" + contentRatingAddSQL);

                            contentRatingAddSQL.executeUpdate();

                            con.commit();
                            con.close();

                            String msgTitle = "";
                            if (postContentType.equals("PC")) {
                                msgTitle = memMemberName + " has uploaded a picture.";
                            } else {
                                msgTitle = memMemberName + " has added a new post.";
                            }

                            //   String msgBody = postContentShortDesc;
                            String msgBody = "";
                            String topicsName = "General"; //
                            String msgPriority = "high"; // normal
//                            String fcmsend1 = fcmsend.sendTopicsWiseNotification(msgTitle, msgBody, topicsName, msgPriority);
//                            logger.info("fcmsend1" + fcmsend1);
//                            System.out.println("fcmsend1::" + fcmsend1);

                            postContentObj = new JSONObject();
                            postContentObj.put("ContentRequestId", postContentRequestId);
                            postContentObj.put("ContentType", postContentType);
//                            postContentObj.put("ContentCategory", postContentCategory);
//                            postContentObj.put("ContentTitle", postContentTitle);
//                            postContentObj.put("ContentShortDesc", postContentShortDesc);
                            postContentObj.put("ContentDesc", postContentDesc);
//                            postContentObj.put("ContentPostLink", postContentPostLink);
                            postContentObj.put("ContentPicture", postContentPicture);
//                            postContentObj.put("ContentPictureLink", contentPostPictureLink);

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Post content added successfully");
                            logingObj.put("ResponseData", postContentObj);

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "2");
                            logingObj.put("ResponseText", "Member password wrong");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("User ID :" + memberId + " Member password wrong");

                        }

                    }

                } else {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "NotFound");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("User ID :" + memberId + " Not Found");

                }
        
        
        
        
        //__________________________________________________________________NNNNNNNNNNNNNNNNNNNNNNN_____________________________________________________________________

        } catch (Exception e) {
            e.printStackTrace();
        }
        // return false;
        //__________________________________________________________________________NAHID_________________________________________________________________
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public static boolean uploadFile(Part filePart, String fileName, String path) {

        System.out.println("fileNamefileNamefileNamefileName " + fileName + "      " + path);
        OutputStream out = null;
        InputStream filecontent = null;
        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
            System.out.println("created directory " + path);
        }

        try {
            out = new FileOutputStream(new File(path + File.separator + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            // MediaMetadataRetriever
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            // out.
            System.out.println("New file " + fileName + " created at " + path);

        } catch (IOException fne) {
            System.out.println("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent " + "location.");
            System.out.println("ERROR: " + fne.getMessage());
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            if (filecontent != null) {
                try {
                    filecontent.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
