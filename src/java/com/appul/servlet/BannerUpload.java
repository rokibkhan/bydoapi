/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.DBAccess;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "BannerUpload", urlPatterns = {"/BannerUpload"})
@MultipartConfig
public class BannerUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();

            Part bannerTitleP = request.getPart("bannerTitle");
            Part bannerDescP = request.getPart("bannerDesc");
            Part bannerShowingOrderP = request.getPart("bannerShowingOrder");
            Part bannerStatusP = request.getPart("status");
            Part file1P = request.getPart("file1");

            Scanner bannerTitleS = new Scanner(bannerTitleP.getInputStream());
            Scanner bannerDescS = new Scanner(bannerDescP.getInputStream());
            Scanner bannerShowingOrderS = new Scanner(bannerShowingOrderP.getInputStream());
            Scanner bannerStatusS = new Scanner(bannerStatusP.getInputStream());

            String bannerTitle = bannerTitleS.nextLine();
            String bannerDesc = bannerDescS.nextLine();
            String bannerShowingOrder = bannerShowingOrderS.nextLine();
            String bannerStatus = bannerStatusS.nextLine();

            getRegistryID getId = new getRegistryID();
            String idS = getId.getID(18);
            int bannerId = Integer.parseInt(idS);

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);
            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            PreparedStatement ps = con.prepareStatement("insert into banner values(?,?,?,?,?,?)");

            ps.setInt(1, bannerId);
            ps.setString(2, bannerTitle);
            ps.setString(3, bannerDesc);
            ps.setString(4, bannerShowingOrder);
            ps.setBinaryStream(5, file1P.getInputStream(), (int) file1P.getSize());
            ps.setString(6, bannerStatus);

            ps.executeUpdate();
            con.commit();
            con.close();
            dbsession.close();
            out.println("Successfully Added Banner Image.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
