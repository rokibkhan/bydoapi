/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Query;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Zeeon
 */
@WebServlet(name = "MemberUpdateProfileBackgroundPictureInfo", urlPatterns = {"/MemberUpdateProfileBackgroundPictureInfo"})
public class MemberUpdateProfileBackgroundPictureInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        Logger logger = Logger.getLogger("memberUpdateProfilePictureInfo.class");

        JSONObject logingObj = new JSONObject();
        JSONObject responseObj = new JSONObject();
        JSONArray logingObjArray = new JSONArray();
        String key = "";
        String memberId = "";
        String givenPassword = "";

        String haveAttachment = ""; //1== yes 0==No attachment    
        String memberPictureName = "";
        String memberPictureNameLink = "";
        String fileType = "";

        String adddate = "";

        JSONArray bannerObjArray = new JSONArray();
        JSONObject bannerObj = new JSONObject();

        if (request.getMethod().equals("GET")) {

            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "GET Method is not allowed here!.");
            bannerObj.put("ResponseData", bannerObjArray);
            PrintWriter writer = response.getWriter();
            writer.write(bannerObj.toJSONString());
            response.getWriter().flush();
            response.getWriter().close();
            return;
        }

        if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memId") && request.getParameterMap().containsKey("fileType")) {
            key = request.getParameter("key").trim();

            memberId = request.getParameter("memId").trim();
            fileType = request.getParameter("fileType") == null ? "" : request.getParameter("fileType");
            //imageBase64String = request.getParameter("imageString");
            logger.info("memberUpdateProfilePictureInfo API  fileType :" + fileType);

            System.out.println("memberUpdateProfilePictureInfo API fileType :" + fileType);

        } else {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Wrong parameter.");
            bannerObj.put("ResponseData", bannerObjArray);
            PrintWriter writer = response.getWriter();
            writer.write(bannerObj.toJSONString());
            response.getWriter().flush();
            response.getWriter().close();

            return;
        }
        System.out.println("process ==== " + request.getParts());

        logger.info("User ID :" + memberId);
        //   int rec = SystemToken.KeyValidation(key);
        int rec = 1;

        logger.info("JoyBangla :: API :: memberUpdateProfilePictureInfo API rec :: " + rec);

        if (rec == 1) {
            try {
                String path = GlobalVariable.imageUploadPath ;
                String distinctpath = "member/";

                if (fileType.equals("1")) {
                    distinctpath = distinctpath;
                } /*else if (fileType.equals("2")) {
                    distinctpath = distinctpath + "audio/";
                } else if (fileType.equals("3")) {
                    distinctpath = distinctpath + "video/";
                }*/

                path = path + distinctpath;
                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());

                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);
                String FileNamePrefix = filePrefixToday + "_" + rand1 + "_" + rand2;
                String updateDate = "";

                updateDate = dateFormatX.format(dateFileToday);
                // FileNamePrefix = UUID.randomUUID().toString().substring(0, 10);
                Collection<Part> parts = new ArrayList<Part>();
                try {
                    parts = request.getParts();

                    System.out.println("process mobile resu  131 partsize==== " + parts.size());
                } catch (Exception e) {
                    System.out.println("process mobile resu  13000 partsize==== " + e.getMessage());
                    // TODO: handle exception
                }

                System.out.println("2");
                if (parts != null && parts.size() > 0) {
                    System.out.println("3");
                    Part filePart_recordFile = request.getPart("file");
                    if (filePart_recordFile != null) {
                        //System.out.println("4");
                        String Value = (String) getFileName(filePart_recordFile);
                        System.out.println("process mobile resu  131 ");
                        if (Value != null) {
                            //Value = Jsoup.clean(Value,Whitelist.simpleText());
                        }
                        System.out.println("recordFile = " + Value);
                        if (Value != null && !Value.equalsIgnoreCase("")) {
                            if (Value != null && !Value.equalsIgnoreCase("")) {
                                String FileName = FileNamePrefix + "" + "" + "" + Value;
                                //appUser.profileImageFile = (FileName);

                                Query memberSQL = dbsession.createSQLQuery("SELECT * FROM member WHERE id = '" + memberId + "'");
                                if (!memberSQL.list().isEmpty()) {

                                    boolean ret = uploadFile(filePart_recordFile, FileName, path);

                                    if (ret) {
                                        Query profilePicUpdate = dbsession.createSQLQuery("UPDATE member_profile_bg SET "
                                                + "bg_file_name = '" + FileName + "',"
                                                + "update_time = '" + updateDate + "'"
                                                + "WHERE member_id = '" + memberId + "'");
                                        profilePicUpdate.executeUpdate();

                                        dbtrx.commit();
                                        if (dbtrx.wasCommitted()) {
                                            JSONObject Obj = new JSONObject();
                                            Obj.put("FullAccessPath", GlobalVariable.baseUrlImg2 + "/upload/" + distinctpath + FileName);
                                            Obj.put("BasePath", distinctpath);
                                            Obj.put("FileName", FileName);
                                            Obj.put("ResponseText", "Success");
                                            Obj.put("ResponseCode", "1");
                                            PrintWriter writer = response.getWriter();
                                            writer.write(Obj.toJSONString());
                                            response.getWriter().flush();
                                            response.getWriter().close();
                                        } else {
                                            JSONObject Obj = new JSONObject();
                                            // Obj.put("FullAccessPath", GlobalVariable.baseUrlImg+distinctpath+FileName);
                                            // Obj.put("BasePath", distinctpath);
                                            // Obj.put("FileName", FileName);
                                            Obj.put("ResponseText", "Failed");
                                            Obj.put("ResponseCode", "0");
                                            PrintWriter writer = response.getWriter();
                                            writer.write(Obj.toJSONString());
                                            response.getWriter().flush();
                                            response.getWriter().close();
                                        }

                                    } else {
                                        JSONObject Obj = new JSONObject();
                                        // Obj.put("FullAccessPath", GlobalVariable.baseUrlImg+distinctpath+FileName);
                                        // Obj.put("BasePath", distinctpath);
                                        // Obj.put("FileName", FileName);
                                        Obj.put("ResponseText", "Failed");
                                        Obj.put("ResponseCode", "0");
                                        PrintWriter writer = response.getWriter();
                                        writer.write(Obj.toJSONString());
                                        response.getWriter().flush();
                                        response.getWriter().close();
                                    }

                                } else {
                                    logingObj = new JSONObject();
                                    logingObj.put("ResponseCode", "0");
                                    logingObj.put("ResponseText", "NotFound");
                                    logingObj.put("ResponseData", logingObjArray);
                                    logger.info("User ID :" + memberId + " Not Found");
                                    PrintWriter writer = response.getWriter();
                                    writer.write(logingObj.toJSONString());
                                    response.getWriter().flush();
                                    response.getWriter().close();
                                }

                            }
                        } else {
                            System.out.println("FieldName has a null Value, not updating" + " = " + Value);
                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "File name is empty");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("Image is empty");
                            PrintWriter writer = response.getWriter();
                            writer.write(logingObj.toJSONString());
                            response.getWriter().flush();
                            response.getWriter().close();

                        }
                        //System.out.println("process profile image "+appUser.profileImageFile);
                    } else {
                        //appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
                        //System.out.println("process profile image from elseeeeee===  "+appUser.profileImageFile);
                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "0");
                        logingObj.put("ResponseText", "File is empty");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("Image is empty");
                        PrintWriter writer = response.getWriter();
                        writer.write(logingObj.toJSONString());
                        response.getWriter().flush();
                        response.getWriter().close();

                    }
                } else {
                    //appUser.profileImageFile = app_userDAO.getProfileImage(appUser.userName);
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "File is empty");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("Image is empty");
                    PrintWriter writer = response.getWriter();
                    writer.write(logingObj.toJSONString());
                    response.getWriter().flush();
                    response.getWriter().close();
                }

            } catch (Exception e) {
                e.printStackTrace();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Something went wrong!");
                logingObj.put("ResponseData", bannerObjArray);
                PrintWriter writer = response.getWriter();
                writer.write(logingObj.toJSONString());
                response.getWriter().flush();
                response.getWriter().close();
            }
        } else {
            logingObj = new JSONObject();
            logingObj.put("ResponseCode", "999");
            logingObj.put("ResponseText", "Key Validation Failed");
            logingObj.put("ResponseData", logingObjArray);
            PrintWriter writer = response.getWriter();
            writer.write(logingObj.toJSONString());
            response.getWriter().flush();
            response.getWriter().close();
        }
        dbsession.flush();
        dbsession.close();
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        System.out.println("Part Header = {0}" + partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    public static boolean uploadFile(Part filePart, String fileName, String path) {

        System.out.println("fileNamefileNamefileNamefileName " + fileName + "      " + path);
        OutputStream out = null;
        InputStream filecontent = null;
        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
            System.out.println("created directory " + path);
        }

        try {
            out = new FileOutputStream(new File(path + File.separator + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            // MediaMetadataRetriever
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            // out.
            System.out.println("New file " + fileName + " created at " + path);

        } catch (IOException fne) {
            System.out.println("You either did not specify a file to upload or are "
                    + "trying to upload a file to a protected or nonexistent " + "location.");
            System.out.println("ERROR: " + fne.getMessage());
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            if (filecontent != null) {
                try {
                    filecontent.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
