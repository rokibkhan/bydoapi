/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "ProductImageUpload", urlPatterns = {"/ProductImageUpload"})
@MultipartConfig
public class ProductImageUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();
            Part styleIdP = request.getPart("styleId");
            Part productIdP = request.getPart("productId");

            Part file1P = request.getPart("file1");
            Part file2P = request.getPart("file2");
            Part file3P = request.getPart("file3");
            Part file4P = request.getPart("file4");

            Scanner styleIdS = new Scanner(styleIdP.getInputStream());
            Scanner productIdS = new Scanner(productIdP.getInputStream());
            String styleId = styleIdS.nextLine();
            String productCode = productIdS.nextLine();

            getRegistryID getId = new getRegistryID();
            String idS = getId.getID(24);
            int productId = Integer.parseInt(idS);

            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            PreparedStatement ps = con.prepareStatement("insert into product_category_image values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps.setInt(1, productId);
            ps.setString(2, productCode);
            ps.setString(3, "PRODUCT_IMAGE_" + productCode + ".JPG");
            ps.setString(4, "PRODUCT_IMAGE_THUMB_" + productCode + ".JPG");
            ps.setString(5, "PRODUCT_IMAGE_SMALL_" + productCode + ".JPG");
            ps.setString(6, "PRODUCT_IMAGE_ORIGINAL_" + productCode + ".JPG");
            // size must be converted to int otherwise it results in error
            ps.setBinaryStream(7, file1P.getInputStream(), (int) file1P.getSize());
            ps.setBinaryStream(8, file2P.getInputStream(), (int) file2P.getSize());
            ps.setBinaryStream(9, file3P.getInputStream(), (int) file3P.getSize());
            ps.setBinaryStream(10, file4P.getInputStream(), (int) file4P.getSize());
            ps.setString(11, entryDate);
            ps.setString(12, userId);
            ps.setString(13, hostname);
            ps.setString(14, ipAddress);
            ps.setString(15, "");
            ps.setString(16, "");
            ps.setString(17, "");
            ps.setString(18, null);
            ps.executeUpdate();
            con.commit();
            ps.close();
            con.close();
            dbsession.close();
            out.println("Successfully Added Product Image.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
