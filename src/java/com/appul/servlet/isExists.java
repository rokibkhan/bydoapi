/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.SyRoles;
import com.appul.entity.SyUser;
import com.appul.entity.SyDept;
import com.appul.util.HibernateUtil;
import java.util.Iterator;
import org.hibernate.*;

/**
 *
 * @author Akter
 */
public class isExists {

    public static boolean isExist(String input, String key) {

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        Query q1 = null;

        if (key.equals("roleid")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyRoles roles where upper(roles.roleId)=upper('" + input + "')");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyRoles roles = (SyRoles) itr1.next();
                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }

        if (key.equals("userid")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyUser user where user.userId='" + input.toUpperCase() + "'");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyUser users = (SyUser) itr1.next();
                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }

        if (key.equals("deptId")) {
            //SyRoles roles=new SyRoles();
            q1 = dbsession.createQuery("from SyDept codept where (codept.deptId)=upper('" + input + "')");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                SyDept coDept = (SyDept) itr1.next();
                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }

        if (key.equals("productId")) {

            q1 = dbsession.createQuery("from ProductCategory  where  productCode='" + input + "' ");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
//                ProductCategory match = (ProductCategory) itr1.next();
                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }

        if (key.equals("productCode")) {

            q1 = dbsession.createQuery("from Product   where  productCode='" + input + "' ");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
//                ProductCategory match = (ProductCategory) itr1.next();

                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }
        
               if (key.equals("emailId")) {

            q1 = dbsession.createQuery("from Consumer   where  emailId='" + input + "' ");

            for (Iterator itr1 = q1.list().iterator(); itr1.hasNext();) {
                
                dbtrx.commit();
                dbsession.close();
                return true;
            }
            dbtrx.commit();
            dbsession.close();
            return false;
        }

        dbtrx.commit();
        dbsession.close();
        return false;

    }
}
